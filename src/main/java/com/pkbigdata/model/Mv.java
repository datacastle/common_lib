package com.pkbigdata.model;

import com.pkbigdata.util.JsonToMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import java.util.Map;

/**
 * Created by jayr110 on 2016/3/8.
 * modelandview
 */
public class Mv extends ModelAndView {
    public Mv(){
    }

    public Mv(View view, String modelName, Object modelObject) {
        super(view, modelName, modelObject);
    }

    public Mv(String viewName) {
        super(viewName);
    }

    public Mv(View view) {
        super(view);
    }

    public Mv(String viewName, Map<String, ?> model) {
        super(viewName, model);
    }

    public Mv(View view, Map<String, ?> model) {
        super(view, model);
    }

    public Mv(String viewName, String modelName, Object modelObject) {
        super(viewName, modelName, modelObject);
    }

    @Override
    public Mv addObject(String attributeName, Object attributeValue) {
        super.addObject(attributeName, attributeValue);
        return this;
    }

    @Override
    public Mv addObject(Object attributeValue) {
        super.addObject(attributeValue);
        return this;
    }

    @Override
    public Mv addAllObjects(Map<String, ?> modelMap) {
        super.addAllObjects(modelMap);
        return this;
    }

    public Mv end(){
        String dataStructure= JsonToMap.beanToJson(this.getModel());
        this.addObject("dataStructure",dataStructure);
        return this;
    }


    public Mv setView(String viewName) {
        super.setViewName(viewName);
        return this;
    }
}
