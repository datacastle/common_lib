package com.pkbigdata.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 赵仁杰 on 2015/3/25.
 * 控制器使用的通用model
 */
public class Model {

    private Map<String, Object> data = new HashMap<String, Object>();
    public String msg = "";
    public Boolean flag = false;
    public Boolean login = true;

    public Model(){

    }
    public Model(String msg){
        this.msg=msg;

    }
    public Model(String msg, Map<String, Object> data){
        this.msg = msg;
        this.data = data;
    }

    /**
     * 清空map
     */
    public void clear() {
        msg = "";
        data.clear();
        flag = false;
    }

    public Model put(String key, Object value) {
        data.put(key, value);
        return this;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public Model setData(Map<String, Object> data) {
        this.data = data;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public Model setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public Boolean getFlag() {
        return flag;
    }

    public Model setFlag(Boolean flag) {
        this.flag = flag;
        return this;
    }

    public Boolean getLogin() {
        return login;
    }

    public Model setLogin(Boolean login) {
        this.login = login;
        return this;
    }
}
