package com.pkbigdata.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

/**
 * Created by lotus_work on 2015/3/30.
 *  封装了需要保存的序列化用户
 */
public class LoginUser implements Serializable {
    private String userid;//用户id
    private String username;//用户名
    private String avatar;//用户头像
    private String userlevel = "noBinding";//用户等级，super超级管理员，manager管理员，noBinding第三方登录的未绑定,normal普通用户
    private String email;//用户邮箱
    private String usertype ;//用户类型
    private boolean status=false;
    private String userIp ;
    private String cmtpId;//竞赛id
    private String cmptName;//竞赛名称
    private String cmptStage;//竞赛阶段
    private String phone;//手机号
    private Long loginTime;

    @JsonIgnore
    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserlevel() {
        return userlevel;
    }

    public void setUserlevel(String userlevel) {
        this.userlevel = userlevel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }

    public String getCmtpId() {
        return cmtpId;
    }

    public void setCmtpId(String cmtpId) {
        this.cmtpId = cmtpId;
    }

    public String getCmptName() {
        return cmptName;
    }

    public void setCmptName(String cmptName) {
        this.cmptName = cmptName;
    }

    public String getCmptStage() {
        return cmptStage;
    }

    public void setCmptStage(String cmptStage) {
        this.cmptStage = cmptStage;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Long getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Long loginTime) {
        this.loginTime = loginTime;
    }

    @Override
    public String toString() {
        return "LoginUser{" +
                "userid='" + userid + '\'' +
                ", username='" + username + '\'' +
                ", userlevel='" + userlevel + '\'' +
                ", email='" + email + '\'' +
                '}';
    }



}
