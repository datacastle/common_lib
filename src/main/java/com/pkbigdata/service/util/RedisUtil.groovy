package com.pkbigdata.service.util

import com.pkbigdata.service.BaseService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.redis.core.ListOperations
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.data.redis.core.ValueOperations
import org.springframework.stereotype.Service

import javax.annotation.Resource
import javax.servlet.http.HttpServletRequest
import java.util.concurrent.TimeUnit

/**
 * Created by 赵仁杰 on 2016/3/28.
 */
@Service
class RedisUtil {
    @Resource
    RedisTemplate redisTemplate;

    ValueOperations<String, Object> valueOps;

    @Autowired
    BaseService service;

    private static final Logger logger = LoggerFactory.getLogger("RedisUtil.class");

    /**
     * 获取redis数据
     * @param key
     * @return
     */
    Object getValue(String key){
        ValueOperations<String, Object> valueOps = redisTemplate.opsForValue();

        Object o = valueOps.get(key);
        return o;
    }
    /**
     * 保存数据到redis
     * @param key
     * @param value
     * @return
     */
    Boolean addValue(String key,Object value){
        ValueOperations<String, Object> valueOps = redisTemplate.opsForValue();
        valueOps.set(key, value,2,TimeUnit.DAYS);
        return true;
    }
    /**
     * 保存数据到redis
     * @param key
     * @param value
     * @return
     */
    Boolean addValue(String key,Object value,long time,TimeUnit unit){
        ValueOperations<String, Object> valueOps = redisTemplate.opsForValue();
        valueOps.set(key, value,time,unit);
        return true;
    }

    /**
     * 删除当前sessiondi所对应的用户
     * @param sessionid
     */
    void remove(String key){
        redisTemplate.delete(key);
    }










    /**
     * 被动退出,有其他用户在登录账号，被挤出登录
     * @param request
     */
    void byLogout(HttpServletRequest request){
        remove(request.getSession().getId()+'dcUser');
        remove('loginUser'+request.getSession().getId());
        request.getSession().removeAttribute("loginUser");
    }



    /**
     *入列
     * @param key
     * @param value
     * @return
     */
    def lPush(String key,def value){
       ListOperations<String,Object> listOps= redisTemplate.opsForList()
        if (value instanceof GString)
            listOps.leftPush(key,value.toString())
        listOps.leftPush(key,value)
    }
    /**
     *批量入列
     * @param key
     * @param value
     * @return
     */
    def batchLPush(String key,Object... value){
        ListOperations<String,Object> listOps= redisTemplate.opsForList()
        (0..(value.length/10000)).each{
            int end = 0;
            if (((it+1)*10000)>=value.length){
                end = value.length-1
            }else{
                end=(it+1)*10000
            }
            def model = value[it*10000..end]
            listOps.leftPushAll(key,model)
        }
    }

    /***
     * 出列
     * @param key
     * @return
     */
    def rPop(String key){
        ListOperations<String,Object> listOps= redisTemplate.opsForList()
        listOps.rightPop(key)
    }

    /**
     * 模糊前缀删除
     * @param prefix
     * @return
     */
    def removeByPrefix(String prefix) {
        Set<String> keys=redisTemplate.keys(prefix+"*");
        redisTemplate.delete(keys);
    }

    /***
     * 查询key有多少个
     * @param key
     * @return
     */
    Long listSize(String key){
        ListOperations<String,Object> listOps= redisTemplate.opsForList()
        listOps.size(key)
    }

    /**
     * 设置一个竞争锁
     * 如果返回值没有或者为false，就返回true
     * @return
     */
    def getAndsetLock = { String key ->
        Boolean flag = getValue("lock" + key)
        if (!flag) {
            addValue("lock" + key, new Boolean(true))
            return true
        } else {
            return false
        }
    }

    /**
     * 清除锁
     * @param key
     */
    void removeLock(String key){
        remove("lock"+key)
    }

    /**
     * 获取list的全部数据
     * @param key
     * @return
     */
    def getList(String key){
        ListOperations<String,Object> listOps= redisTemplate.opsForList()
        listOps.range(key,0 as Long,listSize(key))
    }







}
