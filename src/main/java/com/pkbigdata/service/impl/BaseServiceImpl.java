package com.pkbigdata.service.impl;

import com.pkbigdata.dao.BaseDao;
import com.pkbigdata.service.BaseService;
import org.hibernate.type.Type;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by jay on 2014/11/28.
 */
@Transactional
@Service("service")
public class BaseServiceImpl implements BaseService {

    @Resource(name = "BaseDao")
    private BaseDao baseDao;

    public BaseDao getBaseDao() {
        return baseDao;
    }

    public void setBaseDao(BaseDao baseDao) {
        this.baseDao = baseDao;
    }
    /**
     * 保存实体
     *
     * @param entity
     */
    @Override
    public <T> void save(T entity) {

        baseDao.saveEntity(entity);
    }

    /**
     * 更新实体
     *
     * @param entity
     */
    @Override
    public <T> void saveOrUpdate(T entity) {
        baseDao.updateEntity(entity);
    }

    /**
     * 批量保存
     *
     * @param entitys
     */
    @Override
    public <T> void batchUpdate(List<T> entitys) {
        baseDao.batchUpdate(entitys);
    }

    /**
     * 删除实体
     *
     * @param entity
     */
    @Override
    public <T> void delete(T entity) {
        baseDao.delEntity(entity);
    }

    /**
     * 批量保存
     *
     * @param entitys
     */
    @Override
    public <T> void batchSave(List<T> entitys) {
        baseDao.batchSave(entitys);
    }

    /**
     * 通过id获取实体
     * @param t
     * @param id
     * @return
     */
    @Override
    public <T> T get(Class<T> t, String id) {
        return baseDao.getEntityById(t,id);
    }

    /**
     * 根据实体名称和实体属性获取唯一值
     *
     * @param t
     * @param propertyName
     * @param value
     * @return
     */
    @Override
    public <T> T findUniqueByProperty(Class<T> t, String propertyName, Object value) {
       return baseDao.findUniqueByProperty(t, propertyName, value);
    }

    /**
     * 通过实体名和实体属性获取列表值
     *
     * @param entityClass
     * @param propertyName
     * @param value
     * @return
     */
    @Override
    public <T> List<T> findByProperty(Class<T> entityClass, String propertyName, Object value) {

        return baseDao.findByProperty(entityClass, propertyName, value);
    }

    /**
     * 通过实体名获取全部实体
     *
     * @param entityClass
     * @return
     */
    @Override
    public <T> List<T> getAll(Class<T> entityClass) {
        return baseDao.getAllEntity(entityClass);
    }

    /**
     * 通过id,删除实体
     *
     * @param entityClass
     * @param id
     */
    @Override
    public <T> void deleteEntityById(Class<T> entityClass, String id) {
        baseDao.delEntityById(entityClass,id);

    }

    /**
     * 通过实体集合删除集合
     *
     * @param entities
     */
    @Override
    public <T> void deleteAllEntitie(Collection<T> entities) {
        baseDao.deleteAllEntitie(entities);
    }

    /**
     * 通过hql获取实体列表
     *
     * @param hql
     * @return
     */
    @Override
    public <T> List<T> findByHqlString(String hql) {
        return baseDao.findByHqlString(hql);
    }

    /**
     * 通过sql更新
     *
     * @param sql
     * @return
     */
    @Override
    public int updateBySqlString(String sql) {

        return baseDao.updateBySqlString(sql);
    }

    /**
     * 通过sql查找list
     *
     * @param query
     * @return
     */
    @Override
    public <T> List<T> findListbySql(String query) {
        return baseDao.findListbySql(query);
    }

    /**
     * 通过属性查找列表，并排序
     *
     * @param entityClass
     * @param propertyName
     * @param value
     * @param isAsc
     * @return
     */
    @Override
    public <T> List<T> findByPropertyisOrder(Class<T> entityClass, String propertyName, Object value, boolean isAsc) {
        return baseDao.findByPropertyisOrder(entityClass, propertyName, value, isAsc);
    }

    /**
     * 通过hql获取唯一实体
     *
     * @param hql
     * @return
     */
    @Override
    public <T> T findsingleResult(String hql) {
        return baseDao.findsingleResult(hql);
    }

    /**
     * 单表分页
     */
	@Override
	public <T> Map<String, Object> queryForPage(Class<T> entityClass, int page,
			int pagesize) {
		
		String hql = "select count(*) from "+ entityClass.getName()+" u";
		
		Map<String,Object> map = new HashMap<String,Object>();
		
		 List list = baseDao.queryForPage(entityClass, page, pagesize); // 要返回的某一页的记录列表
		 Long allRow = baseDao.findsingleResult(hql); // 总记录数
		 int totalPage = (int) (allRow%pagesize==0?allRow/pagesize:allRow/pagesize+1); // 总页数
		 int currentPage = page; // 当前页
		 int pageSize = pagesize; // 每页记录数
		 boolean isFirstPage = currentPage==1; // 是否为第一页
		 boolean isLastPage = currentPage==totalPage; // 是否为最后一页
		 boolean hasPreviousPage = currentPage>1; // 是否有前一页
		 boolean hasNextPage = currentPage<totalPage; // 是否有下一页
		
		map.put("list", list);
		map.put("allRow", allRow);
		map.put("totalPage", totalPage);
		map.put("currentPage", currentPage);
		map.put("pageSize", pageSize);
		map.put("isFirstPage", isFirstPage);
		map.put("isLasePage", isLastPage);
		map.put("hasPreviousPage", hasPreviousPage);
		map.put("hasNextPage", hasNextPage);
		
		 
		 
		 
		return map;
	}

	/**
     * 单表分页查询，通过属性排序,TRUE则使用order by property ASC
     * false 则使用order by property DESC
     * @param entityClass
     * @param page
     * @param pagesize
     * @param propertyName
     * @param isAsc
     * @return
     */
	@Override
	public <T> Map<String, Object> queryForPage(Class<T> entityClass, int page,
			int pagesize, String propertyName, boolean isAsc) {
		String hql = "select count(*) from "+ entityClass.getName()+" u";
		
		Map<String,Object> map = new HashMap<String,Object>();
		
		 List list = baseDao.queryForPage(entityClass, page, pagesize,propertyName,isAsc); // 要返回的某一页的记录列表
		 Long allRow = baseDao.findsingleResult(hql); // 总记录数
        int totalPage = (int) (allRow%pagesize==0?allRow/pagesize:allRow/pagesize+1); // 总页数
		 int currentPage = page; // 当前页
		 int pageSize = pagesize; // 每页记录数
		 boolean isFirstPage = currentPage==1; // 是否为第一页
		 boolean isLastPage = currentPage==totalPage; // 是否为最后一页
		 boolean hasPreviousPage = currentPage>1; // 是否有前一页
		 boolean hasNextPage = currentPage<totalPage; // 是否有下一页
		
		map.put("list", list);
		map.put("allRow", allRow);
		map.put("totalPage", totalPage);
		map.put("currentPage", currentPage);
		map.put("pageSize", pageSize);
		map.put("isFirstPage", isFirstPage);
		map.put("isLasePage", isLastPage);
		map.put("hasPreviousPage", hasPreviousPage);
		map.put("hasNextPage", hasNextPage);
		
		 
		 
		 
		return map;
	}

	/**
     * 通过hql分页
     * @param hql
     * @param page
     * @param pagesize
     * @return
     */
	@Override
	public <T> Map<String, Object> queryForPageByHQL(String hql, int page,
			int pagesize) {
        /**
         * 排除select在这个分页里面的干扰
         */
        String hql1 = "select count(*) ";
        hql = hql.trim();
        if (hql.startsWith("select")){
            hql1 = "select count(*) "+hql.substring(hql.indexOf("from"));
        }else{
            hql1 += hql;
        }
		
		Map<String,Object> map = new HashMap<String,Object>();
		
		 List list = baseDao.queryForPageByHQL(hql, page, pagesize); // 要返回的某一页的记录列表
		 Long allRow = baseDao.findsingleResult(hql1); // 总记录数
        int totalPage = (int) (allRow%pagesize==0?allRow/pagesize:allRow/pagesize+1); // 总页数
		 int currentPage = page; // 当前页
		 int pageSize = pagesize; // 每页记录数
		 boolean isFirstPage = currentPage==1; // 是否为第一页
		 boolean isLastPage = currentPage==totalPage; // 是否为最后一页
		 boolean hasPreviousPage = currentPage>1; // 是否有前一页
		 boolean hasNextPage = currentPage<totalPage; // 是否有下一页
		
		map.put("list", list);
		map.put("allRow", allRow);
		map.put("totalPage", totalPage);
		map.put("currentPage", currentPage);
		map.put("pageSize", pageSize);
		map.put("isFirstPage", isFirstPage);
		map.put("isLasePage", isLastPage);
		map.put("hasPreviousPage", hasPreviousPage);
		map.put("hasNextPage", hasNextPage);
		return map;
	}

	@Override
	public void executeHql(String hql, Object... params) {
		this.baseDao.executeHql(hql, params);
		
	}

	@Override
	public void executeHql(String hql, Map<String, Object> params) {
		this.baseDao.executeHql(hql, params);
		
	}

	@Override
	public <T> T selectByHql(String hql, Object... params) {
		return this.baseDao.selectByHql(hql, params);
	}

	@Override
	public <T> List<T> selectListByHql(String hql, Object... params) {
		return this.baseDao.selectListByHql(hql, params);
	}

	@Override
	public void executeSql(String sql, Object... params) {
		this.baseDao.executeSql(sql, params);
		
	}

	@Override
	public <T> T selectBySql(String sql, Class<T> entity, Object... params) {
		return this.baseDao.selectBySql(sql, entity, params);
	}

    /**
     * 执行绑定参数的SQL。用于查询单个对象
     *
     * @param sql    用“？”来定义参数位置
     * @param params 基本数据类型的参数
     * @return
     */
    @Override
    public <T> List<T> selectBySql(String sql, Object... params) {
        return this.baseDao.selectBySql(sql,params);
    }

    @Override
    public <T> List<T> selectBySql(String sql, Map<String,Type> type, Object... params) {
        return this.baseDao.selectBySql(sql,type,params);
    }

    @Override
    public <T> List<T> selectListBySql(String sql, Class<T> entity, Object... params) {

        //return this.baseDao.selectListByHql(sql,entity,params);
        return this.baseDao.selectListBySql(sql, entity, params);
    }

    @Override
    public <T> Map<String, Object> queryForPageByHQL(String hql, int page,
                                                     int pageSize, Object... params) {
        /**
         * 排除select在这个分页里面的干扰
         */
        String hql1 = "select count(*) ";
        hql = hql.trim();
        if (hql.startsWith("select")){
            hql1 = "select count(*) "+hql.substring(hql.indexOf("from"));
        }else{
            hql1 += hql;
        }
        Map<String,Object> map = new HashMap<String,Object>();
        List list = baseDao.queryForPageByHQL(hql, page, pageSize, params); // 要返回的某一页的记录列表
        Long allRow = baseDao.findsingleResult(hql1,params); // 总记录数
        int totalPage = (int) (allRow%pageSize==0?allRow/pageSize:allRow/pageSize+1); // 总页数
        int currentPage = page; // 当前页
        boolean isFirstPage = currentPage==1; // 是否为第一页
        boolean isLastPage = currentPage==totalPage; // 是否为最后一页
        boolean hasPreviousPage = currentPage>1; // 是否有前一页
        boolean hasNextPage = currentPage<totalPage; // 是否有下一页

        map.put("list", list);
        map.put("allRow", allRow);
        map.put("totalPage", totalPage);
        map.put("currentPage", currentPage);
        map.put("pageSize", pageSize);
        map.put("isFirstPage", isFirstPage);
        map.put("isLasePage", isLastPage);
        map.put("hasPreviousPage", hasPreviousPage);
        map.put("hasNextPage", hasNextPage);
        return map;
    }




    @Override
    public <T> Map<String, Object> queryForPageBySQL(String sql, int page, int pageSize, Object... params) {
        /**
         * 排除select在这个分页里面的干扰
         */
        String sql1 = "select count(*) ";
         sql = sql.trim();
         sql1 = sql1+sql.substring(sql.indexOf("from"));
        Map<String,Object> map = new HashMap<String,Object>();
        List list = baseDao.queryForPageBySQL(sql, page, pageSize, params); // 要返回的某一页的记录列表
        Long allRow = ((BigInteger)baseDao.selectSingleBySql(sql1,params)).longValue(); // 总记录数
        int totalPage = (int) (allRow%pageSize==0?allRow/pageSize:allRow/pageSize+1); // 总页数
        int currentPage = page; // 当前页
        boolean isFirstPage = currentPage==1; // 是否为第一页
        boolean isLastPage = currentPage==totalPage; // 是否为最后一页
        boolean hasPreviousPage = currentPage>1; // 是否有前一页
        boolean hasNextPage = currentPage<totalPage; // 是否有下一页

        map.put("list", list);
        map.put("allRow", allRow);
        map.put("totalPage", totalPage);
        map.put("currentPage", currentPage);
        map.put("pageSize", pageSize);
        map.put("isFirstPage", isFirstPage);
        map.put("isLasePage", isLastPage);
        map.put("hasPreviousPage", hasPreviousPage);
        map.put("hasNextPage", hasNextPage);
        return map;
    }

    @Override
    public <T> Map<String, Object> queryForPageBySQL(String sql, int page, int pageSize) {
        /**
         * 排除select在这个分页里面的干扰
         */
        String sql1 = "select count(*) ";
        sql = sql.trim();
        sql1 = sql1+sql.substring(sql.indexOf("from"));


        Map<String,Object> map = new HashMap<String,Object>();
        List list = baseDao.queryForPageBySQL(sql, page, pageSize); // 要返回的某一页的记录列表
        Long allRow = ((BigInteger)baseDao.selectSingleBySql(sql1)).longValue(); // 总记录数
        int totalPage = (int) (allRow%pageSize==0?allRow/pageSize:allRow/pageSize+1); // 总页数
        int currentPage = page; // 当前页
        boolean isFirstPage = currentPage==1; // 是否为第一页
        boolean isLastPage = currentPage==totalPage; // 是否为最后一页
        boolean hasPreviousPage = currentPage>1; // 是否有前一页
        boolean hasNextPage = currentPage<totalPage; // 是否有下一页

        map.put("list", list);
        map.put("allRow", allRow);
        map.put("totalPage", totalPage);
        map.put("currentPage", currentPage);
        map.put("pageSize", pageSize);
        map.put("isFirstPage", isFirstPage);
        map.put("isLasePage", isLastPage);
        map.put("hasPreviousPage", hasPreviousPage);
        map.put("hasNextPage", hasNextPage);
        return map;
    }
    /**
     * 通过hql取第一条
     *
     * @param hql
     * @return
     */
    @Override
    public <T> T findByHqlFirst(String hql) {
        return this.baseDao.findByHqlFirst(hql);
    }

    /**
     * 通过hql取第一条
     *
     * @param hql
     * @param params
     * @return
     */
    @Override
    public <T> T findByHqlFirst(String hql, Object... params) {
        return this.baseDao.findByHqlFirst(hql,params);
    }

    @Override
    public Integer getTableCount(String sql) {
        return this.baseDao.getTableCount(sql);
    }

    /**
     * 多参数查找，严格遵循params的长度是2的倍数
     *
     * @param entity
     * @param params
     * @return
     */
    @Override
    public <T> List<T> findByProperties(Class<T> entity, Object... params) {
        return this.baseDao.findByProperties(entity,params);
    }

    /**
     * 排序，多参数查找，严格遵循params的长度是2的倍数,
     *
     * @param entity        实体表名
     * @param orderProperty 排序字段
     * @param isAsc         怎么排序
     * @param params        查找参数
     * @return
     */
    @Override

    public <T> List<T> findByProperties(Class<T> entity, String orderProperty, boolean isAsc, Object... params) {
        return this.baseDao.findByProperties(entity,orderProperty,isAsc,params);
    }

    /**
     * 执行绑定参数的hql。用于查询出集合对象
     *
     * @param hql    在HQL查询语句中用”?”来定义参数位置
     * @param limit  取这条hql的前几条数据
     * @param params 可变参数 注意：参数只能是基本数据类型
     * @return 集合对象
     */
    @Override
    public <T> List<T> selectListByLimit(String hql, int limit, Object... params) {
        return this.baseDao.selectListByLimit(hql,limit,params);
    }
}

