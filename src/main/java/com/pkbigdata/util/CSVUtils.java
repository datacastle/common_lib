package com.pkbigdata.util;

import org.apache.commons.beanutils.BeanUtils;

import java.io.*;
import java.util.*;

/**
 * 文件操作
 */
public class CSVUtils {
    /**
     * 读取csv文件，结果为Map
     * @param key 用csv一行的第几个字段作为key,如果越界了，默认取第一个。注意key从0开始，如果key为-1，则是以行数为key。
     * @param isReadFirst 是否读取第一行，true为读取
     * @return
     */
    public static Map<String,Map> readCsv(File csv,int key,Boolean isReadFirst){
        Map<String,Map> data = new HashMap<String,Map>();
        try {
            InputStreamReader isr = new InputStreamReader(new FileInputStream(csv), "UTF-8");
            BufferedReader reader = new BufferedReader(isr);
            int lineNum = 0;
            if (!isReadFirst){
                reader.readLine();
                lineNum += 1;
            }
            String line = null;
            while((line=reader.readLine())!=null){
                lineNum += 1;
                String item[] = line.split(",");//CSV格式文件为逗号分隔符文件，这里根据逗号切分

                if (item == null){
                    continue;
                }

                Map map = new HashMap();
                for (int i = 0; i < item.length; i++) {
                    map.put(i+"",item[i]);
                }

                if (key == -1){
                    data.put(lineNum+"",map);
                    continue;
                }

                if (key<0||key>item.length-1){
                    key = 0;
                }

                data.put(item[key],map);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    /**
     * 生成为CVS文件
     * @param exportData
     *       源数据List
     * @param map
     *       csv文件的列表头map
     * @param outPutPath
     *       文件路径
     * @param fileName
     *       文件名称
     * @return
     */
    @SuppressWarnings("rawtypes")
    public static File createCSVFile(List exportData, LinkedHashMap map, String outPutPath,
                                     String fileName) {
        File csvFile = null;
        BufferedWriter csvFileOutputStream = null;

        try {
            File file = new File(outPutPath);
            if (!file.exists()) {
                file.mkdir();
            }
            //定义文件名格式并创建
            csvFile = File.createTempFile(fileName, ".csv", new File(outPutPath));
            System.out.println("csvFile：" + csvFile);
            // UTF-8使正确读取分隔符","
            csvFileOutputStream = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
                    csvFile), "UTF-8"), 1024);
            System.out.println("csvFileOutputStream：" + csvFileOutputStream);
            // 写入文件头部
            for (Iterator propertyIterator = map.entrySet().iterator(); propertyIterator.hasNext();) {
                Map.Entry propertyEntry = (Map.Entry) propertyIterator.next();
                csvFileOutputStream.write("\"" + (String) propertyEntry.getValue() != null ? (String) propertyEntry.getValue() : "" + "\"");
                if (propertyIterator.hasNext()) {
                    csvFileOutputStream.write(",");
                }
            }
            csvFileOutputStream.newLine();
            // 写入文件内容
            for (Iterator iterator = exportData.iterator(); iterator.hasNext();) {
                Object row = (Object) iterator.next();
                for (Iterator propertyIterator = map.entrySet().iterator(); propertyIterator
                        .hasNext();) {
                    Map.Entry propertyEntry = (Map.Entry) propertyIterator
                            .next();
                    if (row == null || propertyEntry==null|| propertyEntry.getKey()==null){
                        continue;
                    }
                    csvFileOutputStream.write((String) BeanUtils.getProperty(row,
                            (String) propertyEntry.getKey())==null?" ":(String) BeanUtils.getProperty(row,
                            (String) propertyEntry.getKey()));
                    if (propertyIterator.hasNext()) {
                        csvFileOutputStream.write(",");
                    }
                }
                if (iterator.hasNext()) {
                    csvFileOutputStream.newLine();
                }
            }
            csvFileOutputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                csvFileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return csvFile;
    }

    /**
     * 生成为CVS文件的字节数组
     * @param exportData
     *       源数据List
     * @param map
     *       csv文件的列表头map
     * @return
     */
    public static byte[] createCSVFile(List exportData, LinkedHashMap map) {
        StringBuffer sb = new StringBuffer();
            // 写入文件头部
            for (Iterator propertyIterator = map.entrySet().iterator(); propertyIterator.hasNext();) {
                Map.Entry propertyEntry = (Map.Entry) propertyIterator.next();
                sb.append( (String)propertyEntry.getValue() != null ? "\""+(String)propertyEntry.getValue()+ "\"" : "\"\"" );
                if (propertyIterator.hasNext()) {
                   sb.append(",");
                }
            }
        sb.append("\r\n");
            // 写入文件内容
            for (Iterator iterator = exportData.iterator(); iterator.hasNext();) {
                Object row = iterator.next();
                for (Iterator propertyIterator = map.entrySet().iterator(); propertyIterator
                        .hasNext();) {
                    Map.Entry propertyEntry = (Map.Entry) propertyIterator
                            .next();
                    if (row == null || propertyEntry==null|| propertyEntry.getKey()==null){
                        continue;
                    }
                    try {
                        sb.append(BeanUtils.getProperty(row,
                                 (String) propertyEntry.getKey())==null?"\"\"":"\""+BeanUtils.getProperty(row,
                                 (String) propertyEntry.getKey())+"\"");
                        } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (propertyIterator.hasNext()) {
                       sb.append(",");
                    }
                }
                sb.append("\r\n");

            }
        return  sb.toString().getBytes();
    }

    /**
     * 删除该目录filePath下的所有文件
     * @param filePath
     *      文件目录路径
     */
    public static void deleteFiles(String filePath) {
        File file = new File(filePath);
        if (file.exists()) {
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isFile()) {
                    files[i].delete();
                }
            }
        }
    }

    /**
     * 删除单个文件
     * @param filePath
     *     文件目录路径
     * @param fileName
     *     文件名称
     */
    public static void deleteFile(String filePath, String fileName) {
        File file = new File(filePath);
        if (file.exists()) {
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isFile()) {
                    if (files[i].getName().equals(fileName)) {
                        files[i].delete();
                        return;
                    }
                }
            }
        }
    }

    /**
     * 测试数据
     * @param args
     */
//    @SuppressWarnings({ "rawtypes", "unchecked" })
//    public static void main(String[] args) {
//        List exportData = new ArrayList<Map>();
//        Map row1 = new LinkedHashMap<String, String>();
//        row1.put("1", "11");
//        row1.put("2", "12");
//        row1.put("3", "13");
//        row1.put("4", "14");
//        exportData.add(row1);
//        row1 = new LinkedHashMap<String, String>();
//        row1.put("1", "21");
//        row1.put("2", "22");
//        row1.put("3", "23");
//        row1.put("4", "24");
//        exportData.add(row1);
//        LinkedHashMap map = new LinkedHashMap();
//        map.put("1", "第一列");
//        map.put("2", "第二列");
//        map.put("3", "第三列");
//        map.put("4", "第四列");
//
//        String path = "c:/export/";
//        String fileName = "文件导出";
//        File file = CSVUtils.createCSVFile(exportData, map, path, fileName);
//        String fileName2 = file.getName();
//        System.out.println("文件名称：" + fileName2);
//    }
}
