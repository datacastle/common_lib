package com.pkbigdata.util;

/**
 * Created by jayr110 on 2015/5/30.
 * 里面还有一些对字符串的工具封装
 */
public class StringUtil {
    /**
     * 对字符串通过下划线分割成数组
     * @param word
     * @return
     */
    public static String[] splitBy_(String word){
        return word.split("_");
    }

    /**
     * 将下划下替换为英文逗号
     * @param word
     * @return
     */
    public static String replaceall(String word){
        word = word.trim();
        word = removeFirstAndEnd(word,"_");
        return word.replaceAll("_",",");
    }


    /**
     * 清除掉字符串两端的固定符号，如果有，则清除，如果没有就不清除
     * @param word
     * @param regex
     * @return
     */
    public static String removeFirstAndEnd(String word,String regex){
        char[] temp = word.toCharArray();

        if (temp == null || temp.length == 0){
            return word;
        }


        String newWord = "";
        for (int i = 0; i < temp.length; i++) {
            if (i == 0){
                if (!regex.equals(temp[i]+"")){
                    newWord += temp[i];
                }
                continue;
            }
            if (i == temp.length-1){
                if (!regex.equals(temp[i]+"")){
                    newWord += temp[i];
                }
                continue;
            }
            newWord += temp[i];
        }

        return newWord;
    }



}
