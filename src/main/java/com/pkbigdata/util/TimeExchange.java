package com.pkbigdata.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * 时间转化工具 date转为时间戳 时间戳转date 互相与String的转换
 * 所有出现的String time 格式都必须为(yyyy-MM-dd HH:mm:ss)，否则出错
 *
 * @author 赵仁杰
 */
public class TimeExchange{

    /**
     * String(yyyy-MM-dd HH:mm:ss) 转 Date
     *
     * @param time
     * @return
     * @throws ParseException
     */
    public static Date StringToDate(String time) throws ParseException {

        Date date = new Date();
        // 注意format的格式要与日期String的格式相匹配
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            date = dateFormat.parse(time);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return date;
    }

    /**
     * Date转为String(yyyy-MM-dd HH:mm:ss)
     *
     * @param time
     * @return
     */
    public static String DateToString(Date time) {
        String dateStr = "";
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            dateStr = dateFormat.format(time);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateStr;
    }

    /**
     * String(yyyy-MM-dd HH:mm:ss)转10位时间戳
     *
     * @param time
     * @return
     */
    public static Integer StringToTimestamp(String time) {

        int times = 0;
        try {
            times = (int) ((Timestamp.valueOf(time).getTime()) / 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (times == 0) {
            System.out.println("String转10位时间戳失败");
        }
        return times;

    }

    /**
     * 10位int型的时间戳转换为String(yyyy-MM-dd HH:mm:ss)
     *
     * @param time
     * @return
     */
    public static String timestampToString(Integer time) {
        //int转long时，先进行转型再进行计算，否则会是计算结束后在转型
        long temp = (long) time * 1000;
        Timestamp ts = new Timestamp(temp);
        String tsStr = "";
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            //方法一  
            tsStr = dateFormat.format(ts);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tsStr;
    }

    /**
     * 10位时间戳转Date
     *
     * @param time
     * @return
     */
    public static Date TimestampToDate(Integer time) {
        long temp = (long) time * 1000;
        Timestamp ts = new Timestamp(temp);
        Date date = new Date();
        try {
            date = ts;
            //System.out.println(date);  
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * Date类型转换为10位时间戳
     *
     * @param time
     * @return
     */
    public static Integer DateToTimestamp(Date time) {
        Timestamp ts = new Timestamp(time.getTime());

        return (int) ((ts.getTime()) / 1000);
    }

    /**
     * 判断时间是否在一天之内
     *
     * @return
     */
    public static boolean isADay(Timestamp time) {
        Long nowTime = System.currentTimeMillis();
        /**
         * 按照毫秒计算
         */
        if (nowTime - time.getTime() < 24*60*60*1000) {
            return true;
        }
        return false;
    }
//    public static void main(String[] args){
//        File test = new File("C:\\Users\\lotus_work\\Desktop\\bbd\\type4_train\\type6_train");
//
//        if (test.isDirectory()){
//            File[] f = test.listFiles();
//
//            System.out.println(f.length);
//
//            for (int i = 0; i < f.length; i++) {
//
//                String c=f[i].getParent();
//                String filename = f[i].getName();
//                filename = filename.replaceFirst("type5","type3");
//                filename = filename.replaceFirst("type6","type4");
//                File mm=new File("C:\\Users\\lotus_work\\Desktop\\bbd\\type4_train\\new\\"+filename);
//
//                fileChannelCopy(f[i],mm);
//
//                System.out.println(i);
//
//
//               /* if(f[i].renameTo(mm))
//                {
//                    System.out.println("修改成功!");
//                }
//                else
//                {
//                    System.out.println("修改失败");
//                }*/
//            }
//
//        }
//
//
//
//
//    }
    public static void fileChannelCopy(File s, File t) {


        FileInputStream fi = null;

        FileOutputStream fo = null;

        FileChannel in = null;

        FileChannel out = null;

        try {

            fi = new FileInputStream(s);

            fo = new FileOutputStream(t);

            in = fi.getChannel();//得到对应的文件通道

            out = fo.getChannel();//得到对应的文件通道

            in.transferTo(0, in.size(), out);//连接两个通道，并且从in通道读取，然后写入out通道

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                fi.close();

                in.close();

                fo.close();

                out.close();

            } catch (IOException e) {

                e.printStackTrace();

            }

        }

    }

    class testa implements Runnable{
        File[] f = null;

        public testa(){

        }
        public testa(File[] f){
            this.f = f;
        }

        /**
         * When an object implementing interface <code>Runnable</code> is used
         * to create a thread, starting the thread causes the object's
         * <code>run</code> method to be called in that separately executing
         * thread.
         * <p/>
         * The general contract of the method <code>run</code> is that it may
         * take any action whatsoever.
         *
         * @see Thread#run()
         */
        @Override
        public void run() {
            for (int i = 0; i < f.length; i++) {

                String c=f[i].getParent();
                String filename = f[i].getName();
                filename = filename.replaceFirst("type5","type3");
                filename = filename.replaceFirst("type6","type4");
                File mm=new File("C:\\Users\\lotus_work\\Desktop\\bbd\\type3_test1\\new\\"+filename);

                fileChannelCopy(f[i],mm);

                System.out.println(i);


               /* if(f[i].renameTo(mm))
                {
                    System.out.println("修改成功!");
                }
                else
                {
                    System.out.println("修改失败");
                }*/
            }
        }
    }

}
