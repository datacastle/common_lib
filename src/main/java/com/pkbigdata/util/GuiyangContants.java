package com.pkbigdata.util;

/**
 * Created by lotus_work on 2015/12/23.
 * 贵阳竞赛合作，临时参数
 */
public class GuiyangContants {
    public static final String REG_URL = "http://www.guiyangdata.gov.cn/user/regcompetition";
    public static final Integer CMPTID = 152;
    public static final String downSSH = "http://www.guiyangdata.gov.cn/download/sshcert?instanceid=";
    public static final String WEB_URL = "http://guiyangdata.gov.cn/";
    public static final String username = "centos";
    public static final String yun_url = "/home/centos/dc";
    public static final String yun_file_name = "dc.zip";
    public static final String yun_reg_temple = "hi,管理员，云平台注册失败，用户名：${username},密码：${password}；</br> 错误代码${code}";
    public static final String yun_vi_temple = "hi,管理员，云服务器登陆失败，ip：${ip},用户：${user}，端口：${port}；";

}
