package com.pkbigdata.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by 李龙飞 on 2015-05-26.
 */
public class CheckCode {

    private static Logger logger= LoggerFactory.getLogger(CheckCode.class);
    public   boolean isCodeCouldCommit(String code,String path){

        //InputStream is=getClass().getResourceAsStream("/fiterClass");
        InputStream is=getClass().getResourceAsStream(path);
        BufferedReader reader=new BufferedReader(new InputStreamReader(is));
        try {
            String className="";
            while ((className = reader.readLine()) != null) {
                int index=code.indexOf(className);
                if (index!=-1)
                    return false;
            }
            System.out.println(reader.readLine());
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }finally {
            try {
                is.close();
                reader.close();
            } catch (IOException e) {
                logger.error(e.getMessage(),e);
            }
        }
        return true;


    }


}
