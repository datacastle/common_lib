package com.pkbigdata.util;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by WillChiang on 2015/7/7.
 */
public class WriteFile {

    private static final String name = "source.txt";

    /*public static void main(String[] args) {
        String source = "heihei";
        writeSourceToFile(source);
    }
*/
    public synchronized static void writeSourceToFile(String path, String source) {

        System.out.println("source:"+source);
        try {
            path = path + File.separator + name;
            System.out.println("path:"+path);
            FileOutputStream f =new FileOutputStream(path,true);
            f.write(source.getBytes());
            String newline = System.getProperty("line.separator");//换行
            f.write(newline.getBytes());
            f.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*File file = new File(path);
        try {
            if(!file.exists()){
                System.out.println("文件不存在");
                if(!file.createNewFile()) {
                    System.out.println("文件创建失败");
                    return;
                }
                System.out.println("文件创建成功");
            }

            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(source);
            writer.newLine();
            writer.flush();
            writer.close();

        }catch (Exception e) {
            e.printStackTrace();
        }*/
    }
//    public static void main(String[] s){
//        String test = "<script></script>";
//        System.out.println(HtmlUtils.htmlEscape(test));
//        System.out.println(HtmlUtils.htmlEscapeDecimal(test));
//        System.out.println(HtmlUtils.htmlEscapeHex(test));
//        System.out.println(HtmlUtils.htmlUnescape(test));
//    }
}
