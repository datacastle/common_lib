package com.pkbigdata.util;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * 文件操作相关工具类
 *
 * @author 蒋伟
 * @2014-12-26
 */
public class FileUtil {

    /**
     * 处理文件上传
     *
     * @param request 请求Request
     * @return 文件在服务器上保存路径
     */
    public static String upload(HttpServletRequest request) {

        String path = null;
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        /** 页面控件的文件流* */
        MultipartFile multipartFile = multipartRequest.getFile("file");
        return upload(multipartFile,request);

    }

    public static String upload(MultipartFile multipartFile, HttpServletRequest request){
        String path = null;
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        /** 构建文件保存的目录* */
        String logoPathDir = "/upload/" + dateformat.format(new Date());
        /** 得到文件保存目录的真实路径* */
        String logoRealPathDir = request.getSession().getServletContext()
                .getRealPath(logoPathDir);
        /** 根据真实路径创建目录* */
        File logoSaveFile = new File(logoRealPathDir);
        if (!logoSaveFile.exists())
            logoSaveFile.mkdirs();


        String origName = multipartFile.getOriginalFilename();
        if (origName == null || origName.equals(""))
            return path;

        /** 获取文件的后缀* */
        String suffix = origName.substring(
                multipartFile.getOriginalFilename().lastIndexOf("."));
        /** 使用UUID生成文件名称* */
        String logImageName = UUID.randomUUID().toString() + suffix;// 构建文件名称
        /** 拼成完整的文件保存路径加文件* */
        String fileName = logoRealPathDir + File.separator + logImageName;
        File file = new File(fileName);
        try {
            multipartFile.transferTo(file);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("path:" + fileName);
        path = fileName;
        return path;
    }

    /**
     * 判断路径是否存在，不存在创建
     *
     * @param path
     * @return
     */
    public static Boolean MakeDir(String path) {

        File file = new File(path);
        if (!file.exists() && !file.isDirectory()) {
            file.mkdir();
        }
        return true;

    }

    /**
     * byte数组转换成16进制字符串
     *
     * @param src
     * @return
     */
    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder();
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    /**
     * 根据文件流读取图片文件真实类型
     * @param is
     * @return
     */
    public static String getTypeByStream(FileInputStream is) {
        byte[] b = new byte[4];
        try {
            is.read(b, 0, b.length);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String type = bytesToHexString(b).toUpperCase();
        if (type.contains("D0CF11E0")) {
            return "excel or doc";
        }
        return type;
    }
    /**
     * 根据文件流读取图片文件真实类型
     * @param is
     * @return
     */
    public static String getTypeByStream(InputStream is) {
        byte[] b = new byte[4];
        try {
            is.read(b, 0, b.length);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String type = bytesToHexString(b).toUpperCase();
        if (type.contains("D0CF11E0")) {
            return "excel or doc";
        }
        if(type.contains("504B0304")){
            return "excelX or docX";
        }
        return type;
    }


}
