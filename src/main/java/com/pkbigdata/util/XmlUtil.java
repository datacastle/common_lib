package com.pkbigdata.util;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 此包封装了对xml的增删查改，默认使用了Security.xml文件，文件放在根目录下
 *
 * @author jay
 */
public class XmlUtil {

    /**
     * 测试
     */
    public void test() {
        try {
            SAXReader sax = new SAXReader();
            // 获得dom4j的文档对象
            Document root = sax.read(XmlUtil.class.getClassLoader()
                    .getResourceAsStream("Security.xml"));

            String level = null;
            Integer intLevel = 0;
            try {
                level = ((Element) root.selectSingleNode("//level")).getTextTrim();
                intLevel = new Integer(level);
            } catch (NumberFormatException e) {
                System.out.println("安全配置文件中缺少level节点,或者level节点的值为非法格式，只可以是数字");
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }


            if (intLevel == 0) {
                System.out.println("用户等级不存在");
                return;
            }
            for (int i = intLevel; i > 0; i--) {
                //按照用户层级遍历xml
                List<?> list = root.selectNodes("//*[@level='" + i + "']");
                /*
				 * 循环遍历集合中的每一个元素 将每一个元素的元素名和值在控制台中打印出来
				 */
                for (Object obj : list) {
                    Element element = (Element) obj;
                    Element elements = (Element) element.selectSingleNode("..");
                    System.out.println(i + "级用户:" + element.attributeValue("id"));
                    System.out.println("父节点名字：" + elements.getName() + "属性:" + elements.attributeValue("id"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * userlevelName,用户等级参数，例如传入admin，admin是一级用户
     *
     * @param userlevelName
     * @return
     */
    public static Set<String> allowUrl(String userlevelName, String userlevel) {
        Set<String> set = new HashSet<String>();
        SAXReader sax = new SAXReader();
        // 获得dom4j的文档对象
        try {
            Document root = sax.read(XmlUtil.class.getClassLoader()
                    .getResourceAsStream("Security.xml"));

            String level = null;
            Integer intLevel = 0;
            try {
                level = ((Element) root.selectSingleNode("//level")).getTextTrim();
                intLevel = new Integer(level);
            } catch (NumberFormatException e) {
                System.out.println("安全配置文件中缺少level节点,或者level节点的值为非法格式，只可以是数字");
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Element element = (Element) root.selectSingleNode(".//*[@id='" + userlevelName + "']");
            if (element != null) {
                List<?> list = element.selectNodes(".//list/url");

                if (list != null) {
                    for (Object object : list) {
                        Element urlElement = (Element) object;
                        set.add(urlElement.getTextTrim());
                    }
                }

                for (int i = Integer.valueOf(userlevel) + 1; i < intLevel; i++) {
                    List nextElement = element.selectNodes(".//*[@level='" + i + "']");
                    for (Object object : nextElement) {
                        Element nodeElement = (Element) object;
                        List nodeList = nodeElement.selectNodes(".//list/url");
                        if (nodeList != null) {
                            for (Object object2 : nodeList) {
                                Element urlElement = (Element) object;
                                set.add(urlElement.getTextTrim());
                            }
                        }
                    }
                }
            }


        } catch (DocumentException e) {

            e.printStackTrace();
        }
        return set;
    }

    /**
     * 获取安全配置文件里所有限制访问的url
     *
     * @return
     */
    public static Set<String> getAllAllowUrl() {
        Set<String> set = new HashSet<String>();
        SAXReader sax = new SAXReader();
        // 获得dom4j的文档对象
        try {
            Document root = sax.read(XmlUtil.class.getClassLoader()
                    .getResourceAsStream("Security.xml"));
            List list = root.selectNodes("//url");
            for (Object object : list) {
                Element element = (Element) object;
                set.add(element.getTextTrim());
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return set;

    }
}
