package com.pkbigdata.util;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.util.Date;
import java.util.Properties;

/**
 * Created by Administrator on 2016/6/6.
 */
public class EmailUtil {


	/*public static void main(String[] args) {
	boolean f = sendMail("18123364878@163.com", "765023932@qq.com", "邮件测试", "javamail邮件测试", null, "Friend");
	System.out.println(f);
}*/

        private static  final  String fromEmail = "android@datacastle.cn";
        private static  final  String password = "Wsc2014123456789";


        public static boolean sendMail(String[] receiveUser, String subject,
                                       String content, String attachment) {
            boolean flag = false;

            try {
                Properties props = new Properties();
                props.setProperty("mail.smtp.auth", "true");
                props.setProperty("mail.host", "smtp.datacastle.cn");
                props.put("mail.smtp.port", "25");
                props.setProperty("mail.transport.protocol", "smtp");
                Session session = Session.getInstance(props);
                Message message = new MimeMessage(session);
                InternetAddress from = new InternetAddress(fromEmail);
                from.setPersonal(MimeUtility.encodeText("DataCastle"));
                message.setFrom(from);


                if(receiveUser.length==1) {
                    // 单独发送
                    InternetAddress to = new InternetAddress(receiveUser[0]);
                    message.setRecipient(Message.RecipientType.TO, to);
                }else{
                    InternetAddress[] iaRecList = new InternetAddress[receiveUser.length];
                    for (int i = 0;i<receiveUser.length;i++){
                        iaRecList[i] = new InternetAddress(receiveUser[i]);
                    }
                    message.setRecipients(Message.RecipientType.TO, iaRecList);
                }



                // 标题
                message.setSubject(MimeUtility.encodeText(subject));
                message.setSentDate(new Date());
                Multipart mm = new MimeMultipart();
                BodyPart mbpFile = new MimeBodyPart();

                mbpFile.setContent(content, "text/html;charset=UTF-8");
                mm.addBodyPart(mbpFile);
                // 添加文件类附件
                if (attachment != null && !"".equals(attachment)) {
                    BodyPart attachmentBodyPart = new MimeBodyPart();
                    DataSource source = new FileDataSource(attachment);
                    attachmentBodyPart.setDataHandler(new DataHandler(source));
                    // MimeUtility.encodeWord可以避免文件名乱码
                    attachmentBodyPart.setFileName(MimeUtility.encodeWord(source
                            .getName()));
                    mm.addBodyPart(attachmentBodyPart);
                }
                message.setContent(mm);
                message.saveChanges();
                Transport transport = session.getTransport();

                transport.connect(fromEmail, password);
                transport.sendMessage(message, message.getAllRecipients());
                transport.close();
                flag = true;
                return flag;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return flag;
        }

    }


