package com.pkbigdata.util;


import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 访问判断
 * Created by hegang on 2016/1/29.
 */
public class UserBrowser {
    public static final String isPcPort = "桌面用户";//用户是桌面用户
    public static final String isMobilePort = "移动用户";//用户是移动用户

    public static Map<String,String> userbrowser(){
        Map<String,String> map = new HashMap();
        map.put("Firefox/","火狐浏览器");
        map.put("Chrome/","谷歌浏览器");
        map.put("Trident/","IE浏览器");
        map.put("Presto/","Opera浏览器");
        map.put("Maxthon/","傲游浏览器");
        map.put("Android","Android手机浏览器");
        map.put("SymbianOS/","塞班手机浏览器");
        map.put("iOS","苹果手机浏览器");
        map.put("MQQBrowser/","QQ浏览器");
        map.put("SAMSUNG","三星浏览器");
        return map;
    }
    /**
     * 浏览器判断，以及移动还是pc判断
     * @param request
     * @return
     */
    public Map<String,String> userbrowser(HttpServletRequest request){
        Map<String,String> map = new HashMap<String,String>();

        String Agent = request.getHeader("User-Agent");
        String user_browser = "未知浏览器";
        if(Agent!=null) {
            for (String string : userbrowser().keySet()) {
                if (Agent.contains(string)) {
                    user_browser = userbrowser().get(string);
                    break;
                }
            }
        }
        //判断用户是pc，还是移动
        String portType =  "";

        if(Agent != null && Agent.toLowerCase().toString().indexOf("mozilla") != -1){
            portType = isPcPort;
        }else{
            portType = isMobilePort;
        }
        map.put("user_browser",user_browser);
        map.put("portType",portType);
        return map;
    }
    /**
     * 内网ip验证
     * @param ip
     * @return
     */
    public boolean isInner(String ip)
    {
        String reg = "(10|127|172|192)\\.([0-1][0-9]{0,2}|[2][0-5]{0,2}|[3-9][0-9]{0,1})\\.([0-1][0-9]{0,2}|[2][0-5]{0,2}|[3-9][0-9]{0,1})\\.([0-1][0-9]{0,2}|[2][0-5]{0,2}|[3-9][0-9]{0,1})";//正则表达式=。 =、懒得做文字处理了、
        Pattern p = Pattern.compile(reg);
        Matcher matcher = p.matcher(ip);
        boolean flag = matcher.find();
        return flag;
    }
}
