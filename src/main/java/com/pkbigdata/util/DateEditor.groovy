package com.pkbigdata.util

import java.beans.PropertyEditorSupport
import java.sql.Timestamp
import java.text.SimpleDateFormat

/**
 * Created by jayr110 on 2016/3/13.
 */
class DateEditor extends PropertyEditorSupport{

    @Override
    void setAsText(String text) throws IllegalArgumentException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Timestamp timestamp = null;
        try {
            timestamp = new Timestamp(Long.valueOf(text));
        } catch (Exception e) {
            e.printStackTrace()
            try {
                timestamp = new Timestamp(format.parse(text).getTime());
            } catch (Exception e1) {
                e1.printStackTrace()
                timestamp = new Timestamp(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(text).getTime());
            }
        }
        setValue(timestamp);
    }
}
