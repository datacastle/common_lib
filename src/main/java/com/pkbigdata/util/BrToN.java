package com.pkbigdata.util;

/**
 * 将html中的br标签转化为windows换行符\n
 *
 * @author jayr110
 */
public class BrToN {
    public static String brToN(String in) {
        if ("".equals(in) || in == null) {
            return in;
        }
        return in.replaceAll("<br>", "\n")
                .replaceAll("<br />", "\n")
                .replaceAll("<br/>", "\n");
    }

}
