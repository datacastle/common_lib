package com.pkbigdata.util

import com.pkbigdata.entity.DcHost
import com.pkbigdata.entity.DcUser
import org.springframework.validation.FieldError
import org.springframework.validation.ObjectError

import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest
import java.security.MessageDigest
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Created by Administrator on 2016/6/24.
 */
public class CommonUtil {

     static String getCron(Date  date){
        String dateFormat="ss mm HH dd MM ? yyyy";
        return formatDateByPattern(date, dateFormat);
    }

    /***
     *
     * @param date
     * @param dateFormat : e.g:yyyy-MM-dd HH:mm:ss
     * @return
     */
     static String formatDateByPattern(Date date,String dateFormat){
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        String formatTimeStr = null;
        if (date != null) {
            formatTimeStr = sdf.format(date);
        }
        return formatTimeStr;
    }


    /**
     * 判断source是否还有表情和生僻字
     * @param source
     * @return
     */
    static Boolean isutf8mb4(String source){
        if (!source){
            return false
        }
        def flag = false
        (0..source.length()-1).each {
            int c = source.codePointAt(it)
            if (c<0x0000||c>0xffff){
                flag = true
            }
        }
        return flag
    }



    /**
     * 竞赛详情PC端UrL转手机端Url
     * @param url
     * @return
     */
    String pcUserInfoUrlToUserInfoPhoneUrl(String url){
       if(url)
        return  "/static_page/m/userInfo.html";
        return null
    }

    static Cookie getCookie(String key, HttpServletRequest request){
        Cookie[] cookie = request.getCookies();
        if (cookie == null){
            return null;
        }
        for (int i = 0; i < cookie.length; i++) {
            if (key.equals(cookie[i].getName())){
                return cookie[i];
            }
        }
        return null;
    }


    /**
     * 查看一个字符串是否可以转换为数字
     * @param str 字符串
     * @return true 可以; false 不可以
     */
    public static boolean isStr2Num(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public  static  String scoreHidden(String score) {
        //处理科学计数
        if (score.contains("e")||score.contains("E")){
            score = new BigDecimal(score).toPlainString()
        }
        String[] temp = score.split("\\.")
        if (temp.length > 1) {
            String finalS = temp[1]
            if (finalS.length() >= 4) {
                String c = "";
                finalS.toCharArray().eachWithIndex { char entry, int i ->
                    if (i == 0 || i == 1 || i == finalS.length() - 1) {
                        c += "" + entry
                    } else {
                        c += "*";
                    }
                }
               score = temp[0] + "." + c
            }
        }
        return score
    }

    public static String getToken(DcUser user) {
        return encPassword(user.getUsername());

    }

    public static String getToken(DcHost user) {
        return encPassword(user.getName() + user.getTime());

    }



    /**
     * 根据URL获取domain
     * @param url
     * @return
     */
    public static String getDomainForUrl(String url){

        String domainUrl = null;
        if (url == null) {
                return null;
        } else {
            Pattern p = Pattern.compile("(?<=http://|\\.)[^.]*?\\.(com|cn|net|org|biz|info|cc|tv)",Pattern.CASE_INSENSITIVE);
            Matcher matcher = p.matcher(url);
            while(matcher.find()){
                domainUrl = matcher.group();
            }
            return domainUrl;
        }
    }

    /**
     * 删除多级目录文件
     * @param dir
     */
    public static void deleteDir_(File dir){
        //1.列出当前目录下的文件以及文件夹
        File[] files = dir.listFiles();
        //2.对该数组进行遍历
        for(File f:files){
            //3.判断是否有目录，如果有，继续使用该功能遍历，如果不是文件夹，直接删除
            if(f.isDirectory()){
                deleteDir(f);
            }else{
                f.delete()//文件删除
            }
        }
    }

    /**
     * 删除多级目录
     * @param file
     */
    public static void deleteDir(File file){
        if(file.isDirectory()){
            File[] subs = file.listFiles();
            /*for(File sub : subs){
                deleteDir(sub);
            }*/
            for(int i =0;i<subs.length;i++){
                File sub = subs[i];
                deleteDir(sub);
            }

        }
        file.delete();
    }

    /**
     * 判断一个字符串是不是邮箱
     * @param email
     * @return
     */
    static Boolean isEmail(String email){
        def regex = ~'''[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[\\w](?:[\\w-]*[\\w])?'''
        return email==~regex
    }
    /**
     * 增加虚拟注册用户数量
     * @param num
     * @return
     */
    static int getRegisterNum(int  num){
        Long start_time = TimeExchange.StringToTimestamp("2016-12-30 13:59:59")
       // Long insecNum= (System.currentTimeMillis() -  start_time*1000)/(1000*60)*(1000/1440)+3000
        //return num+(insecNum>10000?10000:insecNum) as int
        if (System.currentTimeMillis()>start_time*1000){
            if((num+10135)>30000){
                num = num+10135
            }else {
                num = 30000
            }
        }else {
            num = num+10135
        }
        return num
    }

    public static String getEntityId() {
        return UUID.randomUUID().toString();
    }

//    public static void main(String[] args) {
//        System.out.println(verifyIdCard("510902199012027378"));
//    }

    /**
     * 对密码进行md5加密
     *
     * @param password 明文密码
     * @return 加密后的密码
     */
    public static String encPassword(String password) {

        char[] hexDigits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                            'a', 'b', 'c', 'd', 'e', 'f']
        byte[] strTemp = password.getBytes();
        try {
            MessageDigest mdTemp = MessageDigest.getInstance("MD5");
            mdTemp.update(strTemp);
            byte[] md = mdTemp.digest();
            int j = md.length;
            char[] str = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String newEnPassword(String password){
        encPassword($/38sjxn93^/$+encPassword(encPassword(password)+ $/./o12`\,asf/$))
    }



    /**
     * 根据帐户名生成UserId
     *
     * @param account 帐户名。如邮箱、昵称等
     * @return 唯一标志的userid
     * @throws UnsupportedEncodingException
     */
    public static String createUserId(String account) throws UnsupportedEncodingException {
        return URLEncoder.encode(account, "utf-8");
    }

    /**
     * 根据userId生成帐户名
     */
    public static String userIdToAccount(String userId) throws UnsupportedEncodingException {
        return URLDecoder.decode(userId, "utf-8");
    }

    /**
     * 获得long型的当前时间
     *
     * @return long型的当前时间
     */
    public static Timestamp getCurrentTime() {
        return new Timestamp(System.currentTimeMillis());
    }

    /**
     * 打印数据验证错误
     *
     * @param list
     * @return
     */
    public static String getBindingResultError(List<ObjectError> list) {
        String error = "";
        for (ObjectError objectError : list) {
            error += objectError.getDefaultMessage();
            error += "<br/>";
        }
        return error;
    }
    public static String getBindingResultErrorForMap(List<FieldError> list) {
        String error = "";
        for (ObjectError objectError : list) {
            error += objectError.getDefaultMessage();
            error += "<br/>";
        }
        return error;
//        Map<String,String> map = new HashMap<>();
//        for (FieldError objectError : list) {
//            map.put(objectError.getField(),objectError.getDefaultMessage());
//        }
//        return JsonToMap.beanToJson(map);
    }




    public static boolean isTokenEqual(DcUser user, String token) {
        String createdToken = getToken(user);
        return createdToken.equals(token);
    }

    /**
     * 从指定时间(beginTime)开始，判断当前是否超过timegap数量的分钟数
     * @author 李龙飞
     * @param beginTime  开始时间
     * @param timeGap 时间间隔
     * @return
     */
    public static boolean isOverTimeGap(Date beginTime, int timeGap) {//

        boolean result = true;
        if (beginTime != null) {
            Calendar calendarLast = Calendar.getInstance();
            calendarLast.setTime(beginTime);
            calendarLast.add(Calendar.MINUTE, timeGap);
            Calendar calendarNow = Calendar.getInstance();
            result=calendarLast.before(calendarNow);
        }
        return result;
    }



    /**
     * 从指定时间(beginTime)开始，判断现在是否超过timegap数量的天数
     * @author 李龙飞
     * @param beginTime 开始时间
     * @param timeGap 间隔的天数
     * @return
     */
    public static boolean isOverTimeGapDay(Date beginTime,int timeGap){//

        boolean result=true;
        if(beginTime!=null) {
            Calendar calendarLast = Calendar.getInstance();
            calendarLast.setTime(beginTime);
            calendarLast.add(Calendar.DAY_OF_MONTH, timeGap);
            Calendar calendarNow = Calendar.getInstance();
            result = calendarLast.before(calendarNow);
        }
        return result;
    }

    /**
     * 判断goalStr是否符合正则表达式regex
     * @author  李龙飞
     * @param regex 正则表达式
     * @param goalStr 目标字符串
     * @return
     */
    public static boolean isMatch(String regex, String goalStr) {

        if (goalStr==null||goalStr.length()==0)
            return false;
        boolean isMatch = false;
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(goalStr);
        isMatch = matcher.matches();
        return isMatch;
    }

    public static String changeNullToDefaul(String value){
        return  value==null?"无":value;
    }
    public static Integer changeNullToDefaul(Integer value){
        return value==null?0:value;
    }



    public static boolean verifyIdCard(String idCord) {
        //String cardNo = "360481197512040035"; // 36900119751204003X // 360481197512040035
        // 1.将身份证号码前面的17位数分别乘以不同的系数。从第一位到第十七位的系数分别为：7 9 10 5 8 4 2 1 6 3 7 9 10 5 8 4 2
        idCord = idCord.toUpperCase();
        int[] intArr = [ 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 ]
        int sum = 0;
        try {
            for (int i = 0; i < intArr.length; i++) {
                // 2.将这17位数字和系数相乘的结果相加。
                sum += Character.digit(idCord.charAt(i), 10) * intArr[i];
                // System.out.println((cardNo.charAt(i) - '0') + " x " + intArr[i] + " = " + (cardNo.charAt(i) - '0') *
                // intArr[i]);
            }
        }catch (Exception e){
            return false;
        }

        System.out.println("Totally sum：" + sum);
        // 3.用加出来和除以11，看余数是多少？
        int mod = sum % 11;
        System.out.println("Totally sum%11 = " + mod);
        // 4.余数只可能有0 1 2 3 4 5 6 7 8 9 10这11个数字。其分别对应的最后一位身份证的号码为1 0 X 9 8 7 6 5 4 3 2。
        int[] intArr2 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        int[] intArr3 = [ 1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2 ]
        String matchDigit = "";
        for (int i = 0; i < intArr2.length; i++) {
            int j = intArr2[i];
            if (j == mod) {
                matchDigit = String.valueOf(intArr3[i]);
                if (intArr3[i] > 57) {
                    matchDigit = String.valueOf((char) intArr3[i]);
                }
            }
        }

        if (matchDigit.equals(idCord.substring(idCord.length() - 1))) {


            return true;
        } else {

            return false;
        }

        // 5.通过上面得知如果余数是2，就会在身份证的第18位数字上出现罗马数字的Ⅹ。如果余数是10，身份证的最后一位号码就是2。
    }








    /*public static void main(String[] args)  {
        Map<String ,String> map=new HashMap<String ,String>();
        try {
            map.put("test","eeeee");
            encodeMapValue("aaaaa", "ssss", map);
            System.out.println(map.get("mail") + " " + map.get("time"));
            System.out.println(decode("aaaaa", URLDecoder.decode("4apAttAHiT4%3D")));;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    public static void loadClass(){

    }

    /* public static boolean isColumnMatchType(String value){
         boolean result=true;
         switch(type){
             case 1:if (!value.equals("0")) result=false;break;
             case 2:;break;
             case 3:;break;
             case 4:;break;
             case 5:;break;
             case 6:;break;
             case 7:;break;
             case 8:;break;
             case 9:;break;
             case 10:;
         }

         return  result;
     }*/

    /**
     * 获取访问的ip地址
     * @param request
     * @return
     */
    public static String getRemortIP(HttpServletRequest request) {
        if (request.getHeader("x-forwarded-for") == null) {
            return request.getRemoteAddr();
        }
        return request.getHeader("x-forwarded-for");
    }


    /**
     * 检测是否是移动设备访问
     *
     * @Title: check
     * @Date : 2014-7-7 下午01:29:07
     * @param userAgent 浏览器标识
     * @return true:移动设备接入，false:pc端接入
     */
    public static boolean checkMobile(String userAgent){
        // \b 是单词边界(连着的两个(字母字符 与 非字母字符) 之间的逻辑上的间隔),
        // 字符串在编译时会被转码一次,所以是 "\\b"
        // \B 是单词内部逻辑间隔(连着的两个字母字符之间的逻辑上的间隔)
        String phoneReg = '''\\b(ip(hone|od)|android|opera m(ob|in)i
                              |windows (phone|ce)|blackberry|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp
                            |laystation portable)|nokia|fennec|htc[-_]
                            |mobile|up.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\\b'''

        String tableReg = "\\b(ipad|tablet|(Nexus 7)|up.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\\b";

        //移动设备正则匹配：手机端、平板
        Pattern phonePat = Pattern.compile(phoneReg, Pattern.CASE_INSENSITIVE);
        Pattern tablePat = Pattern.compile(tableReg, Pattern.CASE_INSENSITIVE);

        if(null == userAgent){
            userAgent = "";
        }
        // 匹配
        Matcher matcherPhone = phonePat.matcher(userAgent);
        Matcher matcherTable = tablePat.matcher(userAgent);
        if(matcherPhone.find() || matcherTable.find()){
            return true;
        } else {
            return false;
        }
    }

    /**
     * 计算竞赛状态，-1表示未通过创建,0表示刚创建，1表示预览，2表示活跃，3表示历史
     * @param passCreate
     * @param preview
     * @param published
     * @param endTime
     * @return
     */
    public static Integer  CheckCompetitionState(Boolean passCreate,Boolean preview,Boolean published,Timestamp endTime){
        if (passCreate ==false){
            return -1;
        }
        if (passCreate == true && preview == false ){
            return 0;
        }
        if (passCreate == true && preview == true && published == false){
            return 1;
        }
        if (passCreate == true && preview == true && published == true && endTime.getTime()>System.currentTimeMillis()){
            return 2;
        }
        if (passCreate == true && preview == true && published == true && endTime.getTime()<System.currentTimeMillis()){
            return 3;
        }
        return 0;
    }

    /**
     * 获取竞赛的文字状态
     * @param state
     * @return
     */
    public static  String getStringCmptState(Integer state){
        Map<Integer,String> stateCmpt = new HashMap<Integer,String>();
        stateCmpt.put(-1,"未通过创建");
        stateCmpt.put(0,"初始");
        stateCmpt.put(1,"预览");
        stateCmpt.put(2,"活跃");
        stateCmpt.put(3,"历史");
        return stateCmpt.get(state);
    }

    /**
     * 判断当前环境是不是windows
     * @return
     */
    public static boolean getWindows(){
        if (System.getProperty("os.name").contains("dows")){
            return true;
        }
        return false;
    }

    /**
     * 将实体list转化为map，某一个属性作为key，实体作为value
     * @param beans
     * @return
     */
    static Map listBeanToMap(List beans,String key){
        def map = new HashMap()
        beans.each {it->
            map.put(it[key],it)
        }
        return map
    }

    /**
     * 加密长链接
     * @param number
     * @return
     */
      public static String  encLongUrl(){
          LocalDateTime nowTime =LocalDateTime.now()
                  int day= nowTime.dayOfMonth;
                  int hours= nowTime.hour;
                  int minutes=nowTime.minute
                  int seconds= nowTime.second
                  int milliseconds = nowTime.nano
          def getChar={int number->
              String str = "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
              char[] charArray = str.toCharArray()
              if(number>=charArray.length){
                  number = number%62;
              }
              return charArray[number].toString()
          }
         return  getChar(day)+getChar(hours)+getChar(minutes)+getChar(seconds)+getChar(milliseconds)
    }

    /**
     * 获取当前语言
     * @param request
     * @return
     */
    public  static  String getDcLang(HttpServletRequest request){
       String t =  request.getSession().getAttribute("dcLang").toString()
        return  t;
    }

}
