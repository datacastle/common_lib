package com.pkbigdata.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * 图片压缩处理
 *
 * @author 赵仁杰
 */
public class ImgCompress {
    private Image img;
    private int width;
    private int height;
    private String savePath;

    /**
     * 构造函数
     *
     * @param fileName 源文件的地址
     * @param savePath 处理后的文件地址
     */
    public ImgCompress(String fileName, String savePath) throws IOException {
        File file = new File(fileName);// 读入文件  
        img = ImageIO.read(file);      // 构造Image对象  
        width = img.getWidth(null);    // 得到源图宽  
        height = img.getHeight(null);  // 得到源图长  
        this.savePath = savePath;

    }

    /**
     * 构造函数
     *
     * @param file     源文件
     * @param savePath 处理后的文件地址
     */
    public ImgCompress(File file, String savePath) throws IOException {
        img = ImageIO.read(file);      // 构造Image对象  
        width = img.getWidth(null);    // 得到源图宽  
        height = img.getHeight(null);  // 得到源图长  
        this.savePath = savePath;
    }

    /**
     * 按照宽度还是高度进行压缩
     *
     * @param w int 最大宽度
     * @param h int 最大高度
     */
    public byte[] resizeFix(int w, int h) throws IOException {

//        if (width / height > w / h) {  
//            resizeByWidth(w);  
//        } else {  
//            resizeByHeight(h);  
//        }  
        return resize(w, h);
    }

    /**
     * 以宽度为基准，等比例放缩图片
     *
     * @param w int 新宽度
     */
    public void resizeByWidth(int w) throws IOException {
        int h = (int) (height * w / width);
        resize(w, h);
    }

    /**
     * 以高度为基准，等比例缩放图片
     *
     * @param h int 新高度
     */
    public void resizeByHeight(int h) throws IOException {
        int w = (int) (width * h / height);
        resize(w, h);
    }

    /**
     * 强制压缩/放大图片到固定的大小
     *
     * @param w int 新宽度
     * @param h int 新高度
     */
    public byte[] resize(int w, int h) throws IOException {
        // SCALE_SMOOTH 的缩略算法 生成缩略图片的平滑度的 优先级比速度高 生成的图片质量比较好 但速度慢  
        BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        image.getGraphics().drawImage(img, 0, 0, w, h, null); // 绘制缩小后的图  

        //File destFile = new File(this.savePath);  
        //FileOutputStream out = new FileOutputStream(destFile); // 输出到文件流  

        ByteArrayOutputStream outs = new ByteArrayOutputStream();
        boolean flag = ImageIO.write(image, "gif", outs);
        byte[] b = outs.toByteArray();
        // 可以正常实现bmp、png、gif转jpg  
        // JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
        // encoder.encode(image); // JPEG编码

        //out.close();  
        return b;
    }
}
