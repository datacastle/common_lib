package com.pkbigdata.entity.activity;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * DcActivityLottery entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "dc_activity_lottery", catalog = "dc_activity")
public class DcActivityLottery implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer prize;//奖品id
	private Integer activityid;//活动id
	private Integer userid;//获奖用户
	private Timestamp time;//获奖时间
	private String orderNumber;
	private String type;
	private Boolean active;

	// Constructors

	/** default constructor */
	public DcActivityLottery() {
	}

	/** full constructor */
	public DcActivityLottery(Integer prize, Integer activityid, Integer userid,
			Timestamp time) {
		this.prize = prize;
		this.activityid = activityid;
		this.userid = userid;
		this.time = time;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "prize")
	public Integer getPrize() {
		return this.prize;
	}

	public void setPrize(Integer prize) {
		this.prize = prize;
	}

	@Column(name = "activityid")
	public Integer getActivityid() {
		return this.activityid;
	}

	public void setActivityid(Integer activityid) {
		this.activityid = activityid;
	}

	@Column(name = "userid")
	public Integer getUserid() {
		return this.userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	@Column(name = "time", length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Column(name = "order_number")
	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	@Column(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "active")
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}