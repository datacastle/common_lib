package com.pkbigdata.entity.activity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ck on 2016/12/29.
 */
@Entity
@Table(name = "dc_activity_rank",catalog = "dc_activity")
public class DcActivityRank implements java.io.Serializable{
    private Integer id;
    private Integer teamId;
    private String  teamName;
    private Integer leaderId;
    private String leaderName;
    private String phone;
    private String username;
    private Integer startRank;
    private Integer endRank;
    private Integer diffRank;
    private Integer cmptId;
    private Timestamp createTime;
    private Timestamp subTime;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "team_id")
    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    @Column(name = "team_name")
    public String getTeamName() {
        return teamName;
    }


    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    @Column(name="leader_id")
    public Integer getLeaderId() {
        return leaderId;
    }

    public void setLeaderId(Integer leaderId) {
        this.leaderId = leaderId;
    }

    @Column(name="leader_name")
    public String getLeaderName() {
        return leaderName;
    }

    public void setLeaderName(String leaderName) {
        this.leaderName = leaderName;
    }

    @Column(name="phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name="username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name="start_rank")
    public Integer getStartRank() {
        return startRank;
    }

    public void setStartRank(Integer startRank) {
        this.startRank = startRank;
    }

    @Column(name="end_rank")
    public Integer getEndRank() {
        return endRank;
    }

    public void setEndRank(Integer endRank) {
        this.endRank = endRank;
    }

    @Column(name="diff_rank")
    public Integer getDiffRank() {
        return diffRank;
    }

    public void setDiffRank(Integer diffRank) {
        this.diffRank = diffRank;
    }

    @Column(name="cmpt_id")
    public Integer getCmptId() {
        return cmptId;
    }

    public void setCmptId(Integer cmptId) {
        this.cmptId = cmptId;
    }

    @Column(name="create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Column(name="sub_time")
    public Timestamp getSubTime() {
        return subTime;
    }

    public void setSubTime(Timestamp subTime) {
        this.subTime = subTime;
    }


}
