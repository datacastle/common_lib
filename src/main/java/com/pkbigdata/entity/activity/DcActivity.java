package com.pkbigdata.entity.activity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by 李龙飞 on 2015-08-28.
 */
@Entity
@Table(name = "dc_activity", catalog = "dc_activity")
public class DcActivity implements java.io.Serializable{
    private int id;//活动Id
    private String name;//活动名称
    private String introduce;//活动介绍
    private String other;//其他
    private Integer belong;//属于哪个主活动
    private Timestamp start;//开始时间
    private Timestamp end;//结束时间

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, insertable = true, updatable = true, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "introduce", nullable = true, insertable = true, updatable = true, length = 65535)
    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    @Basic
    @Column(name = "other", nullable = true, insertable = true, updatable = true, length = 65535)
    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    @Basic
    @Column(name = "belong", nullable = true, insertable = true, updatable = true)
    public Integer getBelong() {
        return belong;
    }

    public void setBelong(Integer belong) {
        this.belong = belong;
    }

    @Basic
    @Column(name = "start", nullable = true, insertable = true, updatable = true)
    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    @Basic
    @Column(name = "end", nullable = true, insertable = true, updatable = true)
    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DcActivity that = (DcActivity) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (introduce != null ? !introduce.equals(that.introduce) : that.introduce != null) return false;
        if (other != null ? !other.equals(that.other) : that.other != null) return false;
        if (belong != null ? !belong.equals(that.belong) : that.belong != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (introduce != null ? introduce.hashCode() : 0);
        result = 31 * result + (other != null ? other.hashCode() : 0);
        result = 31 * result + (belong != null ? belong.hashCode() : 0);
        return result;
    }
}
