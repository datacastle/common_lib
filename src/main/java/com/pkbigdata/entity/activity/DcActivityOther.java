package com.pkbigdata.entity.activity;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * DcActivityOther entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "dc_activity_other", catalog = "dc_activity")
public class DcActivityOther implements java.io.Serializable {

	// Fields

	private Integer id;
	private String one;
	private String two;
	private String type;
	private Timestamp time;

	// Constructors

	/** default constructor */
	public DcActivityOther() {
	}

	/** full constructor */
	public DcActivityOther(String one, String two, String type, Timestamp time) {
		this.one = one;
		this.two = two;
		this.type = type;
		this.time = time;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "one")
	public String getOne() {
		return this.one;
	}

	public void setOne(String one) {
		this.one = one;
	}

	@Column(name = "two")
	public String getTwo() {
		return this.two;
	}

	public void setTwo(String two) {
		this.two = two;
	}

	@Column(name = "type")
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "time", length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

}