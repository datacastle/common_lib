package com.pkbigdata.entity.activity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by 李龙飞 on 2015-08-27.
 */
@Entity
@Table(name = "dc_activity_honoree", catalog = "dc_activity")
public class DcActivityHonoree implements java.io.Serializable{
    private int id;//兑奖id
    private int honoreeId;//兑换人id
    private int prize;//奖品id
    private String receiveAddress;//收货地址
    private String phone;//收货电话
    private String status;//兑换状态
    private Timestamp exchangeTime;//兑奖时间
    private Timestamp sendTime;//发货时间
    private String name;//收件人姓名
    private String prizeInfo;//奖品描述
    private Integer activityId;//活动Id
    private Integer times;//奖品数量

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "honoree_id", nullable = false, insertable = true, updatable = true)
    public int getHonoreeId() {
        return honoreeId;
    }

    public void setHonoreeId(int honoreeId) {
        this.honoreeId = honoreeId;
    }

    @Basic
    @Column(name = "prize", nullable = false, insertable = true, updatable = true)
    public int getPrize() {
        return prize;
    }

    public void setPrize(int prize) {
        this.prize = prize;
    }

    @Basic
    @Column(name = "receive_address", nullable = true, insertable = true, updatable = true, length = 255)
    public String getReceiveAddress() {
        return receiveAddress;
    }

    public void setReceiveAddress(String receiveAddress) {
        this.receiveAddress = receiveAddress;
    }

    @Basic
    @Column(name = "phone", nullable = true, insertable = true, updatable = true, length = 100)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "status", nullable = true, insertable = true, updatable = true, length = 10)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "exchange_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getExchangeTime() {
        return exchangeTime;
    }

    public void setExchangeTime(Timestamp exchangeTime) {
        this.exchangeTime = exchangeTime;
    }

    @Basic
    @Column(name = "send_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getSendTime() {
        return sendTime;
    }

    public void setSendTime(Timestamp sendTime) {
        this.sendTime = sendTime;
    }

    @Basic
    @Column(name = "name", nullable = true, insertable = true, updatable = true, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "prize_info", nullable = true, insertable = true, updatable = true, length = 255)
    public String getPrizeInfo() {
        return prizeInfo;
    }

    public void setPrizeInfo(String prizeInfo) {
        this.prizeInfo = prizeInfo;
    }

    @Basic
    @Column(name = "activity_id", nullable = true, insertable = true, updatable = true)
    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    @Column(name = "times")
    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DcActivityHonoree that = (DcActivityHonoree) o;

        if (id != that.id) return false;
        if (honoreeId != that.honoreeId) return false;
        if (prize != that.prize) return false;
        if (receiveAddress != null ? !receiveAddress.equals(that.receiveAddress) : that.receiveAddress != null)
            return false;
        if (phone != null ? !phone.equals(that.phone) : that.phone != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (exchangeTime != null ? !exchangeTime.equals(that.exchangeTime) : that.exchangeTime != null) return false;
        if (sendTime != null ? !sendTime.equals(that.sendTime) : that.sendTime != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (prizeInfo != null ? !prizeInfo.equals(that.prizeInfo) : that.prizeInfo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + honoreeId;
        result = 31 * result + prize;
        result = 31 * result + (receiveAddress != null ? receiveAddress.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (exchangeTime != null ? exchangeTime.hashCode() : 0);
        result = 31 * result + (sendTime != null ? sendTime.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (prizeInfo != null ? prizeInfo.hashCode() : 0);
        return result;
    }
}
