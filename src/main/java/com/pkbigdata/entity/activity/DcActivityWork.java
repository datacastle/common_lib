package com.pkbigdata.entity.activity;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * DcActivityWork entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "dc_activity_work", catalog = "dc_activity")
public class DcActivityWork implements java.io.Serializable {

	// Fields

	private Integer id;
	private Timestamp time;
	private Integer activityId;
	private Integer teamId;
	private Integer userId;
	private Integer rank;

	// Constructors

	/** default constructor */
	public DcActivityWork() {
	}

	/** full constructor */
	public DcActivityWork(Timestamp time, Integer activityId, Integer userId,
			Integer rank) {
		this.time = time;
		this.activityId = activityId;
		this.userId = userId;
		this.rank = rank;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "time", length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Column(name = "activity_id")
	public Integer getActivityId() {
		return this.activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}

	@Column(name = "user_id")
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "rank")
	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	@Column(name = "team_id")
	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}
}