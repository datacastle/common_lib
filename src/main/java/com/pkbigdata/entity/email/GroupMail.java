package com.pkbigdata.entity.email;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by jay on 2016/8/30.
 */
@Entity
@Table(name = "group_mail", catalog = "xyemail")
public class GroupMail implements java.io.Serializable{
    private int id;
    private String file;
    private String content;
    private String sub;
    private Timestamp createTime;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "file")
    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    @Basic
    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "sub")
    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    @Basic
    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GroupMail groupMail = (GroupMail) o;

        if (id != groupMail.id) return false;
        if (file != null ? !file.equals(groupMail.file) : groupMail.file != null) return false;
        if (content != null ? !content.equals(groupMail.content) : groupMail.content != null) return false;
        if (sub != null ? !sub.equals(groupMail.sub) : groupMail.sub != null) return false;
        if (createTime != null ? !createTime.equals(groupMail.createTime) : groupMail.createTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (file != null ? file.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (sub != null ? sub.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        return result;
    }
}
