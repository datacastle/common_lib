package com.pkbigdata.entity.bbs;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BbsMission entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_mission", catalog = "bbs")

public class BbsMission implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer score;//签到随机获得积分
	private Integer authorId;//签到人id
	private Timestamp inTime;//签到时间
	private Integer day;//连续签到天数
	private String remarks;

	// Constructors

	/** default constructor */
	public BbsMission() {
	}

	/** minimal constructor */
	public BbsMission(Integer score, Integer authorId, Timestamp inTime, Integer day) {
		this.score = score;
		this.authorId = authorId;
		this.inTime = inTime;
		this.day = day;
	}

	/** full constructor */
	public BbsMission(Integer score, Integer authorId, Timestamp inTime, Integer day, String remarks) {
		this.score = score;
		this.authorId = authorId;
		this.inTime = inTime;
		this.day = day;
		this.remarks = remarks;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "score", nullable = false)

	public Integer getScore() {
		return this.score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Column(name = "author_id", nullable = false)

	public Integer getAuthorId() {
		return this.authorId;
	}

	public void setAuthorId(Integer authorId) {
		this.authorId = authorId;
	}

	@Column(name = "in_time", nullable = false, length = 19)

	public Timestamp getInTime() {
		return this.inTime;
	}

	public void setInTime(Timestamp inTime) {
		this.inTime = inTime;
	}

	@Column(name = "day", nullable = false)

	public Integer getDay() {
		return this.day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	@Column(name = "remarks", length = 500)

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}