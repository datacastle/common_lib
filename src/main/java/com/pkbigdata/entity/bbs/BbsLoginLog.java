package com.pkbigdata.entity.bbs;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BbsLoginLog entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_login_log", catalog = "bbs")

public class BbsLoginLog implements java.io.Serializable {

	// Fields

	private Integer id;
	private String ip;//登录者ip
	private Integer userId;//登录用户id
	private String username;//用户名
	private Timestamp loginTime;//主站登录时间
	private Timestamp createTime;//登录时间
	private String source;//渠道
	private Timestamp loginOutTime;
	private Double onlineTime;//在线时长
	private String remarks;
	private String sessionId;

	// Constructors

	/** default constructor */
	public BbsLoginLog() {
	}

	/** full constructor */
	public BbsLoginLog(String ip, Integer userId, String username, String source, Timestamp loginTime,
			Timestamp createTime, Timestamp loginOutTime, Double onlineTime, String remarks) {
		this.ip = ip;
		this.userId = userId;
		this.username = username;
		this.source = source;
		this.loginTime = loginTime;
		this.createTime = createTime;
		this.loginOutTime = loginOutTime;
		this.onlineTime = onlineTime;
		this.remarks = remarks;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "ip")

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Column(name = "user_id")

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "username")

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "source")

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Column(name = "login_time", length = 19)

	public Timestamp getLoginTime() {
		return this.loginTime;
	}

	public void setLoginTime(Timestamp loginTime) {
		this.loginTime = loginTime;
	}

	@Column(name = "create_time", length = 19)

	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Column(name = "login_out_time", length = 19)

	public Timestamp getLoginOutTime() {
		return this.loginOutTime;
	}

	public void setLoginOutTime(Timestamp loginOutTime) {
		this.loginOutTime = loginOutTime;
	}

	@Column(name = "online_time", precision = 22, scale = 0)

	public Double getOnlineTime() {
		return this.onlineTime;
	}

	public void setOnlineTime(Double onlineTime) {
		this.onlineTime = onlineTime;
	}

	@Column(name = "remarks")

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Column(name = "session_id")
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
}