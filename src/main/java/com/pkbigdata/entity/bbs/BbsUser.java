package com.pkbigdata.entity.bbs;

import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * BbsUser entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_user", catalog = "bbs")

public class BbsUser implements java.io.Serializable {

	// Fields

	private Integer id;
	private String nickname;//用户昵称
	private String username;//用户名
	private String password;//密码
	private Integer score;//积分
	private String avatar;//头像
	private Date mission;//签到日期
	private Timestamp inTime;
	private String url;//个人主页
	private String signature;//个性签名
	private String userType;//用户类型（0:普通用户1:管理员）
	private String email;

	// Constructors

	/** default constructor */
	public BbsUser() {
	}

	/** minimal constructor */
	public BbsUser(String nickname, Integer score, Timestamp inTime, String userType) {
		this.nickname = nickname;
		this.score = score;
		this.inTime = inTime;
		this.userType = userType;
	}

	/** full constructor */
	public BbsUser(String username, String password, String nickname, String email, Integer score, String avatar,
			Date mission, Timestamp inTime, String url, String signature, String userType) {
		this.username = username;
		this.password = password;
		this.nickname = nickname;
		this.email = email;
		this.score = score;
		this.avatar = avatar;
		this.mission = mission;
		this.inTime = inTime;
		this.url = url;
		this.signature = signature;
		this.userType = userType;
	}

	// Property accessors
	@Id
	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "username")

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "password")

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "nickname", nullable = false, length = 50)

	public String getNickname() {
		return this.nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	@Column(name = "email")

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "score", nullable = false)

	public Integer getScore() {
		return this.score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Column(name = "avatar")

	public String getAvatar() {
		return this.avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "mission", length = 10)

	public Date getMission() {
		return this.mission;
	}

	public void setMission(Date mission) {
		this.mission = mission;
	}

	@Column(name = "in_time", nullable = false, length = 19)

	public Timestamp getInTime() {
		return this.inTime;
	}

	public void setInTime(Timestamp inTime) {
		this.inTime = inTime;
	}

	@Column(name = "url")

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "signature", length = 1000)

	public String getSignature() {
		return this.signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	@Column(name = "user_type", nullable = false, length = 1)

	public String getUserType() {
		return this.userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

}