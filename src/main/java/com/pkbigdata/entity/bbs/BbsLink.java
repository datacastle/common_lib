package com.pkbigdata.entity.bbs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BbsLink entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_link", catalog = "bbs")

public class BbsLink implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;//外链名称
	private String url;//外链路径
	private Integer displayIndex;//排序

	// Constructors

	/** default constructor */
	public BbsLink() {
	}

	/** full constructor */
	public BbsLink(String name, String url, Integer displayIndex) {
		this.name = name;
		this.url = url;
		this.displayIndex = displayIndex;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 45)

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "url", nullable = false)

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "display_index", nullable = false)

	public Integer getDisplayIndex() {
		return this.displayIndex;
	}

	public void setDisplayIndex(Integer displayIndex) {
		this.displayIndex = displayIndex;
	}

}