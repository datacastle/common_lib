package com.pkbigdata.entity.bbs;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BbsCollect entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_collect", catalog = "bbs")

public class BbsCollect implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer tid;//帖子id
	private Integer authorId;//收藏者id
	private Timestamp inTime;

	// Constructors

	/** default constructor */
	public BbsCollect() {
	}

	/** full constructor */
	public BbsCollect(Integer tid, Integer authorId, Timestamp inTime) {
		this.tid = tid;
		this.authorId = authorId;
		this.inTime = inTime;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "tid", nullable = false)

	public Integer getTid() {
		return this.tid;
	}

	public void setTid(Integer tid) {
		this.tid = tid;
	}

	@Column(name = "author_id", nullable = false)

	public Integer getAuthorId() {
		return this.authorId;
	}

	public void setAuthorId(Integer authorId) {
		this.authorId = authorId;
	}

	@Column(name = "in_time", nullable = false, length = 19)

	public Timestamp getInTime() {
		return this.inTime;
	}

	public void setInTime(Timestamp inTime) {
		this.inTime = inTime;
	}

}