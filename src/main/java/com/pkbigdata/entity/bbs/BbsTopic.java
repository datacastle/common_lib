package com.pkbigdata.entity.bbs;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BbsTopic entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_topic", catalog = "bbs")

public class BbsTopic implements java.io.Serializable {

	// Fields

	private Integer id;
	private String oldId;//dc2.0帖子id
	private Integer SId;//版块id
	private String title;//帖子标题
	private String content;//贴子内容
	private Timestamp inTime;//创建时间
	private Timestamp modifyTime;//修改时间
	private Timestamp lastReplyTime;//最后回复时间
	private String lastReplyAuthorId;//最后回复用户id
	private Integer reply;//回复次数
	private Integer view;//查看次数
	private Integer clickNum;//点赞次数
	private Integer authorId;//作者id
	private Integer reposted;//1：转载 0：原创
	private String originalUrl;//原文连接
	private Integer top;//1置顶 0默认
	private Integer good;//1精华 0默认
	private Integer showStatus;//1显示0不显示
	private Integer complexStatus;//综合（0默认1综合）
	private Integer loginToView;//登录可查看
	private Integer backToView;//回复可查看
	private Boolean isdelete;
	private Integer type;//0:普通1:通知2：公告

	// Constructors

	/** default constructor */
	public BbsTopic() {
	}

	/** minimal constructor */
	public BbsTopic(Integer SId, String title, String content, Timestamp inTime, Integer authorId, Integer loginToView,
			Integer backToView) {
		this.SId = SId;
		this.title = title;
		this.content = content;
		this.inTime = inTime;
		this.authorId = authorId;
		this.loginToView = loginToView;
		this.backToView = backToView;
	}

	/** full constructor */
	public BbsTopic(String oldId, Integer SId, String title, String content, Timestamp inTime, Timestamp modifyTime,
			Timestamp lastReplyTime, String lastReplyAuthorId, Integer reply, Integer view, Integer clickNum,
			Integer authorId, Integer reposted, String originalUrl, Integer top, Integer type, Integer good,
			Integer showStatus, Integer complexStatus, Integer loginToView, Integer backToView, Boolean isdelete) {
		this.oldId = oldId;
		this.SId = SId;
		this.title = title;
		this.content = content;
		this.inTime = inTime;
		this.modifyTime = modifyTime;
		this.lastReplyTime = lastReplyTime;
		this.lastReplyAuthorId = lastReplyAuthorId;
		this.reply = reply;
		this.view = view;
		this.clickNum = clickNum;
		this.authorId = authorId;
		this.reposted = reposted;
		this.originalUrl = originalUrl;
		this.top = top;
		this.type = type;
		this.good = good;
		this.showStatus = showStatus;
		this.complexStatus = complexStatus;
		this.loginToView = loginToView;
		this.backToView = backToView;
		this.isdelete = isdelete;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "old_id")

	public String getOldId() {
		return this.oldId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId;
	}

	@Column(name = "s_id", nullable = false)

	public Integer getSId() {
		return this.SId;
	}

	public void setSId(Integer SId) {
		this.SId = SId;
	}

	@Column(name = "title", nullable = false)

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "content", nullable = false)

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "in_time", nullable = false, length = 19)

	public Timestamp getInTime() {
		return this.inTime;
	}

	public void setInTime(Timestamp inTime) {
		this.inTime = inTime;
	}

	@Column(name = "modify_time", length = 19)

	public Timestamp getModifyTime() {
		return this.modifyTime;
	}

	public void setModifyTime(Timestamp modifyTime) {
		this.modifyTime = modifyTime;
	}

	@Column(name = "last_reply_time", length = 19)

	public Timestamp getLastReplyTime() {
		return this.lastReplyTime;
	}

	public void setLastReplyTime(Timestamp lastReplyTime) {
		this.lastReplyTime = lastReplyTime;
	}

	@Column(name = "last_reply_author_id", length = 32)

	public String getLastReplyAuthorId() {
		return this.lastReplyAuthorId;
	}

	public void setLastReplyAuthorId(String lastReplyAuthorId) {
		this.lastReplyAuthorId = lastReplyAuthorId;
	}

	@Column(name = "reply")

	public Integer getReply() {
		return this.reply;
	}

	public void setReply(Integer reply) {
		this.reply = reply;
	}

	@Column(name = "view")

	public Integer getView() {
		return this.view;
	}

	public void setView(Integer view) {
		this.view = view;
	}

	@Column(name = "click_num")

	public Integer getClickNum() {
		return this.clickNum;
	}

	public void setClickNum(Integer clickNum) {
		this.clickNum = clickNum;
	}

	@Column(name = "author_id", nullable = false)

	public Integer getAuthorId() {
		return this.authorId;
	}

	public void setAuthorId(Integer authorId) {
		this.authorId = authorId;
	}

	@Column(name = "reposted")

	public Integer getReposted() {
		return this.reposted;
	}

	public void setReposted(Integer reposted) {
		this.reposted = reposted;
	}

	@Column(name = "original_url")

	public String getOriginalUrl() {
		return this.originalUrl;
	}

	public void setOriginalUrl(String originalUrl) {
		this.originalUrl = originalUrl;
	}

	@Column(name = "top")

	public Integer getTop() {
		return this.top;
	}

	public void setTop(Integer top) {
		this.top = top;
	}

	@Column(name = "type")

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "good")

	public Integer getGood() {
		return this.good;
	}

	public void setGood(Integer good) {
		this.good = good;
	}

	@Column(name = "show_status")

	public Integer getShowStatus() {
		return this.showStatus;
	}

	public void setShowStatus(Integer showStatus) {
		this.showStatus = showStatus;
	}

	@Column(name = "complex_status")

	public Integer getComplexStatus() {
		return this.complexStatus;
	}

	public void setComplexStatus(Integer complexStatus) {
		this.complexStatus = complexStatus;
	}

	@Column(name = "login_to_view", nullable = false)

	public Integer getLoginToView() {
		return this.loginToView;
	}

	public void setLoginToView(Integer loginToView) {
		this.loginToView = loginToView;
	}

	@Column(name = "back_to_view", nullable = false)

	public Integer getBackToView() {
		return this.backToView;
	}

	public void setBackToView(Integer backToView) {
		this.backToView = backToView;
	}

	@Column(name = "isdelete")

	public Boolean getIsdelete() {
		return this.isdelete;
	}

	public void setIsdelete(Boolean isdelete) {
		this.isdelete = isdelete;
	}

}