package com.pkbigdata.entity.bbs;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BbsUserActivity entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_user_activity", catalog = "bbs")

public class BbsUserActivity implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer activityId;
	private String type;
	private Boolean isJoin;
	private Boolean isFun;
	private Integer userId;
	private Timestamp inTime;

	// Constructors

	/** default constructor */
	public BbsUserActivity() {
	}

	/** full constructor */
	public BbsUserActivity(Integer activityId, String type, Boolean isJoin, Boolean isFun, Integer userId,
			Timestamp inTime) {
		this.activityId = activityId;
		this.type = type;
		this.isJoin = isJoin;
		this.isFun = isFun;
		this.userId = userId;
		this.inTime = inTime;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "activity_id")

	public Integer getActivityId() {
		return this.activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}

	@Column(name = "type", length = 1)

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "is_join")

	public Boolean getIsJoin() {
		return this.isJoin;
	}

	public void setIsJoin(Boolean isJoin) {
		this.isJoin = isJoin;
	}

	@Column(name = "is_fun")

	public Boolean getIsFun() {
		return this.isFun;
	}

	public void setIsFun(Boolean isFun) {
		this.isFun = isFun;
	}

	@Column(name = "user_id")

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "in_time", length = 19)

	public Timestamp getInTime() {
		return this.inTime;
	}

	public void setInTime(Timestamp inTime) {
		this.inTime = inTime;
	}

}