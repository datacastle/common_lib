package com.pkbigdata.entity.bbs;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * BbsTrainType entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_train_type", catalog = "bbs", uniqueConstraints = @UniqueConstraint(columnNames = "name") )

public class BbsTrainType implements java.io.Serializable {

	// Fields

	private Integer id;
	private String  name ;//类型名称
	private Timestamp inTime;//添加时间
	private boolean isDelete;//删除标记

	// Constructors

	/** default constructor */
	public BbsTrainType() {
	}

	/** full constructor */
	public BbsTrainType(String name, Timestamp inTime, Boolean isDelete) {
		this.name = name;
		this.inTime = inTime;
		this.isDelete = isDelete;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", unique = true, nullable = false)

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "in_time", nullable = false, length = 19)

	public Timestamp getInTime() {
		return this.inTime;
	}

	public void setInTime(Timestamp inTime) {
		this.inTime = inTime;
	}

	@Column(name = "is_delete", nullable = false)

	public Boolean getIsDelete() {
		return this.isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

}