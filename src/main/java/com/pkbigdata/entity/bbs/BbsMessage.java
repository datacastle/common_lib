package com.pkbigdata.entity.bbs;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BbsMessage entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_message", catalog = "bbs")

public class BbsMessage implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer sendId;//发送者id
	private String msg;//发送消息
	private Integer type;//类型
	private String group;//用户组
	private Timestamp postDate;//发送时间
	private Integer recId;//收消息人id
	private Integer status;//状态

	// Constructors

	/** default constructor */
	public BbsMessage() {
	}

	/** minimal constructor */
	public BbsMessage(Integer sendId, String msg, Integer type, String group, Timestamp postDate, Integer recId) {
		this.sendId = sendId;
		this.msg = msg;
		this.type = type;
		this.group = group;
		this.postDate = postDate;
		this.recId = recId;
	}

	/** full constructor */
	public BbsMessage(Integer sendId, String msg, Integer type, String group, Timestamp postDate, Integer recId,
			Integer status) {
		this.sendId = sendId;
		this.msg = msg;
		this.type = type;
		this.group = group;
		this.postDate = postDate;
		this.recId = recId;
		this.status = status;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "send_id", nullable = false)

	public Integer getSendId() {
		return this.sendId;
	}

	public void setSendId(Integer sendId) {
		this.sendId = sendId;
	}

	@Column(name = "msg", nullable = false, length = 65535)

	public String getMsg() {
		return this.msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Column(name = "type", nullable = false)

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "group", nullable = false)

	public String getGroup() {
		return this.group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	@Column(name = "post_date", nullable = false, length = 19)

	public Timestamp getPostDate() {
		return this.postDate;
	}

	public void setPostDate(Timestamp postDate) {
		this.postDate = postDate;
	}

	@Column(name = "rec_id", nullable = false)

	public Integer getRecId() {
		return this.recId;
	}

	public void setRecId(Integer recId) {
		this.recId = recId;
	}

	@Column(name = "status")

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}