package com.pkbigdata.entity.bbs;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * BbsActivity entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_activity", catalog = "bbs", uniqueConstraints = @UniqueConstraint(columnNames = "name") )

public class BbsActivity implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;//活动名称
	private Timestamp startTime;//开始时间
	private String location;//地点
	private String cost;//费用
	private String type;//类型
	private String host;//主办方
	private Integer num;//报名人数
	private String detail;//详情
	private Timestamp registrationTime;//报名截止时间
	private Timestamp activitiesDeadline;//活动结束时间
	private String activityType;//活动类型（1、行业活动2、dc活动3.行业新闻）
	private String url;//外链接
	private String img;//图片路径
	private  String  introduction;//新闻简介

	// Constructors

	/** default constructor */
	public BbsActivity() {
	}

	/** full constructor */
	public BbsActivity(String name, String img, String url, Timestamp startTime, String location, String cost,
			String type, String host, Integer num, String detail, Timestamp registrationTime,
			Timestamp activitiesDeadline, String activityType, String introduction) {
		this.name = name;
		this.img = img;
		this.url = url;
		this.startTime = startTime;
		this.location = location;
		this.cost = cost;
		this.type = type;
		this.host = host;
		this.num = num;
		this.detail = detail;
		this.registrationTime = registrationTime;
		this.activitiesDeadline = activitiesDeadline;
		this.activityType = activityType;
		this.introduction = introduction;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", unique = true)

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "img")

	public String getImg() {
		return this.img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Column(name = "url", length = 500)

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "start_time", length = 19)

	public Timestamp getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	@Column(name = "location", length = 500)

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Column(name = "cost", length = 250)

	public String getCost() {
		return this.cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	@Column(name = "type")

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "host")

	public String getHost() {
		return this.host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	@Column(name = "num")

	public Integer getNum() {
		return this.num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	@Column(name = "detail")

	public String getDetail() {
		return this.detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	@Column(name = "registration_time", length = 19)

	public Timestamp getRegistrationTime() {
		return this.registrationTime;
	}

	public void setRegistrationTime(Timestamp registrationTime) {
		this.registrationTime = registrationTime;
	}

	@Column(name = "activities_deadline", length = 19)

	public Timestamp getActivitiesDeadline() {
		return this.activitiesDeadline;
	}

	public void setActivitiesDeadline(Timestamp activitiesDeadline) {
		this.activitiesDeadline = activitiesDeadline;
	}

	@Column(name = "activity_type")

	public String getActivityType() {
		return this.activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	@Column(name = "introduction")

	public String getIntroduction() {
		return this.introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

}