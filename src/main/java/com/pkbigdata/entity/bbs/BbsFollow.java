package com.pkbigdata.entity.bbs;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BbsFollow entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_follow", catalog = "bbs")

public class BbsFollow implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer TId;//贴子id
	private Integer UId;//赞同者id
	private Timestamp inTime;

	// Constructors

	/** default constructor */
	public BbsFollow() {
	}

	/** minimal constructor */
	public BbsFollow(Integer TId, Integer UId) {
		this.TId = TId;
		this.UId = UId;
	}

	/** full constructor */
	public BbsFollow(Integer TId, Integer UId, Timestamp inTime) {
		this.TId = TId;
		this.UId = UId;
		this.inTime = inTime;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "t_id", nullable = false)

	public Integer getTId() {
		return this.TId;
	}

	public void setTId(Integer TId) {
		this.TId = TId;
	}

	@Column(name = "u_id", nullable = false)

	public Integer getUId() {
		return this.UId;
	}

	public void setUId(Integer UId) {
		this.UId = UId;
	}

	@Column(name = "in_time", length = 19)

	public Timestamp getInTime() {
		return this.inTime;
	}

	public void setInTime(Timestamp inTime) {
		this.inTime = inTime;
	}

}