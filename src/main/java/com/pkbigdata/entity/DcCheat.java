package com.pkbigdata.entity;




import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ck on 2017/2/24.
 */
@Table(name = "dc_cheat",catalog = "dc" )
@Entity
public class DcCheat implements java.io.Serializable{
    private Integer id;
    private Integer userId;
    private Integer teamId; //用户所属队伍
    private String teams;//所有关联队伍 （包括主队伍）id s
    private Integer cmptId;     //竞赛id
    private String cause;//作弊原因
    private Integer cheat;//作弊次数
    private String suggestion;//处理意见
    private Timestamp createTime;
    private Boolean delFlag; //true 为已删除记录
    private Boolean submitFlag ;// 是否有 已经归还了提交权限，true为已经手动 设置过了（即已经恢复了权限）
    private Timestamp submitRelieveTime;    //手动恢复提交权限次数的时间，
    private Boolean relieveJoinFlag;        //是否手动恢复了 参赛权限，true为已经设置过了，即已恢复

    @Column(name="relieveJoin_flag")
    public Boolean getRelieveJoinFlag() {
        return relieveJoinFlag;
    }

    public void setRelieveJoinFlag(Boolean relieveJoinFlag) {
        this.relieveJoinFlag = relieveJoinFlag;
    }

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "user_id")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "team_id")
    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    @Column(name = "cmpt_id")
    public Integer getCmptId() {
        return cmptId;
    }

    public void setCmptId(Integer cmptId) {
        this.cmptId = cmptId;
    }

    @Column(name = "cause")
    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    @Column(name="cheat")
    public Integer getCheat() {
        return cheat;
    }

    public void setCheat(Integer cheat) {
        this.cheat = cheat;
    }

    @Column(name = "suggestion")
    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Column(name = "del_flag")
    public Boolean getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Boolean delFlag) {
        this.delFlag = delFlag;
    }

    @Column(name = "teams")
    public String getTeams() {
        return teams;
    }

    public void setTeams(String teams) {
        this.teams = teams;
    }

    @Column(name = "submit_flag")
    public Boolean getSubmitFlag() {
        return submitFlag;
    }

    public void setSubmitFlag(Boolean submitFlag) {
        this.submitFlag = submitFlag;
    }

    @Column(name = "submitRelieve_time")
    public Timestamp getSubmitRelieveTime() {
        return submitRelieveTime;
    }

    public void setSubmitRelieveTime(Timestamp submitRelieveTime) {
        this.submitRelieveTime = submitRelieveTime;
    }
}

