package com.pkbigdata.entity;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * DcMessage entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "dc_message", catalog = "dc")
public class DcMessage implements java.io.Serializable {

	// Fields

    private int id;
    private String subject;//消息主题
    private int senderId;//发送消息者id
    private String senderName;//发送消息者名字
    private String senderRole;//发送消息者角色
    private int receiverId;//接收消息者id
    private String receiverName;//接收者name
    private String receiverRole;//接收者角色
    private String type;//消息类型
    private String content;//消息内容
    private Boolean isRead;//消息是否已读
    private Timestamp time;//发送时间
    private Boolean isReplay;//是否回信
    private Integer replayFor;//被回复消息id
    private Boolean userDelete;//是否删除
    private Timestamp readTime;//读信时间
    private Timestamp replayTime;//回信时间
	private String senderEmail;//发消息者邮箱
	private Integer teamId;//队伍id
	private Boolean accept;//是否接受

	// Constructors

	/** default constructor */
	public DcMessage() {
	}

	/** minimal constructor */
	public DcMessage(String subject, Integer senderId, String senderRole,
			Integer receiverId, String receiverRole, Timestamp time) {
		this.subject = subject;
		this.senderId = senderId;
		this.senderRole = senderRole;
		this.receiverId = receiverId;
		this.receiverRole = receiverRole;
		this.time = time;
	}

	/** full constructor */
	public DcMessage(String subject, Integer senderId, String senderName,
			String senderRole, Integer receiverId, String receiverName,
			String receiverRole, String type, String content, Boolean isRead,
			Timestamp time, Boolean isReplay, Integer replayFor,
			Boolean userDelete, Timestamp readTime, Timestamp replayTime,
			String senderEmail) {
		this.subject = subject;
		this.senderId = senderId;
		this.senderName = senderName;
		this.senderRole = senderRole;
		this.receiverId = receiverId;
		this.receiverName = receiverName;
		this.receiverRole = receiverRole;
		this.type = type;
		this.content = content;
		this.isRead = isRead;
		this.time = time;
		this.isReplay = isReplay;
		this.replayFor = replayFor;
		this.userDelete = userDelete;
		this.readTime = readTime;
		this.replayTime = replayTime;
		this.senderEmail = senderEmail;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "subject", nullable = false, length = 50)
	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Column(name = "sender_id", nullable = false)
	public Integer getSenderId() {
		return this.senderId;
	}

	public void setSenderId(Integer senderId) {
		this.senderId = senderId;
	}

	@Column(name = "sender_name", length = 30)
	public String getSenderName() {
		return this.senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	@Column(name = "sender_role", nullable = false, length = 10)
	public String getSenderRole() {
		return this.senderRole;
	}

	public void setSenderRole(String senderRole) {
		this.senderRole = senderRole;
	}

	@Column(name = "receiver_id", nullable = false)
	public Integer getReceiverId() {
		return this.receiverId;
	}

	public void setReceiverId(Integer receiverId) {
		this.receiverId = receiverId;
	}

	@Column(name = "receiver_name", length = 30)
	public String getReceiverName() {
		return this.receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	@Column(name = "receiver_role", nullable = false, length = 10)
	public String getReceiverRole() {
		return this.receiverRole;
	}

	public void setReceiverRole(String receiverRole) {
		this.receiverRole = receiverRole;
	}

	@Column(name = "type", length = 20)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "content", length = 65535)
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "is_read")
	public Boolean getIsRead() {
		return this.isRead;
	}

	public void setIsRead(Boolean isRead) {
		this.isRead = isRead;
	}

	@Column(name = "time", nullable = false, length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Column(name = "is_Replay")
	public Boolean getIsReplay() {
		return this.isReplay;
	}

	public void setIsReplay(Boolean isReplay) {
		this.isReplay = isReplay;
	}

	@Column(name = "replay_for")
	public Integer getReplayFor() {
		return this.replayFor;
	}

	public void setReplayFor(Integer replayFor) {
		this.replayFor = replayFor;
	}

	@Column(name = "user_delete")
	public Boolean getUserDelete() {
		return this.userDelete;
	}

	public void setUserDelete(Boolean userDelete) {
		this.userDelete = userDelete;
	}

	@Column(name = "read_time", length = 19)
	public Timestamp getReadTime() {
		return this.readTime;
	}

	public void setReadTime(Timestamp readTime) {
		this.readTime = readTime;
	}

	@Column(name = "replay_time", length = 19)
	public Timestamp getReplayTime() {
		return this.replayTime;
	}

	public void setReplayTime(Timestamp replayTime) {
		this.replayTime = replayTime;
	}

	@Column(name = "sender_email", length = 50)
	public String getSenderEmail() {
		return this.senderEmail;
	}

	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}

	@Column(name = "team_id")
	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}
	@Column(name = "accept")
	public Boolean getAccept() {
		return accept;
	}

	public void setAccept(Boolean accept) {
		this.accept = accept;
	}
}