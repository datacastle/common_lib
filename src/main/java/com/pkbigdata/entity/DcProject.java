package com.pkbigdata.entity;// default package

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * DcProject entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "dc_project", catalog = "dc")
public class DcProject implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;
	private Timestamp startTime;
	private Timestamp endTime;
	private String desc;
	private Integer userId;

	// Constructors

	/** default constructor */
	public DcProject() {
	}

	/** full constructor */
	public DcProject(String name, Timestamp startTime, Timestamp endTime,
			String desc, Integer userId) {
		this.name = name;
		this.startTime = startTime;
		this.endTime = endTime;
		this.desc = desc;
		this.userId = userId;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "\"name\"", length = 500)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "start_time", length = 19)
	public Timestamp getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	@Column(name = "end_time", length = 19)
	public Timestamp getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	@Column(name = "\"desc\"", length = 1000)
	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Column(name = "user_id")
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}