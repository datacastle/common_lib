package com.pkbigdata.entity;// default package

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * DcThirdLogin entity. @author MyEclipse Persistence Tools
 * 第三方登录表
 */
@Entity
@Table(name = "dc_third_login", catalog = "dc")
public class DcThirdLogin implements java.io.Serializable {

	// Fields

	private Integer id;
	private String thirdId;//第三方id
	private String userName;//第三方用户名
	private Integer localId;//本平台id
	private String thirdLoginType;//第三方登录类型
	private String thirdAvatar;//头像
    private String thirdUrl;//第三方平台的链接，可以达到个人主页
	private Timestamp creatTime;//创建时间

	// Constructors

	/** default constructor */
	public DcThirdLogin() {
	}

	/** minimal constructor */
	public DcThirdLogin(String thirdId, String userName, String thirdLoginType,
			Timestamp creatTime) {
		this.thirdId = thirdId;
		this.userName = userName;
		this.thirdLoginType = thirdLoginType;
		this.creatTime = creatTime;
	}

	/** full constructor */
	public DcThirdLogin(String thirdId, String userName, Integer localId,
			String thirdLoginType, String thirdAvatar, Timestamp creatTime) {
		this.thirdId = thirdId;
		this.userName = userName;
		this.localId = localId;
		this.thirdLoginType = thirdLoginType;
		this.thirdAvatar = thirdAvatar;
		this.creatTime = creatTime;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "third_id", nullable = false, length = 50)
	public String getThirdId() {
		return this.thirdId;
	}

	public void setThirdId(String thirdId) {
		this.thirdId = thirdId;
	}

	@Column(name = "user_name", nullable = false, length = 50)
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "local_id")
	public Integer getLocalId() {
		return this.localId;
	}

	public void setLocalId(Integer localId) {
		this.localId = localId;
	}

	@Column(name = "third_login_type", nullable = false, length = 10)
	public String getThirdLoginType() {
		return this.thirdLoginType;
	}

	public void setThirdLoginType(String thirdLoginType) {
		this.thirdLoginType = thirdLoginType;
	}

	@Column(name = "third_avatar", length = 100)
	public String getThirdAvatar() {
		return this.thirdAvatar;
	}

	public void setThirdAvatar(String thirdAvatar) {
		this.thirdAvatar = thirdAvatar;
	}

	@Column(name = "creat_time", nullable = false, length = 19)
	public Timestamp getCreatTime() {
		return this.creatTime;
	}

	public void setCreatTime(Timestamp creatTime) {
		this.creatTime = creatTime;
	}

    @Column(name = "third_url",length = 500)
    public String getThirdUrl() {
        return thirdUrl;
    }

    public void setThirdUrl(String thirdUrl) {
        this.thirdUrl = thirdUrl;
    }
}