package com.pkbigdata.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by zhaorenjie on 2015/4/1.
 * 主办方表
 */
@Entity
@Table(name = "dc_host", catalog = "dc")
public class DcHost implements java.io.Serializable{
    private int id;
    private String name;//主办方名
    private String avatar;//主办方头像
    private String email;//主办方邮箱
    private String tel;//主办方电话
    private Timestamp time;//主办方创建时间
    private String password;//主办方密码
    private String contacts;//联系人
    private String address;//地址
    private String homePage;//主页
    private String introduction;//简介
    private Boolean show;//是否显示
    private String city;//所在城市
    private String companySize;//公司规模
    private String industryDirection;//行业方向
    private String logoPath;//logo图片路径
    private String contactsPosition;//联系人职务
    private String bannerPath;//banner图路径
    private String  province;//省
    private String area;//区
    private String stage;//阶段
    private String resumeEmail;//简历收件邮箱


    @Id
    @Column(name = "id")
    @GeneratedValue
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "avatar")
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "tel")
    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Basic
    @Column(name = "time")
    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "contacts")
    public String getContacts() {
        return this.contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    @Column(name = "address")
    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "home_page")
    public String getHomePage() {
        return this.homePage;
    }

    public void setHomePage(String homePage) {
        this.homePage = homePage;
    }

    @Column(name = "Introduction", length = 500)
    public String getIntroduction() {
        return this.introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    @Column(name = "\"show\"")
    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    @Column(name="city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column(name = "company_size")
    public String getCompanySize() {
        return companySize;
    }

    public void setCompanySize(String companySize) {
        this.companySize = companySize;
    }
    @Column(name="industry_direction")
    public String getIndustryDirection() {
        return industryDirection;
    }

    public void setIndustryDirection(String industryDirection) {
        this.industryDirection = industryDirection;
    }

    @Column(name="logo_path")
    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    @Column(name="contacts_position")
    public String getContactsPosition() {
        return contactsPosition;
    }

    public void setContactsPosition(String contactsPosition) {
        this.contactsPosition = contactsPosition;
    }
     @Column(name = "banner_path")
    public String getBannerPath() {
        return bannerPath;
    }

    public void setBannerPath(String bannerPath) {
        this.bannerPath = bannerPath;
    }

     @Column(name="province")
    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

     @Column(name="area")
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @Column(name="stage")
    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    @Column(name="resume_email")
    public String getResumeEmail() {
        return resumeEmail;
    }

    public void setResumeEmail(String resumeEmail) {
        this.resumeEmail = resumeEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DcHost that = (DcHost) o;

        if (id != that.id) return false;
        if (avatar != null ? !avatar.equals(that.avatar) : that.avatar != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (tel != null ? !tel.equals(that.tel) : that.tel != null) return false;
        if (time != null ? !time.equals(that.time) : that.time != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (avatar != null ? avatar.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (tel != null ? tel.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
    /** default constructor */
    public DcHost() {
    }

    /** minimal constructor */
    public DcHost(String name, String email, String tel, Timestamp time,
                  String password) {
        this.name = name;
        this.email = email;
        this.tel = tel;
        this.time = time;
        this.password = password;
    }

    /** full constructor */
    public DcHost(String name, String avatar, String email, String tel,
                  Timestamp time, String password, String contacts, String address,
                  String homePage, String introduction) {
        this.name = name;
        this.avatar = avatar;
        this.email = email;
        this.tel = tel;
        this.time = time;
        this.password = password;
        this.contacts = contacts;
        this.address = address;
        this.homePage = homePage;
        this.introduction = introduction;
    }
}
