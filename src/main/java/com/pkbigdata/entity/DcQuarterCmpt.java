package com.pkbigdata.entity;

import javax.persistence.*;

/**
 * 智慧中国杯子竞赛进入复赛阶段队伍记录表
 * Created by ck on 2017/2/9.
 */
@Entity
@Table(name = "dc_quarter_cmpt",catalog = "dc")
public class DcQuarterCmpt implements java.io.Serializable{
    private Integer id;
    private Integer teamId;
    private String teamName;
    private Integer cmptId;
    private Boolean upload;
    private Integer subId;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "team_name")
    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    @Column(name = "cmpt_id")
    public Integer getCmptId() {
        return cmptId;
    }

    public void setCmptId(Integer cmptId) {
        this.cmptId = cmptId;
    }

    @Column(name ="team_id")
    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    @Column(name="upload")
    public Boolean getUpload() {
        return upload;
    }

    public void setUpload(Boolean upload) {
        this.upload = upload;
    }

    @Column(name = "sub_id")
    public Integer getSubId() {
        return subId;
    }

    public void setSubId(Integer subId) {
        this.subId = subId;
    }
}
