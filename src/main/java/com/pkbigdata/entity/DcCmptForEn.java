package com.pkbigdata.entity;

import javax.persistence.*;

/**
 * Created by jay on 2016/7/20.
 * 竞赛说明表
 */
@Entity
@Table(name = "dc_cmpt_for_en", catalog = "dc")
public class DcCmptForEn implements java.io.Serializable{
    private Integer id;
    private Integer cmptId;//竞赛ID
    private String uploadDataFormat;//提交说明
    private String taskDescription;//赛题
    private String datasetDescription;//数据描述
    private String evaluationCriteria;//评分规则
    private String backGround;//背景
    private String rewardTypeInfo;//奖励描述
    private String timeProcess;//时间安排
    private String name;//英文名称

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "cmpt_id")
    public Integer getCmptId() {
        return cmptId;
    }

    public void setCmptId(Integer cmptId) {
        this.cmptId = cmptId;
    }

    @Basic
    @Column(name = "upload_data_format")
    public String getUploadDataFormat() {
        return uploadDataFormat;
    }

    public void setUploadDataFormat(String uploadDataFormat) {
        this.uploadDataFormat = uploadDataFormat;
    }

    @Basic
    @Column(name = "task_description")
    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    @Basic
    @Column(name = "dataset_description")
    public String getDatasetDescription() {
        return datasetDescription;
    }

    public void setDatasetDescription(String datasetDescription) {
        this.datasetDescription = datasetDescription;
    }

    @Basic
    @Column(name = "evaluation_criteria")
    public String getEvaluationCriteria() {
        return evaluationCriteria;
    }

    public void setEvaluationCriteria(String evaluationCriteria) {
        this.evaluationCriteria = evaluationCriteria;
    }

    @Basic
    @Column(name = "back_ground")
    public String getBackGround() {
        return backGround;
    }

    public void setBackGround(String backGround) {
        this.backGround = backGround;
    }

    @Basic
    @Column(name = "reward_type_info")
    public String getRewardTypeInfo() {
        return rewardTypeInfo;
    }

    public void setRewardTypeInfo(String rewardTypeInfo) {
        this.rewardTypeInfo = rewardTypeInfo;
    }

    @Basic
    @Column(name = "time_process")
    public String getTimeProcess() {
        return timeProcess;
    }

    public void setTimeProcess(String timeProcess) {
        this.timeProcess = timeProcess;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DcCmptForEn that = (DcCmptForEn) o;

        if (id != that.id) return false;
        if (cmptId != null ? !cmptId.equals(that.cmptId) : that.cmptId != null) return false;
        if (uploadDataFormat != null ? !uploadDataFormat.equals(that.uploadDataFormat) : that.uploadDataFormat != null)
            return false;
        if (taskDescription != null ? !taskDescription.equals(that.taskDescription) : that.taskDescription != null)
            return false;
        if (datasetDescription != null ? !datasetDescription.equals(that.datasetDescription) : that.datasetDescription != null)
            return false;
        if (evaluationCriteria != null ? !evaluationCriteria.equals(that.evaluationCriteria) : that.evaluationCriteria != null)
            return false;
        if (backGround != null ? !backGround.equals(that.backGround) : that.backGround != null) return false;
        if (rewardTypeInfo != null ? !rewardTypeInfo.equals(that.rewardTypeInfo) : that.rewardTypeInfo != null)
            return false;
        if (timeProcess != null ? !timeProcess.equals(that.timeProcess) : that.timeProcess != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (cmptId != null ? cmptId.hashCode() : 0);
        result = 31 * result + (uploadDataFormat != null ? uploadDataFormat.hashCode() : 0);
        result = 31 * result + (taskDescription != null ? taskDescription.hashCode() : 0);
        result = 31 * result + (datasetDescription != null ? datasetDescription.hashCode() : 0);
        result = 31 * result + (evaluationCriteria != null ? evaluationCriteria.hashCode() : 0);
        result = 31 * result + (backGround != null ? backGround.hashCode() : 0);
        result = 31 * result + (rewardTypeInfo != null ? rewardTypeInfo.hashCode() : 0);
        result = 31 * result + (timeProcess != null ? timeProcess.hashCode() : 0);
        return result;
    }
}
