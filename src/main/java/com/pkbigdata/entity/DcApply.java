package com.pkbigdata.entity;// default package

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * DcApply entity. @author MyEclipse Persistence Tools
 * 管理员审核表
 */
@Entity
@Table(name = "dc_apply", catalog = "dc")
public class DcApply implements java.io.Serializable {

	// Fields

    private Integer id;
    private String type;//审核类型
    private Integer applyid;//审核id
    private Timestamp applytime;//申请时间
    private Boolean pass;//处理结果
    private Timestamp dealtime;//处理时间
	private Integer managerid;//处理申请的管理员id

	// Constructors

	/** default constructor */
	public DcApply() {
	}

	/** minimal constructor */
	public DcApply(String type, Integer applyid, Timestamp applytime,
			Boolean pass) {
		this.type = type;
		this.applyid = applyid;
		this.applytime = applytime;
		this.pass = pass;
	}

	/** full constructor */
	public DcApply(String type, Integer applyid, Timestamp applytime,
			Boolean pass, Timestamp dealtime, Integer managerid) {
		this.type = type;
		this.applyid = applyid;
		this.applytime = applytime;
		this.pass = pass;
		this.dealtime = dealtime;
		this.managerid = managerid;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "type", nullable = false)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "applyid", nullable = false)
	public Integer getApplyid() {
		return this.applyid;
	}

	public void setApplyid(Integer applyid) {
		this.applyid = applyid;
	}

	@Column(name = "applytime", nullable = false, length = 19)
	public Timestamp getApplytime() {
		return this.applytime;
	}

	public void setApplytime(Timestamp applytime) {
		this.applytime = applytime;
	}

	@Column(name = "pass", nullable = false)
	public Boolean getPass() {
		return this.pass;
	}

	public void setPass(Boolean pass) {
		this.pass = pass;
	}

	@Column(name = "dealtime", length = 19)
	public Timestamp getDealtime() {
		return this.dealtime;
	}

	public void setDealtime(Timestamp dealtime) {
		this.dealtime = dealtime;
	}

	@Column(name = "managerid")
	public Integer getManagerid() {
		return this.managerid;
	}

	public void setManagerid(Integer managerid) {
		this.managerid = managerid;
	}

}