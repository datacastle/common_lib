package com.pkbigdata.entity;// default package

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

/**
 * DcUser entity. @author MyEclipse Persistence Tools
 * 参赛用户表
 */
@Entity
@Table(name = "dc_user", catalog = "dc", uniqueConstraints = {
        @UniqueConstraint(columnNames = "email"),
        @UniqueConstraint(columnNames = "username")})
public class DcUser implements java.io.Serializable {

    // Fields

    private Integer id;
    private String username;//用户名
    private String phone;//电话号码
    private String legalname;//真实姓名
    private String avatar;//用户头像
    private String password;//用户密码
    private String email;//用户邮箱
    private String skills;//用户技能
    private String bio;//简介
    private String location;//所在地
    private Float points;//所得总分
    private String tier;//用户头衔
    private String social;//社交信息
    private Boolean subscribe;//
    private Timestamp signupTime;//注册时间
    private String token;//用户邮箱验证时的口令
    private Timestamp tokenTime;//生成token的时间
    private Boolean status;//用户状态信息
    private String idCardFile;//身份证文件映射
    private String idCard;//身份证号码
    private Boolean phoneAuth;//手机认证
    private Boolean certification;//实名认证
    private Boolean frozen = false;//是否被锁定
    private Integer credit;//信用污点次数
    private String creditInfo;//信用污点记录，追加记录
    private String workStatus;//在读、在职、自由职业者、其他
    private String workFor;//公司名或学校名
    private String degree;//学位
    private String workName;//职位或者专业
    private Date workBegin;//在公司或者学校的开始时间
    private Date workEnd;//在公司或者学校的结束时间
    private String workInfo;//工作描述
    private String regSession;//注册时的session
    private String regIp;//注册时的ip
    private String workExperience;
    private String label;//竞赛标签，用竖线风格
    private Integer personalStatus;//个人状态
    private Integer myShare;//我的分享
    private Integer myEvents;//我的活动
    private Boolean notificationTeam;//团队通知
    private Boolean systemNotification;//系统通知
    private Boolean privateLetter;//私信通知
    private String resumePdfUrl;//简历pdf路径
    private String resumeUrl;//简历原件路径
    private Boolean resumePublic;//简历是否公开
    private String refererUrl;//注册渠道来源
    private String registerType;//注册方式
    // Constructors

    /**
     * default constructor
     */
    public DcUser() {
    }

    /**
     * minimal constructor
     */
    public DcUser(String username, String legalname, String avatar,
                  String password, String email, Float points, String tier,
                  Boolean subscribe, Timestamp signupTime, Boolean status) {
        this.username = username;
        this.legalname = legalname;
        this.avatar = avatar;
        this.password = password;
        this.email = email;
        this.points = points;
        this.tier = tier;
        this.subscribe = subscribe;
        this.signupTime = signupTime;
        this.status = status;
    }

    /**
     * full constructor
     */
    public DcUser(String username, String phone, String legalname,
                  String avatar, String password, String email, String skills,
                  String bio, String location, Float points, String tier,
                  String social, Boolean subscribe, Timestamp signupTime,
                  String token, Timestamp tokenTime, Boolean status,
                  String idCardFile, String idCard, Boolean phoneAuth,
                  Boolean certification, Boolean frozen, Integer credit,
                  String creditInfo) {
        this.username = username;
        this.phone = phone;
        this.legalname = legalname;
        this.avatar = avatar;
        this.password = password;
        this.email = email;
        this.skills = skills;
        this.bio = bio;
        this.location = location;
        this.points = points;
        this.tier = tier;
        this.social = social;
        this.subscribe = subscribe;
        this.signupTime = signupTime;
        this.token = token;
        this.tokenTime = tokenTime;
        this.status = status;
        this.idCardFile = idCardFile;
        this.idCard = idCard;
        this.phoneAuth = phoneAuth;
        this.certification = certification;
        this.frozen = frozen;
        this.credit = credit;
        this.creditInfo = creditInfo;


    }

    // Property accessors
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "username", unique = true, nullable = false, length = 50)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "phone", length = 11)
    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name = "legalname", nullable = false, length = 16)
    public String getLegalname() {
        return this.legalname;
    }

    public void setLegalname(String legalname) {
        this.legalname = legalname;
    }

    @Column(name = "avatar", nullable = false)
    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Column(name = "password", nullable = false, length = 32)
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "email", unique = true,  length = 64)
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "skills")
    public String getSkills() {
        return this.skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    @Column(name = "bio", length = 65535)
    public String getBio() {
        return this.bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    @Column(name = "location", length = 64)
    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Column(name = "points", nullable = false)
    public Float getPoints() {
        return this.points;
    }

    public void setPoints(Float points) {
        this.points = points;
    }

    @Column(name = "tier", nullable = false, length = 7)
    public String getTier() {
        return this.tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    @Column(name = "social", length = 65535)
    public String getSocial() {
        return this.social;
    }

    public void setSocial(String social) {
        this.social = social;
    }

    @Column(name = "subscribe", nullable = false)
    public Boolean getSubscribe() {
        return this.subscribe;
    }

    public void setSubscribe(Boolean subscribe) {
        this.subscribe = subscribe;
    }

    @Column(name = "signup_time", nullable = false, length = 19)
    public Timestamp getSignupTime() {
        return this.signupTime;
    }

    public void setSignupTime(Timestamp signupTime) {
        this.signupTime = signupTime;
    }

    @Column(name = "token", length = 32)
    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Column(name = "token_time", length = 19)
    public Timestamp getTokenTime() {
        return this.tokenTime;
    }

    public void setTokenTime(Timestamp tokenTime) {
        this.tokenTime = tokenTime;
    }

    @Column(name = "status", nullable = false)
    public Boolean getStatus() {
        return this.status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Column(name = "id_card_file", length = 255)
    public String getIdCardFile() {
        return this.idCardFile;
    }

    public void setIdCardFile(String idCardFile) {
        this.idCardFile = idCardFile;
    }

    @Column(name = "id_card", length = 18)
    public String getIdCard() {
        return this.idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    @Column(name = "phone_auth")
    public Boolean getPhoneAuth() {
        return this.phoneAuth;
    }

    public void setPhoneAuth(Boolean phoneAuth) {
        this.phoneAuth = phoneAuth;
    }

    @Column(name = "certification")
    public Boolean getCertification() {
        return this.certification;
    }

    public void setCertification(Boolean certification) {
        this.certification = certification;
    }

    @Column(name = "frozen")
    public Boolean getFrozen() {
        return this.frozen;
    }

    public void setFrozen(Boolean frozen) {
        this.frozen = frozen;
    }

    @Column(name = "credit")
    public Integer getCredit() {
        return this.credit;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    @Column(name = "credit_info", length = 500)
    public String getCreditInfo() {
        return this.creditInfo;
    }

    public void setCreditInfo(String creditInfo) {
        this.creditInfo = creditInfo;
    }

    @Column(name = "work_status", length = 10)
    public String getWorkStatus() {
        return this.workStatus;
    }

    public void setWorkStatus(String workStatus) {
        this.workStatus = workStatus;
    }

    @Column(name = "work_for", length = 50)
    public String getWorkFor() {
        return this.workFor;
    }

    public void setWorkFor(String workFor) {
        this.workFor = workFor;
    }

    @Column(name = "degree", length = 5)
    public String getDegree() {
        return this.degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    @Column(name = "work_name", length = 50)
    public String getWorkName() {
        return this.workName;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    @Column(name = "work_begin", length = 19)
    public Date getWorkBegin() {
        return this.workBegin;
    }

    public void setWorkBegin(Date workBegin) {
        this.workBegin = workBegin;
    }

    @Column(name = "work_end", length = 19)
    public Date getWorkEnd() {
        return this.workEnd;
    }

    public void setWorkEnd(Date workEnd) {
        this.workEnd = workEnd;
    }

    @Column(name = "reg_session", length = 255)
    public String getRegSession() {
        return regSession;
    }

    public void setRegSession(String regSession) {
        this.regSession = regSession;
    }

    @Column(name = "reg_ip", length = 255)
    public String getRegIp() {
        return regIp;
    }

    public void setRegIp(String regIp) {
        this.regIp = regIp;
    }

    @Column(name = "work_info", length = 500)
    public String getWorkInfo() {
        return workInfo;
    }

    public void setWorkInfo(String workInfo) {
        this.workInfo = workInfo;
    }

    @Column(name = "work_experience")
    public String getWorkExperience() {
        return workExperience;
    }

    public void setWorkExperience(String workExperience) {
        this.workExperience = workExperience;
    }

    @Column(name = "label",length = 1000)
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Column(name = "personal_status")
    public Integer getPersonalStatus() {
        return personalStatus;
    }

    public void setPersonalStatus(Integer personalStatus) {
        this.personalStatus = personalStatus;
    }
    @Column(name = "my_share")
    public Integer getMyShare() {
        return myShare;
    }

    public void setMyShare(Integer myShare) {
        this.myShare = myShare;
    }
    @Column(name = "my_events")
    public Integer getMyEvents() {
        return myEvents;
    }

    public void setMyEvents(Integer myEvents) {
        this.myEvents = myEvents;
    }
    @Column(name = "notification_team")
    public Boolean getNotificationTeam() {
        return notificationTeam;
    }

    public void setNotificationTeam(Boolean notificationTeam) {
        this.notificationTeam = notificationTeam;
    }
    @Column(name = "system_notification")
    public Boolean getSystemNotification() {
        return systemNotification;
    }

    public void setSystemNotification(Boolean systemNotification) {
        this.systemNotification = systemNotification;
    }

    @Column(name = "resume_pdf_url")
    public String getResumePdfUrl() {
        return resumePdfUrl;
    }

    public void setResumePdfUrl(String resumePdfUrl) {
        this.resumePdfUrl = resumePdfUrl;
    }

    @Column(name="resume_url")
    public String getResumeUrl() {
        return resumeUrl;
    }

    public void setResumeUrl(String resumeUrl) {
        this.resumeUrl = resumeUrl;
    }

    @Column(name="resume_public")
    public Boolean getResumePublic() {
        return resumePublic;
    }

    public void setResumePublic(Boolean resumePublic) {
        this.resumePublic = resumePublic;
    }

    @Column(name = "referer_url")
    public String getRefererUrl() {
        return refererUrl;
    }

    public void setRefererUrl(String refererUrl) {
        this.refererUrl = refererUrl;
    }


    @Column(name="register_type")
    public String getRegisterType() {
        return registerType;
    }

    public void setRegisterType(String registerType) {
        this.registerType = registerType;
    }

    @Column(name="private_letter")
    public Boolean getPrivateLetter() {
        return privateLetter;
    }

    public void setPrivateLetter(Boolean privateLetter) {
        this.privateLetter = privateLetter;
    }
}