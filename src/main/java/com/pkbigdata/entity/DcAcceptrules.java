package com.pkbigdata.entity;// default package

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * DcAcceptrules entity. @author MyEclipse Persistence Tools
 *关联用户与竞赛之间规则接受关系
 */
@Entity
@Table(name = "dc_acceptrules", catalog = "dc")
public class DcAcceptrules implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer userId;//用户id
	private Integer cmptId;//竞赛
	private Timestamp time;//接受规则时间

	// Constructors

	/** default constructor */
	public DcAcceptrules() {
	}

	/** full constructor */
	public DcAcceptrules(Integer userId, Integer cmptId, Timestamp time) {
		this.userId = userId;
		this.cmptId = cmptId;
		this.time = time;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "user_id", nullable = false)
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "cmpt_id", nullable = false)
	public Integer getCmptId() {
		return this.cmptId;
	}

	public void setCmptId(Integer cmptId) {
		this.cmptId = cmptId;
	}

	@Column(name = "time", nullable = false, length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

}