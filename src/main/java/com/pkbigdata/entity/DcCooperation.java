package com.pkbigdata.entity;

import javax.persistence.*;

/**
 * DcCooperation entity. @author MyEclipse Persistence Tools
 * 合作
 */
@Entity
@Table(name = "dc_cooperation", catalog = "dc")
public class DcCooperation implements java.io.Serializable {

	// Fields

	private Integer id;
	private String company;
	private String name;
	private String jobTitle;
	private String organization;
	private String domain;
	private String organizationSize;
	private String purpose;
	private String desc;
	private String mail;
	private String phone;

	// Constructors

	/** default constructor */
	public DcCooperation() {
	}

	/** full constructor */
	public DcCooperation(String company, String name, String jobTitle,
						 String organization, String domain, String organizationSize,
						 String purpose, String desc, String mail, String phone) {
		this.company = company;
		this.name = name;
		this.jobTitle = jobTitle;
		this.organization = organization;
		this.domain = domain;
		this.organizationSize = organizationSize;
		this.purpose = purpose;
		this.desc = desc;
		this.mail = mail;
		this.phone = phone;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "company")
	public String getCompany() {
		return this.company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "job_title")
	public String getJobTitle() {
		return this.jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	@Column(name = "organization")
	public String getOrganization() {
		return this.organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	@Column(name = "domain")
	public String getDomain() {
		return this.domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	@Column(name = "organization_size")
	public String getOrganizationSize() {
		return this.organizationSize;
	}

	public void setOrganizationSize(String organizationSize) {
		this.organizationSize = organizationSize;
	}

	@Column(name = "purpose")
	public String getPurpose() {
		return this.purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	@Column(name = "\"desc\"", length = 2000)
	public String getDesc() {
		return this.desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Column(name = "mail")
	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@Column(name = "phone")
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}