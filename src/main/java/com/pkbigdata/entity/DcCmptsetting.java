package com.pkbigdata.entity;// default package

import javax.persistence.*;

/**
 * DcCmptsetting entity. @author MyEclipse Persistence Tools
 * 竞赛设置表
 */
@Entity
@Table(name = "dc_cmptsetting", catalog = "dc", uniqueConstraints = @UniqueConstraint(columnNames = "cmpt_id"))
public class DcCmptsetting implements java.io.Serializable {

	// Fields

	private Integer id;

    //比赛规则
	private Integer cmptId;//竞赛id
	private Integer mts;//队伍最多成员数
	private Integer mds;//一天最多提交数
    private String additionalRules;//附加规则

    //评价标准
    private String algorithmCategory;//评分算法类别
    private Integer algorithm;//评分算法
    private String evaluationCriteria;//评价标准
    private String weightedAverageType;//加权平均数类型
    private Double weightedAverage;//加权平均数


    //竞赛数据
    private String datasetDescription;//数据集描述
    private String dataUrl;//平台链接


    private String password;//平台链接密码
    private String taskDescription;//任务描述

    //提交结果
    private String testFile;//测试集文件
    private String exampleFile;//示例提交文件
    private String uploadDataFormat;//提交数据格式


    //提交结果格式映射，json
    private String solution;//测试文件映射
    private String sample;//示例提交文件映射



    //在线编程
    private Boolean onlineJudge = false;//是否采用在线编程
    private String language;//编程语言
    private Integer cpu;//cpu上限
    private Integer ram;//内存上限，单位M
    private Integer runTime;//运行时间,单位分钟
    private String onlineJudgeInfo;//在线编程简介
    private String onlineJudgeData;//数据源文件地址
    private String clusterLocation;//集群位置
    private Integer rankNum;//排行榜的数量
    private Boolean lockUpload;//锁定提交，竞赛完结期使用的功能
    private Boolean autoPublish;//是否自动发布

    private String  staticRank;//竞赛结束时的固定排行榜



/*    //暂时不需要，但是不删除
	private Integer sps;//用于计算最终成绩的提交数*/


	// Constructors



    /** default constructor */
    public DcCmptsetting() {
    }

    /** minimal constructor */
    public DcCmptsetting(Integer cmptId, Integer mts, Integer mds, Integer sps) {
        this.cmptId = cmptId;
        this.mts = mts;
        this.mds = mds;

    }

    /** full constructor */
    public DcCmptsetting(Integer cmptId, Integer mts, Integer mds, Integer sps,
                         String solution, String sample, String additionalRules,
                         String algorithmCategory, Integer algorithm,
                         String evaluationCriteria, String weightedAverageType,
                         Double weightedAverage, String datasetDescription, String dataUrl,
                         String password, String taskDescription, Integer testFile,
                         Integer exampleFile, String uploadDataFormat) {
        this.cmptId = cmptId;
        this.mts = mts;
        this.mds = mds;

        this.solution = solution;
        this.sample = sample;
        this.additionalRules = additionalRules;
        this.algorithmCategory = algorithmCategory;
        this.algorithm = algorithm;
        this.evaluationCriteria = evaluationCriteria;
        this.weightedAverageType = weightedAverageType;
        this.weightedAverage = weightedAverage;
        this.datasetDescription = datasetDescription;
        this.dataUrl = dataUrl;
        this.password = password;
        this.taskDescription = taskDescription;
        this.uploadDataFormat = uploadDataFormat;
    }

    // Property accessors
    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "cmpt_id", unique = true, nullable = false)
    public Integer getCmptId() {
        return this.cmptId;
    }

    public void setCmptId(Integer cmptId) {
        this.cmptId = cmptId;
    }

    @Column(name = "mts", nullable = false)
    public Integer getMts() {
        return this.mts;
    }

    public void setMts(Integer mts) {
        this.mts = mts;
    }

    @Column(name = "mds", nullable = false)
    public Integer getMds() {
        return this.mds;
    }

    public void setMds(Integer mds) {
        this.mds = mds;
    }


    @Column(name = "solution", length = 65535)
    public String getSolution() {
        return this.solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    @Column(name = "sample", length = 65535)
    public String getSample() {
        return this.sample;
    }

    public void setSample(String sample) {
        this.sample = sample;
    }

    @Column(name = "additional_rules", length = 65535)
    public String getAdditionalRules() {
        return this.additionalRules;
    }

    public void setAdditionalRules(String additionalRules) {
        this.additionalRules = additionalRules;
    }

    @Column(name = "algorithm_category")
    public String getAlgorithmCategory() {
        return this.algorithmCategory;
    }

    public void setAlgorithmCategory(String algorithmCategory) {
        this.algorithmCategory = algorithmCategory;
    }

    @Column(name = "algorithm")
    public Integer getAlgorithm() {
        return this.algorithm;
    }

    public void setAlgorithm(Integer algorithm) {
        this.algorithm = algorithm;
    }

    @Column(name = "evaluation_criteria", length = 65535)
    public String getEvaluationCriteria() {
        return this.evaluationCriteria;
    }

    public void setEvaluationCriteria(String evaluationCriteria) {
        this.evaluationCriteria = evaluationCriteria;
    }

    @Column(name = "weighted_average_type")
    public String getWeightedAverageType() {
        return this.weightedAverageType;
    }

    public void setWeightedAverageType(String weightedAverageType) {
        this.weightedAverageType = weightedAverageType;
    }

    @Column(name = "weighted_average")
    public Double getWeightedAverage() {
        return this.weightedAverage;
    }

    public void setWeightedAverage(Double weightedAverage) {
        this.weightedAverage = weightedAverage;
    }

    @Column(name = "dataset_description", length = 65535)
    public String getDatasetDescription() {
        return this.datasetDescription;
    }

    public void setDatasetDescription(String datasetDescription) {
        this.datasetDescription = datasetDescription;
    }

    @Column(name = "data_url")
    public String getDataUrl() {
        return this.dataUrl;
    }

    public void setDataUrl(String dataUrl) {
        this.dataUrl = dataUrl;
    }

    @Column(name = "password")
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "task_description", length = 65535)
    public String getTaskDescription() {
        return this.taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    @Column(name = "test_file")
    public String getTestFile() {
        return this.testFile;
    }

    public void setTestFile(String testFile) {
        this.testFile = testFile;
    }

    @Column(name = "example_file")
    public String getExampleFile() {
        return this.exampleFile;
    }

    public void setExampleFile(String exampleFile) {
        this.exampleFile = exampleFile;
    }

    @Column(name = "upload_data_format", length = 65535)
    public String getUploadDataFormat() {
        return this.uploadDataFormat;
    }

    public void setUploadDataFormat(String uploadDataFormat) {
        this.uploadDataFormat = uploadDataFormat;
    }

    @Column(name = "online_judge")
    public Boolean getOnlineJudge() {
        return this.onlineJudge;
    }

    public void setOnlineJudge(Boolean onlineJudge) {
        this.onlineJudge = onlineJudge;
    }

    @Column(name = "language")
    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Column(name = "cpu")
    public Integer getCpu() {
        return this.cpu;
    }

    public void setCpu(Integer cpu) {
        this.cpu = cpu;
    }

    @Column(name = "ram")
    public Integer getRam() {
        return this.ram;
    }

    public void setRam(Integer ram) {
        this.ram = ram;
    }

    @Column(name = "run_time")
    public Integer getRunTime() {
        return this.runTime;
    }

    public void setRunTime(Integer runTime) {
        this.runTime = runTime;
    }

    @Column(name = "online_judge_info", length = 500)
    public String getOnlineJudgeInfo() {
        return this.onlineJudgeInfo;
    }

    public void setOnlineJudgeInfo(String onlineJudgeInfo) {
        this.onlineJudgeInfo = onlineJudgeInfo;
    }

    @Column(name="online_judge_data")
    public String getOnlineJudgeData() {
        return onlineJudgeData;
    }

    public void setOnlineJudgeData(String onlineJudgeData) {
        this.onlineJudgeData = onlineJudgeData;
    }

    @Column(name="cluster_location")
    public String getClusterLocation() {
        return clusterLocation;
    }

    public void setClusterLocation(String clusterLocation) {
        this.clusterLocation = clusterLocation;
    }

    @Column(name="rank_num")
    public Integer getRankNum() {
        return rankNum;
    }

    public void setRankNum(Integer rankNum) {
        this.rankNum = rankNum;
    }

    @Column(name="lock_upload")
    public Boolean getLockUpload() {
        return lockUpload;
    }

    public void setLockUpload(Boolean lockUpload) {
        this.lockUpload = lockUpload;
    }

    @Column(name="auto_publish")
    public Boolean getAutoPublish() {
        return autoPublish;
    }

    public void setAutoPublish(Boolean autoPublish) {
        this.autoPublish = autoPublish;
    }

    @Column(name = "static_rank")
    public String getStaticRank() {
        return staticRank;
    }

    public void setStaticRank(String staticRank) {
        this.staticRank = staticRank;
    }
}