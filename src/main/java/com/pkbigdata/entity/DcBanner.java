package com.pkbigdata.entity;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * DcBanner entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "dc_banner", catalog = "dc")
public class DcBanner implements java.io.Serializable {

	// Fields

	private Integer id;
	private String img;//图片
	private String url;//跳转路径
	private Integer sort;//排序
	private Boolean status;//状态

	// Constructors

	/** default constructor */
	public DcBanner() {
	}

	/** full constructor */
	public DcBanner(String img, String url, Integer sort, Boolean status) {
		this.img = img;
		this.url = url;
		this.sort = sort;
		this.status = status;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "img", length = 500)
	public String getImg() {
		return this.img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Column(name = "url", length = 500)
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "sort")
	public Integer getSort() {
		return this.sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	@Column(name = "status")
	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

}