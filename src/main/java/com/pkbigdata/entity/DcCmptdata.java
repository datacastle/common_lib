package com.pkbigdata.entity;// default package

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * DcCmptdata entity. @author MyEclipse Persistence Tools
 * 竞赛相关数据表
 */
@Entity
@Table(name = "dc_cmptdata", catalog = "dc")
public class DcCmptdata implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer cmptId;//竞赛id
	private String name;//数据名称
	private String file;//储存文件路径或文件索引id
	private String size;//数据大小
	private String format;//数据格式
	private Timestamp time;//上传时间
	private Integer type;//文件格式 (1:2:3:csv)
	private String extrainfo;//测试提交所得分数， json 格式
    private String status = "";//提交状态

	// Constructors

	/** default constructor */
	public DcCmptdata() {
	}

	/** minimal constructor */
	public DcCmptdata(Integer cmptId, String name, String file, String size,
			String format, Timestamp time, Integer type) {
		this.cmptId = cmptId;
		this.name = name;
		this.file = file;
		this.size = size;
		this.format = format;
		this.time = time;
		this.type = type;
	}

	/** full constructor */
	public DcCmptdata(Integer cmptId, String name, String file, String size,
			String format, Timestamp time, Integer type, String extrainfo) {
		this.cmptId = cmptId;
		this.name = name;
		this.file = file;
		this.size = size;
		this.format = format;
		this.time = time;
		this.type = type;
		this.extrainfo = extrainfo;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "cmpt_id", nullable = false)
	public Integer getCmptId() {
		return this.cmptId;
	}

	public void setCmptId(Integer cmptId) {
		this.cmptId = cmptId;
	}

	@Column(name = "name", nullable = false, length = 32)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "file", nullable = false, length = 64)
	public String getFile() {
		return this.file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	@Column(name = "size", nullable = false, length = 16)
	public String getSize() {
		return this.size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	@Column(name = "format", nullable = false, length = 16)
	public String getFormat() {
		return this.format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	@Column(name = "time", nullable = false, length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Column(name = "type", nullable = false)
	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "extrainfo", length = 100)
	public String getExtrainfo() {
		return this.extrainfo;
	}

	public void setExtrainfo(String extrainfo) {
		this.extrainfo = extrainfo;
	}

    @Column(name = "status", length = 255)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}