package com.pkbigdata.entity;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * DcScore entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "dc_score", catalog = "dc")
public class DcScore implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer cmptId;//竞赛id
	private Integer stage;//阶段
	private String teamName;//团队名
	private String avatar;//队伍头像
	private Float oldScore;//昨天得分
	private Integer oldRank;//昨天提交排名
	private Float nowScore;//现在得分
	private Integer nowRank;//这次提交paiming
	private Timestamp updateTime;//提交更新时间
	private Integer teamId;//团队id
	private Integer rankListId;//排行榜id
	private Integer diffRank;//排名差
    private Integer commitAccount;//提交次数
	// Constructors

	/** default constructor */
	public DcScore() {
	}

	/** full constructor */
	public DcScore(Integer cmptId, Integer stage, Float oldScore,
				   Integer oldRank, Float nowScore, Integer nowRank,
				   Timestamp updateTime) {
		this.cmptId = cmptId;
		this.stage = stage;
		this.oldScore = oldScore;
		this.oldRank = oldRank;
		this.nowScore = nowScore;
		this.nowRank = nowRank;
		this.updateTime = updateTime;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "cmpt_id")
	public Integer getCmptId() {
		return this.cmptId;
	}

	public void setCmptId(Integer cmptId) {
		this.cmptId = cmptId;
	}

	@Column(name = "stage")
	public Integer getStage() {
		return this.stage;
	}

	public void setStage(Integer stage) {
		this.stage = stage;
	}

	@Column(name = "old_score", precision = 255, scale = 0)
	public Float getOldScore() {
		return this.oldScore;
	}

	public void setOldScore(Float oldScore) {
		this.oldScore = oldScore;
	}

	@Column(name = "old_rank")
	public Integer getOldRank() {
		return this.oldRank;
	}

	public void setOldRank(Integer oldRank) {
		this.oldRank = oldRank;
	}

	@Column(name = "now_score", precision = 255, scale = 0)
	public Float getNowScore() {
		return this.nowScore;
	}

	public void setNowScore(Float nowScore) {
		this.nowScore = nowScore;
	}

	@Column(name = "now_rank")
	public Integer getNowRank() {
		return this.nowRank;
	}

	public void setNowRank(Integer nowRank) {
		this.nowRank = nowRank;
	}

	@Column(name = "update_time", length = 19)
	public Timestamp getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	@Column(name = "team_id")
	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	@Column(name = "rankList_id")
	public Integer getRankListId() {
		return rankListId;
	}

	public void setRankListId(Integer rankListId) {
		this.rankListId = rankListId;
	}

	@Column(name="diff_rank")
	public Integer getDiffRank() {
		return diffRank;
	}

	public void setDiffRank(Integer diffRank) {
		this.diffRank = diffRank;
	}

	@Column(name="commit_account")
	public Integer getCommitAccount() {
		return commitAccount;
	}

	public void setCommitAccount(Integer commitAccount) {
		this.commitAccount = commitAccount;
	}

	@Column(name="team_name")
	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	@Column(name="avatar")
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
}