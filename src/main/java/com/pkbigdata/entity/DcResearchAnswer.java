package com.pkbigdata.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ck on 2016/8/26.
 */
@Entity
@Table(name="dc_research_answer",catalog = "dc")
public class DcResearchAnswer implements java.io.Serializable{
    private Integer id;
    private String answer;//答案
    private Integer userId;//用户Id
    private Integer researchId;//问题id
    private Timestamp createTime;//回答时间

    @Id
    @GeneratedValue
    @Column(name="id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name="answer")
    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Column(name="user_id")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name="research_id")
    public Integer getResearchId() {
        return researchId;
    }

    public void setResearchId(Integer researchId) {
        this.researchId = researchId;
    }

    @Column(name="create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }
}
