package com.pkbigdata.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by 李龙飞 on 2015-04-21.
 * 邀请表
 */
@Entity
@Table(name = "dc_invite", catalog = "dc")
public class DcInvite implements java.io.Serializable{
    private Integer id;
    private Integer inviter;//邀请人id
    private String inviteeMail;//受邀人邮箱或账号或手机号
    private Timestamp time;//发出邀请时间
    private String status;//状态(值为：接受，过期，邀请中)
    private Timestamp expireTime;//过期时间
    private String uri;//邀请链接
    private String inviteCode;//邀请码
    private Integer teamId;//邀请队伍id
    private Integer cmptId;//邀请竞赛

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "inviter", nullable = false, insertable = true, updatable = true)
    public Integer getInviter() {
        return inviter;
    }

    public void setInviter(Integer inviter) {
        this.inviter = inviter;
    }

    @Basic
    @Column(name = "invitee_mail", nullable = false, insertable = true, updatable = true, length = 50)
    public String getInviteeMail() {
        return inviteeMail;
    }

    public void setInviteeMail(String inviteeMail) {
        this.inviteeMail = inviteeMail;
    }

    @Basic
    @Column(name = "time", nullable = false, insertable = true, updatable = true)
    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Basic
    @Column(name = "status", nullable = false, insertable = true, updatable = true, length = 20)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "expire_time", nullable = false, insertable = true, updatable = true)
    public Timestamp getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Timestamp expireTime) {
        this.expireTime = expireTime;
    }

    @Basic
    @Column(name = "uri", nullable = false, insertable = true, updatable = true, length = 255)
    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Basic
    @Column(name = "invite_code", nullable = false, insertable = true, updatable = true, length = 50)
    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DcInvite dcInvite = (DcInvite) o;

        if (id != dcInvite.id) return false;
        if (inviter != dcInvite.inviter) return false;
        if (expireTime != null ? !expireTime.equals(dcInvite.expireTime) : dcInvite.expireTime != null) return false;
        if (inviteCode != null ? !inviteCode.equals(dcInvite.inviteCode) : dcInvite.inviteCode != null) return false;
        if (inviteeMail != null ? !inviteeMail.equals(dcInvite.inviteeMail) : dcInvite.inviteeMail != null)
            return false;
        if (status != null ? !status.equals(dcInvite.status) : dcInvite.status != null) return false;
        if (time != null ? !time.equals(dcInvite.time) : dcInvite.time != null) return false;
        if (uri != null ? !uri.equals(dcInvite.uri) : dcInvite.uri != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + inviter;
        result = 31 * result + (inviteeMail != null ? inviteeMail.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (expireTime != null ? expireTime.hashCode() : 0);
        result = 31 * result + (uri != null ? uri.hashCode() : 0);
        result = 31 * result + (inviteCode != null ? inviteCode.hashCode() : 0);
        return result;
    }

    @Basic
    @Column(name = "team_id", nullable = false, insertable = true, updatable = true)
    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    @Column(name = "cmpt_id")
    public Integer getCmptId() {
        return cmptId;
    }

    public void setCmptId(Integer cmptId) {
        this.cmptId = cmptId;
    }
}
