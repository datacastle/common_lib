package com.pkbigdata.entity;// default package

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * DcSubmission entity. @author MyEclipse Persistence Tools
 * 结果文件提交表
 */
@Entity
@Table(name = "dc_submission", catalog = "dc")
public class DcSubmission implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer userId;//提交用户id
	private Integer teamId;//提交队伍id
	private String name;//用户提交的文件名
	private Timestamp time;//提交时间
	private String description;//用户提交的描述
	private String file;//存储路径
	private Float publicScore;//公开得分
	private Float privateScore;//得分
	private String info;//提交状态信息
	private Boolean selected;//是否被用户选择
	private Integer status;//提交文件的的状态()
    private Integer rank;//当次提交的排名
	private String other;//其他信息
	private Boolean deleteStatus;//删除状态
	private Timestamp deleteTime;//删除时间
	private Integer stage;//哪一个阶段提交
	private Integer rankListId;//排行榜id


	// Constructors

	/** default constructor */
	public DcSubmission() {
	}

	/** minimal constructor */
	public DcSubmission(Integer userId, Integer teamId, String name,
			Timestamp time, String file, Float publicScore, Float privateScore,
			String info, Boolean selected) {
		this.userId = userId;
		this.teamId = teamId;
		this.name = name;
		this.time = time;
		this.file = file;
		this.publicScore = publicScore;
		this.privateScore = privateScore;
		this.info = info;
		this.selected = selected;
	}

	/** full constructor */
	public DcSubmission(Integer userId, Integer teamId, String name,
			Timestamp time, String description, String file, Float publicScore,
			Float privateScore, String info, Boolean selected, Integer status) {
		this.userId = userId;
		this.teamId = teamId;
		this.name = name;
		this.time = time;
		this.description = description;
		this.file = file;
		this.publicScore = publicScore;
		this.privateScore = privateScore;
		this.info = info;
		this.selected = selected;
		this.status = status;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "user_id", nullable = false)
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "team_id", nullable = false)
	public Integer getTeamId() {
		return this.teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	@Column(name = "name", nullable = false, length = 64)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "time", nullable = false, length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Column(name = "description")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "file", nullable = false, length = 64)
	public String getFile() {
		return this.file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	@Column(name = "public_score", nullable = false, precision = 5, scale = 5)
	public Float getPublicScore() {
		return this.publicScore;
	}

	public void setPublicScore(Float publicScore) {
		this.publicScore = publicScore;
	}

	@Column(name = "private_score", nullable = false, precision = 5, scale = 5)
	public Float getPrivateScore() {
		return this.privateScore;
	}

	public void setPrivateScore(Float privateScore) {
		this.privateScore = privateScore;
	}

	@Column(name = "info", nullable = false, length = 100)
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Column(name = "selected", nullable = false)
	public Boolean getSelected() {
		return this.selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

    @Column(name = "rank")
    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

	@Column(name = "other")
	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	@Column(name="delete_status")
	public Boolean getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(Boolean deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	@Column(name="delete_time")
	public Timestamp getDeleteTime() {
		return deleteTime;
	}

	public void setDeleteTime(Timestamp deleteTime) {
		this.deleteTime = deleteTime;
	}

	@Column(name="stage")
	public Integer getStage() {
		return stage;
	}

	public void setStage(Integer stage) {
		this.stage = stage;
	}
	@Column(name = "rankList_id")
	public Integer getRankListId() {
		return rankListId;
	}

	public void setRankListId(Integer rankListId) {
		this.rankListId = rankListId;
	}
}