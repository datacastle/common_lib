package com.pkbigdata.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * 用户通知信息
 * Created by Administrator on 2016/1/14.
 */
@Entity
@Table(name = "dc_notice_user", catalog = "dc")
public class DcNoticeUser {
    private int id;
    private Timestamp readTime;//通知读取时间
    private Integer userId;//用户id
    private String readType;//判断是否已读
    private String userName;//用户姓名
    private Integer managerNoticeId;//管理者通知信息ID
    private String titile;//通知主题
    private Timestamp createTime;//创建时间
    private Boolean delFlag;//删除标记

    @Id
    @Column(name = "id")
    @GeneratedValue
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "read_time")
    public Timestamp getReadTime() {
        return readTime;
    }

    public void setReadTime(Timestamp readTime) {
        this.readTime = readTime;
    }

    @Basic
    @Column(name = "user_id")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "read_type")
    public String getReadType() {
        return readType;
    }

    public void setReadType(String readType) {
        this.readType = readType;
    }

    @Basic
    @Column(name = "user_name")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "manager_notice_id")
    public Integer getManagerNoticeId() {
        return managerNoticeId;
    }

    public void setManagerNoticeId(Integer managerNoticeId) {
        this.managerNoticeId = managerNoticeId;
    }

    @Basic
    @Column(name = "titile")
    public String getTitile() {
        return titile;
    }

    public void setTitile(String titile) {
        this.titile = titile;
    }

    @Basic
    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DcNoticeUser that = (DcNoticeUser) o;

        if (id != that.id) return false;
        if (readTime != null ? !readTime.equals(that.readTime) : that.readTime != null) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (readType != null ? !readType.equals(that.readType) : that.readType != null) return false;
        if (userName != null ? !userName.equals(that.userName) : that.userName != null) return false;
        if (managerNoticeId != null ? !managerNoticeId.equals(that.managerNoticeId) : that.managerNoticeId != null)
            return false;
        if (titile != null ? !titile.equals(that.titile) : that.titile != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (readTime != null ? readTime.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (readType != null ? readType.hashCode() : 0);
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (managerNoticeId != null ? managerNoticeId.hashCode() : 0);
        result = 31 * result + (titile != null ? titile.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        return result;
    }

    @Column(name = "del_flag")
    public Boolean getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Boolean delFlag) {
        this.delFlag = delFlag;
    }
}
