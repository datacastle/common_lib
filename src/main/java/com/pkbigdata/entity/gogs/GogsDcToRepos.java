package com.pkbigdata.entity.gogs;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by jay on 2016/8/17.
 */
@Entity
@Table(name = "dc_to_repos", catalog = "gogs")
public class GogsDcToRepos implements java.io.Serializable{
    private int id;
    private Integer repoId;
    private Integer dcUserId;
    private Integer gogsUserId;
    private Timestamp createTime;
    private Integer cmptId;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "repo_id")
    public Integer getRepoId() {
        return repoId;
    }

    public void setRepoId(Integer repoId) {
        this.repoId = repoId;
    }

    @Basic
    @Column(name = "dc_user_id")
    public Integer getDcUserId() {
        return dcUserId;
    }

    public void setDcUserId(Integer dcUserId) {
        this.dcUserId = dcUserId;
    }

    @Basic
    @Column(name = "gogs_user_id")
    public Integer getGogsUserId() {
        return gogsUserId;
    }

    public void setGogsUserId(Integer gogsUserId) {
        this.gogsUserId = gogsUserId;
    }

    @Basic
    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "cmpt_id")
    public Integer getCmptId() {
        return cmptId;
    }

    public void setCmptId(Integer cmptId) {
        this.cmptId = cmptId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsDcToRepos that = (GogsDcToRepos) o;

        if (id != that.id) return false;
        if (repoId != null ? !repoId.equals(that.repoId) : that.repoId != null) return false;
        if (dcUserId != null ? !dcUserId.equals(that.dcUserId) : that.dcUserId != null) return false;
        if (gogsUserId != null ? !gogsUserId.equals(that.gogsUserId) : that.gogsUserId != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (cmptId != null ? !cmptId.equals(that.cmptId) : that.cmptId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (repoId != null ? repoId.hashCode() : 0);
        result = 31 * result + (dcUserId != null ? dcUserId.hashCode() : 0);
        result = 31 * result + (gogsUserId != null ? gogsUserId.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (cmptId != null ? cmptId.hashCode() : 0);
        return result;
    }
}
