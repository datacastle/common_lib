package com.pkbigdata.entity.gogs;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by jay on 2016/8/19.
 * dc用户git信息初始化
 */
@Entity
@Table(name = "dc_user_init", catalog = "gogs")
public class GogsDcUserInit implements java.io.Serializable{
    private int id;
    private Integer userId;
    private Boolean emailAuth;
    private Boolean pwdAuth;
    private Timestamp pwdAuthTime;
    private Timestamp emailAuthTime;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_id")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "email_auth")
    public Boolean getEmailAuth() {
        return emailAuth;
    }

    public void setEmailAuth(Boolean emailAuth) {
        this.emailAuth = emailAuth;
    }

    @Basic
    @Column(name = "pwd_auth")
    public Boolean getPwdAuth() {
        return pwdAuth;
    }

    public void setPwdAuth(Boolean pwdAuth) {
        this.pwdAuth = pwdAuth;
    }

    @Basic
    @Column(name = "pwd_auth_time")
    public Timestamp getPwdAuthTime() {
        return pwdAuthTime;
    }

    public void setPwdAuthTime(Timestamp pwdAuthTime) {
        this.pwdAuthTime = pwdAuthTime;
    }

    @Basic
    @Column(name = "email_auth_time")
    public Timestamp getEmailAuthTime() {
        return emailAuthTime;
    }

    public void setEmailAuthTime(Timestamp emailAuthTime) {
        this.emailAuthTime = emailAuthTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsDcUserInit that = (GogsDcUserInit) o;

        if (id != that.id) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (emailAuth != null ? !emailAuth.equals(that.emailAuth) : that.emailAuth != null) return false;
        if (pwdAuth != null ? !pwdAuth.equals(that.pwdAuth) : that.pwdAuth != null) return false;
        if (pwdAuthTime != null ? !pwdAuthTime.equals(that.pwdAuthTime) : that.pwdAuthTime != null) return false;
        if (emailAuthTime != null ? !emailAuthTime.equals(that.emailAuthTime) : that.emailAuthTime != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (emailAuth != null ? emailAuth.hashCode() : 0);
        result = 31 * result + (pwdAuth != null ? pwdAuth.hashCode() : 0);
        result = 31 * result + (pwdAuthTime != null ? pwdAuthTime.hashCode() : 0);
        result = 31 * result + (emailAuthTime != null ? emailAuthTime.hashCode() : 0);
        return result;
    }
}
