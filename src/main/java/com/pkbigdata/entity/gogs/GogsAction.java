package com.pkbigdata.entity.gogs;

import javax.persistence.*;

/**
 * Created by jay on 2016/8/12.
 */
@Entity
@Table(name = "action", catalog = "gogs")
public class GogsAction implements java.io.Serializable{
    private long id;
    private Long userId;
    private Integer opType;
    private Long actUserId;
    private String actUserName;
    private String actEmail;
    private Long repoId;
    private String repoUserName;
    private String repoName;
    private String refName;
    private byte isPrivate;
    private String content;
    private Long createdUnix;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "op_type")
    public Integer getOpType() {
        return opType;
    }

    public void setOpType(Integer opType) {
        this.opType = opType;
    }

    @Basic
    @Column(name = "act_user_id")
    public Long getActUserId() {
        return actUserId;
    }

    public void setActUserId(Long actUserId) {
        this.actUserId = actUserId;
    }

    @Basic
    @Column(name = "act_user_name")
    public String getActUserName() {
        return actUserName;
    }

    public void setActUserName(String actUserName) {
        this.actUserName = actUserName;
    }

    @Basic
    @Column(name = "act_email")
    public String getActEmail() {
        return actEmail;
    }

    public void setActEmail(String actEmail) {
        this.actEmail = actEmail;
    }

    @Basic
    @Column(name = "repo_id")
    public Long getRepoId() {
        return repoId;
    }

    public void setRepoId(Long repoId) {
        this.repoId = repoId;
    }

    @Basic
    @Column(name = "repo_user_name")
    public String getRepoUserName() {
        return repoUserName;
    }

    public void setRepoUserName(String repoUserName) {
        this.repoUserName = repoUserName;
    }

    @Basic
    @Column(name = "repo_name")
    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    @Basic
    @Column(name = "ref_name")
    public String getRefName() {
        return refName;
    }

    public void setRefName(String refName) {
        this.refName = refName;
    }

    @Basic
    @Column(name = "is_private")
    public byte getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(byte isPrivate) {
        this.isPrivate = isPrivate;
    }

    @Basic
    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "created_unix")
    public Long getCreatedUnix() {
        return createdUnix;
    }

    public void setCreatedUnix(Long createdUnix) {
        this.createdUnix = createdUnix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsAction that = (GogsAction) o;

        if (id != that.id) return false;
        if (isPrivate != that.isPrivate) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (opType != null ? !opType.equals(that.opType) : that.opType != null) return false;
        if (actUserId != null ? !actUserId.equals(that.actUserId) : that.actUserId != null) return false;
        if (actUserName != null ? !actUserName.equals(that.actUserName) : that.actUserName != null) return false;
        if (actEmail != null ? !actEmail.equals(that.actEmail) : that.actEmail != null) return false;
        if (repoId != null ? !repoId.equals(that.repoId) : that.repoId != null) return false;
        if (repoUserName != null ? !repoUserName.equals(that.repoUserName) : that.repoUserName != null) return false;
        if (repoName != null ? !repoName.equals(that.repoName) : that.repoName != null) return false;
        if (refName != null ? !refName.equals(that.refName) : that.refName != null) return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;
        if (createdUnix != null ? !createdUnix.equals(that.createdUnix) : that.createdUnix != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (opType != null ? opType.hashCode() : 0);
        result = 31 * result + (actUserId != null ? actUserId.hashCode() : 0);
        result = 31 * result + (actUserName != null ? actUserName.hashCode() : 0);
        result = 31 * result + (actEmail != null ? actEmail.hashCode() : 0);
        result = 31 * result + (repoId != null ? repoId.hashCode() : 0);
        result = 31 * result + (repoUserName != null ? repoUserName.hashCode() : 0);
        result = 31 * result + (repoName != null ? repoName.hashCode() : 0);
        result = 31 * result + (refName != null ? refName.hashCode() : 0);
        result = 31 * result + (int) isPrivate;
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (createdUnix != null ? createdUnix.hashCode() : 0);
        return result;
    }
}
