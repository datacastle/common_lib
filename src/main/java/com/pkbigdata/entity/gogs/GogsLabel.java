package com.pkbigdata.entity.gogs;

import javax.persistence.*;

/**
 * Created by jay on 2016/8/12.
 */
@Entity
@Table(name = "label", catalog = "gogs")
public class GogsLabel implements java.io.Serializable{
    private long id;
    private Long repoId;
    private String name;
    private String color;
    private Integer numIssues;
    private Integer numClosedIssues;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "repo_id")
    public Long getRepoId() {
        return repoId;
    }

    public void setRepoId(Long repoId) {
        this.repoId = repoId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "color")
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Basic
    @Column(name = "num_issues")
    public Integer getNumIssues() {
        return numIssues;
    }

    public void setNumIssues(Integer numIssues) {
        this.numIssues = numIssues;
    }

    @Basic
    @Column(name = "num_closed_issues")
    public Integer getNumClosedIssues() {
        return numClosedIssues;
    }

    public void setNumClosedIssues(Integer numClosedIssues) {
        this.numClosedIssues = numClosedIssues;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsLabel gogsLabel = (GogsLabel) o;

        if (id != gogsLabel.id) return false;
        if (repoId != null ? !repoId.equals(gogsLabel.repoId) : gogsLabel.repoId != null) return false;
        if (name != null ? !name.equals(gogsLabel.name) : gogsLabel.name != null) return false;
        if (color != null ? !color.equals(gogsLabel.color) : gogsLabel.color != null) return false;
        if (numIssues != null ? !numIssues.equals(gogsLabel.numIssues) : gogsLabel.numIssues != null) return false;
        if (numClosedIssues != null ? !numClosedIssues.equals(gogsLabel.numClosedIssues) : gogsLabel.numClosedIssues != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (repoId != null ? repoId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (color != null ? color.hashCode() : 0);
        result = 31 * result + (numIssues != null ? numIssues.hashCode() : 0);
        result = 31 * result + (numClosedIssues != null ? numClosedIssues.hashCode() : 0);
        return result;
    }
}
