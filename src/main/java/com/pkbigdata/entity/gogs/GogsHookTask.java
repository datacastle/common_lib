package com.pkbigdata.entity.gogs;

import javax.persistence.*;

/**
 * Created by jay on 2016/8/12.
 */
@Entity
@Table(name = "hook_task", catalog = "gogs")
public class GogsHookTask implements java.io.Serializable{
    private long id;
    private Long repoId;
    private Long hookId;
    private String uuid;
    private Integer type;
    private String url;
    private String payloadContent;
    private Integer contentType;
    private String eventType;
    private Byte isSsl;
    private Byte isDelivered;
    private Long delivered;
    private Byte isSucceed;
    private String requestContent;
    private String responseContent;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "repo_id")
    public Long getRepoId() {
        return repoId;
    }

    public void setRepoId(Long repoId) {
        this.repoId = repoId;
    }

    @Basic
    @Column(name = "hook_id")
    public Long getHookId() {
        return hookId;
    }

    public void setHookId(Long hookId) {
        this.hookId = hookId;
    }

    @Basic
    @Column(name = "uuid")
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Basic
    @Column(name = "type")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Basic
    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "payload_content")
    public String getPayloadContent() {
        return payloadContent;
    }

    public void setPayloadContent(String payloadContent) {
        this.payloadContent = payloadContent;
    }

    @Basic
    @Column(name = "content_type")
    public Integer getContentType() {
        return contentType;
    }

    public void setContentType(Integer contentType) {
        this.contentType = contentType;
    }

    @Basic
    @Column(name = "event_type")
    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    @Basic
    @Column(name = "is_ssl")
    public Byte getIsSsl() {
        return isSsl;
    }

    public void setIsSsl(Byte isSsl) {
        this.isSsl = isSsl;
    }

    @Basic
    @Column(name = "is_delivered")
    public Byte getIsDelivered() {
        return isDelivered;
    }

    public void setIsDelivered(Byte isDelivered) {
        this.isDelivered = isDelivered;
    }

    @Basic
    @Column(name = "delivered")
    public Long getDelivered() {
        return delivered;
    }

    public void setDelivered(Long delivered) {
        this.delivered = delivered;
    }

    @Basic
    @Column(name = "is_succeed")
    public Byte getIsSucceed() {
        return isSucceed;
    }

    public void setIsSucceed(Byte isSucceed) {
        this.isSucceed = isSucceed;
    }

    @Basic
    @Column(name = "request_content")
    public String getRequestContent() {
        return requestContent;
    }

    public void setRequestContent(String requestContent) {
        this.requestContent = requestContent;
    }

    @Basic
    @Column(name = "response_content")
    public String getResponseContent() {
        return responseContent;
    }

    public void setResponseContent(String responseContent) {
        this.responseContent = responseContent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsHookTask that = (GogsHookTask) o;

        if (id != that.id) return false;
        if (repoId != null ? !repoId.equals(that.repoId) : that.repoId != null) return false;
        if (hookId != null ? !hookId.equals(that.hookId) : that.hookId != null) return false;
        if (uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;
        if (payloadContent != null ? !payloadContent.equals(that.payloadContent) : that.payloadContent != null)
            return false;
        if (contentType != null ? !contentType.equals(that.contentType) : that.contentType != null) return false;
        if (eventType != null ? !eventType.equals(that.eventType) : that.eventType != null) return false;
        if (isSsl != null ? !isSsl.equals(that.isSsl) : that.isSsl != null) return false;
        if (isDelivered != null ? !isDelivered.equals(that.isDelivered) : that.isDelivered != null) return false;
        if (delivered != null ? !delivered.equals(that.delivered) : that.delivered != null) return false;
        if (isSucceed != null ? !isSucceed.equals(that.isSucceed) : that.isSucceed != null) return false;
        if (requestContent != null ? !requestContent.equals(that.requestContent) : that.requestContent != null)
            return false;
        if (responseContent != null ? !responseContent.equals(that.responseContent) : that.responseContent != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (repoId != null ? repoId.hashCode() : 0);
        result = 31 * result + (hookId != null ? hookId.hashCode() : 0);
        result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (payloadContent != null ? payloadContent.hashCode() : 0);
        result = 31 * result + (contentType != null ? contentType.hashCode() : 0);
        result = 31 * result + (eventType != null ? eventType.hashCode() : 0);
        result = 31 * result + (isSsl != null ? isSsl.hashCode() : 0);
        result = 31 * result + (isDelivered != null ? isDelivered.hashCode() : 0);
        result = 31 * result + (delivered != null ? delivered.hashCode() : 0);
        result = 31 * result + (isSucceed != null ? isSucceed.hashCode() : 0);
        result = 31 * result + (requestContent != null ? requestContent.hashCode() : 0);
        result = 31 * result + (responseContent != null ? responseContent.hashCode() : 0);
        return result;
    }
}
