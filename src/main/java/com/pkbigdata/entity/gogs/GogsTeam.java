package com.pkbigdata.entity.gogs;

import javax.persistence.*;

/**
 * Created by jay on 2016/8/12.
 */
@Entity
@Table(name = "team", catalog = "gogs")
public class GogsTeam implements java.io.Serializable{
    private long id;
    private Long orgId;
    private String lowerName;
    private String name;
    private String description;
    private Integer authorize;
    private Integer numRepos;
    private Integer numMembers;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "org_id")
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    @Basic
    @Column(name = "lower_name")
    public String getLowerName() {
        return lowerName;
    }

    public void setLowerName(String lowerName) {
        this.lowerName = lowerName;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "authorize")
    public Integer getAuthorize() {
        return authorize;
    }

    public void setAuthorize(Integer authorize) {
        this.authorize = authorize;
    }

    @Basic
    @Column(name = "num_repos")
    public Integer getNumRepos() {
        return numRepos;
    }

    public void setNumRepos(Integer numRepos) {
        this.numRepos = numRepos;
    }

    @Basic
    @Column(name = "num_members")
    public Integer getNumMembers() {
        return numMembers;
    }

    public void setNumMembers(Integer numMembers) {
        this.numMembers = numMembers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsTeam gogsTeam = (GogsTeam) o;

        if (id != gogsTeam.id) return false;
        if (orgId != null ? !orgId.equals(gogsTeam.orgId) : gogsTeam.orgId != null) return false;
        if (lowerName != null ? !lowerName.equals(gogsTeam.lowerName) : gogsTeam.lowerName != null) return false;
        if (name != null ? !name.equals(gogsTeam.name) : gogsTeam.name != null) return false;
        if (description != null ? !description.equals(gogsTeam.description) : gogsTeam.description != null)
            return false;
        if (authorize != null ? !authorize.equals(gogsTeam.authorize) : gogsTeam.authorize != null) return false;
        if (numRepos != null ? !numRepos.equals(gogsTeam.numRepos) : gogsTeam.numRepos != null) return false;
        if (numMembers != null ? !numMembers.equals(gogsTeam.numMembers) : gogsTeam.numMembers != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (orgId != null ? orgId.hashCode() : 0);
        result = 31 * result + (lowerName != null ? lowerName.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (authorize != null ? authorize.hashCode() : 0);
        result = 31 * result + (numRepos != null ? numRepos.hashCode() : 0);
        result = 31 * result + (numMembers != null ? numMembers.hashCode() : 0);
        return result;
    }
}
