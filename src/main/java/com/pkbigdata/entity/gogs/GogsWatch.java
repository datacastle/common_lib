package com.pkbigdata.entity.gogs;

import javax.persistence.*;

/**
 * Created by jay on 2016/8/12.
 */
@Entity
@Table(name = "watch", catalog = "gogs")
public class GogsWatch implements java.io.Serializable{
    private long id;
    private Long userId;
    private Long repoId;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "repo_id")
    public Long getRepoId() {
        return repoId;
    }

    public void setRepoId(Long repoId) {
        this.repoId = repoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsWatch gogsWatch = (GogsWatch) o;

        if (id != gogsWatch.id) return false;
        if (userId != null ? !userId.equals(gogsWatch.userId) : gogsWatch.userId != null) return false;
        if (repoId != null ? !repoId.equals(gogsWatch.repoId) : gogsWatch.repoId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (repoId != null ? repoId.hashCode() : 0);
        return result;
    }
}
