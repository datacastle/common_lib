package com.pkbigdata.entity.gogs;

import javax.persistence.*;

/**
 * Created by jay on 2016/8/12.
 */
@Entity
@Table(name = "repository", catalog = "gogs")
public class GogsRepository implements java.io.Serializable{
    private long id;
    private Long ownerId;
    private String lowerName;
    private String name;
    private String description;
    private String website;
    private String defaultBranch;
    private Integer numWatches;
    private Integer numStars;
    private Integer numForks;
    private Integer numIssues;
    private Integer numClosedIssues;
    private Integer numPulls;
    private Integer numClosedPulls;
    private int numMilestones;
    private int numClosedMilestones;
    private Byte isPrivate;
    private Byte isBare;
    private Byte isMirror;
    private byte enableWiki;
    private Byte enableExternalWiki;
    private String externalWikiUrl;
    private byte enableIssues;
    private Byte enableExternalTracker;
    private String externalTrackerFormat;
    private String externalTrackerStyle;
    private byte enablePulls;
    private byte isFork;
    private Long forkId;
    private Long createdUnix;
    private Long updatedUnix;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "owner_id")
    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    @Basic
    @Column(name = "lower_name")
    public String getLowerName() {
        return lowerName;
    }

    public void setLowerName(String lowerName) {
        this.lowerName = lowerName;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "website")
    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Basic
    @Column(name = "default_branch")
    public String getDefaultBranch() {
        return defaultBranch;
    }

    public void setDefaultBranch(String defaultBranch) {
        this.defaultBranch = defaultBranch;
    }

    @Basic
    @Column(name = "num_watches")
    public Integer getNumWatches() {
        return numWatches;
    }

    public void setNumWatches(Integer numWatches) {
        this.numWatches = numWatches;
    }

    @Basic
    @Column(name = "num_stars")
    public Integer getNumStars() {
        return numStars;
    }

    public void setNumStars(Integer numStars) {
        this.numStars = numStars;
    }

    @Basic
    @Column(name = "num_forks")
    public Integer getNumForks() {
        return numForks;
    }

    public void setNumForks(Integer numForks) {
        this.numForks = numForks;
    }

    @Basic
    @Column(name = "num_issues")
    public Integer getNumIssues() {
        return numIssues;
    }

    public void setNumIssues(Integer numIssues) {
        this.numIssues = numIssues;
    }

    @Basic
    @Column(name = "num_closed_issues")
    public Integer getNumClosedIssues() {
        return numClosedIssues;
    }

    public void setNumClosedIssues(Integer numClosedIssues) {
        this.numClosedIssues = numClosedIssues;
    }

    @Basic
    @Column(name = "num_pulls")
    public Integer getNumPulls() {
        return numPulls;
    }

    public void setNumPulls(Integer numPulls) {
        this.numPulls = numPulls;
    }

    @Basic
    @Column(name = "num_closed_pulls")
    public Integer getNumClosedPulls() {
        return numClosedPulls;
    }

    public void setNumClosedPulls(Integer numClosedPulls) {
        this.numClosedPulls = numClosedPulls;
    }

    @Basic
    @Column(name = "num_milestones")
    public int getNumMilestones() {
        return numMilestones;
    }

    public void setNumMilestones(int numMilestones) {
        this.numMilestones = numMilestones;
    }

    @Basic
    @Column(name = "num_closed_milestones")
    public int getNumClosedMilestones() {
        return numClosedMilestones;
    }

    public void setNumClosedMilestones(int numClosedMilestones) {
        this.numClosedMilestones = numClosedMilestones;
    }

    @Basic
    @Column(name = "is_private")
    public Byte getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(Byte isPrivate) {
        this.isPrivate = isPrivate;
    }

    @Basic
    @Column(name = "is_bare")
    public Byte getIsBare() {
        return isBare;
    }

    public void setIsBare(Byte isBare) {
        this.isBare = isBare;
    }

    @Basic
    @Column(name = "is_mirror")
    public Byte getIsMirror() {
        return isMirror;
    }

    public void setIsMirror(Byte isMirror) {
        this.isMirror = isMirror;
    }

    @Basic
    @Column(name = "enable_wiki")
    public byte getEnableWiki() {
        return enableWiki;
    }

    public void setEnableWiki(byte enableWiki) {
        this.enableWiki = enableWiki;
    }

    @Basic
    @Column(name = "enable_external_wiki")
    public Byte getEnableExternalWiki() {
        return enableExternalWiki;
    }

    public void setEnableExternalWiki(Byte enableExternalWiki) {
        this.enableExternalWiki = enableExternalWiki;
    }

    @Basic
    @Column(name = "external_wiki_url")
    public String getExternalWikiUrl() {
        return externalWikiUrl;
    }

    public void setExternalWikiUrl(String externalWikiUrl) {
        this.externalWikiUrl = externalWikiUrl;
    }

    @Basic
    @Column(name = "enable_issues")
    public byte getEnableIssues() {
        return enableIssues;
    }

    public void setEnableIssues(byte enableIssues) {
        this.enableIssues = enableIssues;
    }

    @Basic
    @Column(name = "enable_external_tracker")
    public Byte getEnableExternalTracker() {
        return enableExternalTracker;
    }

    public void setEnableExternalTracker(Byte enableExternalTracker) {
        this.enableExternalTracker = enableExternalTracker;
    }

    @Basic
    @Column(name = "external_tracker_format")
    public String getExternalTrackerFormat() {
        return externalTrackerFormat;
    }

    public void setExternalTrackerFormat(String externalTrackerFormat) {
        this.externalTrackerFormat = externalTrackerFormat;
    }

    @Basic
    @Column(name = "external_tracker_style")
    public String getExternalTrackerStyle() {
        return externalTrackerStyle;
    }

    public void setExternalTrackerStyle(String externalTrackerStyle) {
        this.externalTrackerStyle = externalTrackerStyle;
    }

    @Basic
    @Column(name = "enable_pulls")
    public byte getEnablePulls() {
        return enablePulls;
    }

    public void setEnablePulls(byte enablePulls) {
        this.enablePulls = enablePulls;
    }

    @Basic
    @Column(name = "is_fork")
    public byte getIsFork() {
        return isFork;
    }

    public void setIsFork(byte isFork) {
        this.isFork = isFork;
    }

    @Basic
    @Column(name = "fork_id")
    public Long getForkId() {
        return forkId;
    }

    public void setForkId(Long forkId) {
        this.forkId = forkId;
    }

    @Basic
    @Column(name = "created_unix")
    public Long getCreatedUnix() {
        return createdUnix;
    }

    public void setCreatedUnix(Long createdUnix) {
        this.createdUnix = createdUnix;
    }

    @Basic
    @Column(name = "updated_unix")
    public Long getUpdatedUnix() {
        return updatedUnix;
    }

    public void setUpdatedUnix(Long updatedUnix) {
        this.updatedUnix = updatedUnix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsRepository that = (GogsRepository) o;

        if (id != that.id) return false;
        if (numMilestones != that.numMilestones) return false;
        if (numClosedMilestones != that.numClosedMilestones) return false;
        if (enableWiki != that.enableWiki) return false;
        if (enableIssues != that.enableIssues) return false;
        if (enablePulls != that.enablePulls) return false;
        if (isFork != that.isFork) return false;
        if (ownerId != null ? !ownerId.equals(that.ownerId) : that.ownerId != null) return false;
        if (lowerName != null ? !lowerName.equals(that.lowerName) : that.lowerName != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (website != null ? !website.equals(that.website) : that.website != null) return false;
        if (defaultBranch != null ? !defaultBranch.equals(that.defaultBranch) : that.defaultBranch != null)
            return false;
        if (numWatches != null ? !numWatches.equals(that.numWatches) : that.numWatches != null) return false;
        if (numStars != null ? !numStars.equals(that.numStars) : that.numStars != null) return false;
        if (numForks != null ? !numForks.equals(that.numForks) : that.numForks != null) return false;
        if (numIssues != null ? !numIssues.equals(that.numIssues) : that.numIssues != null) return false;
        if (numClosedIssues != null ? !numClosedIssues.equals(that.numClosedIssues) : that.numClosedIssues != null)
            return false;
        if (numPulls != null ? !numPulls.equals(that.numPulls) : that.numPulls != null) return false;
        if (numClosedPulls != null ? !numClosedPulls.equals(that.numClosedPulls) : that.numClosedPulls != null)
            return false;
        if (isPrivate != null ? !isPrivate.equals(that.isPrivate) : that.isPrivate != null) return false;
        if (isBare != null ? !isBare.equals(that.isBare) : that.isBare != null) return false;
        if (isMirror != null ? !isMirror.equals(that.isMirror) : that.isMirror != null) return false;
        if (enableExternalWiki != null ? !enableExternalWiki.equals(that.enableExternalWiki) : that.enableExternalWiki != null)
            return false;
        if (externalWikiUrl != null ? !externalWikiUrl.equals(that.externalWikiUrl) : that.externalWikiUrl != null)
            return false;
        if (enableExternalTracker != null ? !enableExternalTracker.equals(that.enableExternalTracker) : that.enableExternalTracker != null)
            return false;
        if (externalTrackerFormat != null ? !externalTrackerFormat.equals(that.externalTrackerFormat) : that.externalTrackerFormat != null)
            return false;
        if (externalTrackerStyle != null ? !externalTrackerStyle.equals(that.externalTrackerStyle) : that.externalTrackerStyle != null)
            return false;
        if (forkId != null ? !forkId.equals(that.forkId) : that.forkId != null) return false;
        if (createdUnix != null ? !createdUnix.equals(that.createdUnix) : that.createdUnix != null) return false;
        if (updatedUnix != null ? !updatedUnix.equals(that.updatedUnix) : that.updatedUnix != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (ownerId != null ? ownerId.hashCode() : 0);
        result = 31 * result + (lowerName != null ? lowerName.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (website != null ? website.hashCode() : 0);
        result = 31 * result + (defaultBranch != null ? defaultBranch.hashCode() : 0);
        result = 31 * result + (numWatches != null ? numWatches.hashCode() : 0);
        result = 31 * result + (numStars != null ? numStars.hashCode() : 0);
        result = 31 * result + (numForks != null ? numForks.hashCode() : 0);
        result = 31 * result + (numIssues != null ? numIssues.hashCode() : 0);
        result = 31 * result + (numClosedIssues != null ? numClosedIssues.hashCode() : 0);
        result = 31 * result + (numPulls != null ? numPulls.hashCode() : 0);
        result = 31 * result + (numClosedPulls != null ? numClosedPulls.hashCode() : 0);
        result = 31 * result + numMilestones;
        result = 31 * result + numClosedMilestones;
        result = 31 * result + (isPrivate != null ? isPrivate.hashCode() : 0);
        result = 31 * result + (isBare != null ? isBare.hashCode() : 0);
        result = 31 * result + (isMirror != null ? isMirror.hashCode() : 0);
        result = 31 * result + (int) enableWiki;
        result = 31 * result + (enableExternalWiki != null ? enableExternalWiki.hashCode() : 0);
        result = 31 * result + (externalWikiUrl != null ? externalWikiUrl.hashCode() : 0);
        result = 31 * result + (int) enableIssues;
        result = 31 * result + (enableExternalTracker != null ? enableExternalTracker.hashCode() : 0);
        result = 31 * result + (externalTrackerFormat != null ? externalTrackerFormat.hashCode() : 0);
        result = 31 * result + (externalTrackerStyle != null ? externalTrackerStyle.hashCode() : 0);
        result = 31 * result + (int) enablePulls;
        result = 31 * result + (int) isFork;
        result = 31 * result + (forkId != null ? forkId.hashCode() : 0);
        result = 31 * result + (createdUnix != null ? createdUnix.hashCode() : 0);
        result = 31 * result + (updatedUnix != null ? updatedUnix.hashCode() : 0);
        return result;
    }
}
