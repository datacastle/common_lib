package com.pkbigdata.entity.gogs;

import javax.persistence.*;

/**
 * Created by jay on 2016/8/12.
 */
@Entity
@Table(name = "pull_request", catalog = "gogs")
public class GogsPullRequest implements java.io.Serializable{
    private long id;
    private Integer type;
    private Integer status;
    private Long issueId;
    private Long index;
    private Long headRepoId;
    private Long baseRepoId;
    private String headUserName;
    private String headBranch;
    private String baseBranch;
    private String mergeBase;
    private Byte hasMerged;
    private String mergedCommitId;
    private Long mergerId;
    private Long mergedUnix;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "type")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Basic
    @Column(name = "status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "issue_id")
    public Long getIssueId() {
        return issueId;
    }

    public void setIssueId(Long issueId) {
        this.issueId = issueId;
    }

    @Basic
    @Column(name = "index")
    public Long getIndex() {
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }

    @Basic
    @Column(name = "head_repo_id")
    public Long getHeadRepoId() {
        return headRepoId;
    }

    public void setHeadRepoId(Long headRepoId) {
        this.headRepoId = headRepoId;
    }

    @Basic
    @Column(name = "base_repo_id")
    public Long getBaseRepoId() {
        return baseRepoId;
    }

    public void setBaseRepoId(Long baseRepoId) {
        this.baseRepoId = baseRepoId;
    }

    @Basic
    @Column(name = "head_user_name")
    public String getHeadUserName() {
        return headUserName;
    }

    public void setHeadUserName(String headUserName) {
        this.headUserName = headUserName;
    }

    @Basic
    @Column(name = "head_branch")
    public String getHeadBranch() {
        return headBranch;
    }

    public void setHeadBranch(String headBranch) {
        this.headBranch = headBranch;
    }

    @Basic
    @Column(name = "base_branch")
    public String getBaseBranch() {
        return baseBranch;
    }

    public void setBaseBranch(String baseBranch) {
        this.baseBranch = baseBranch;
    }

    @Basic
    @Column(name = "merge_base")
    public String getMergeBase() {
        return mergeBase;
    }

    public void setMergeBase(String mergeBase) {
        this.mergeBase = mergeBase;
    }

    @Basic
    @Column(name = "has_merged")
    public Byte getHasMerged() {
        return hasMerged;
    }

    public void setHasMerged(Byte hasMerged) {
        this.hasMerged = hasMerged;
    }

    @Basic
    @Column(name = "merged_commit_id")
    public String getMergedCommitId() {
        return mergedCommitId;
    }

    public void setMergedCommitId(String mergedCommitId) {
        this.mergedCommitId = mergedCommitId;
    }

    @Basic
    @Column(name = "merger_id")
    public Long getMergerId() {
        return mergerId;
    }

    public void setMergerId(Long mergerId) {
        this.mergerId = mergerId;
    }

    @Basic
    @Column(name = "merged_unix")
    public Long getMergedUnix() {
        return mergedUnix;
    }

    public void setMergedUnix(Long mergedUnix) {
        this.mergedUnix = mergedUnix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsPullRequest that = (GogsPullRequest) o;

        if (id != that.id) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (issueId != null ? !issueId.equals(that.issueId) : that.issueId != null) return false;
        if (index != null ? !index.equals(that.index) : that.index != null) return false;
        if (headRepoId != null ? !headRepoId.equals(that.headRepoId) : that.headRepoId != null) return false;
        if (baseRepoId != null ? !baseRepoId.equals(that.baseRepoId) : that.baseRepoId != null) return false;
        if (headUserName != null ? !headUserName.equals(that.headUserName) : that.headUserName != null) return false;
        if (headBranch != null ? !headBranch.equals(that.headBranch) : that.headBranch != null) return false;
        if (baseBranch != null ? !baseBranch.equals(that.baseBranch) : that.baseBranch != null) return false;
        if (mergeBase != null ? !mergeBase.equals(that.mergeBase) : that.mergeBase != null) return false;
        if (hasMerged != null ? !hasMerged.equals(that.hasMerged) : that.hasMerged != null) return false;
        if (mergedCommitId != null ? !mergedCommitId.equals(that.mergedCommitId) : that.mergedCommitId != null)
            return false;
        if (mergerId != null ? !mergerId.equals(that.mergerId) : that.mergerId != null) return false;
        if (mergedUnix != null ? !mergedUnix.equals(that.mergedUnix) : that.mergedUnix != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (issueId != null ? issueId.hashCode() : 0);
        result = 31 * result + (index != null ? index.hashCode() : 0);
        result = 31 * result + (headRepoId != null ? headRepoId.hashCode() : 0);
        result = 31 * result + (baseRepoId != null ? baseRepoId.hashCode() : 0);
        result = 31 * result + (headUserName != null ? headUserName.hashCode() : 0);
        result = 31 * result + (headBranch != null ? headBranch.hashCode() : 0);
        result = 31 * result + (baseBranch != null ? baseBranch.hashCode() : 0);
        result = 31 * result + (mergeBase != null ? mergeBase.hashCode() : 0);
        result = 31 * result + (hasMerged != null ? hasMerged.hashCode() : 0);
        result = 31 * result + (mergedCommitId != null ? mergedCommitId.hashCode() : 0);
        result = 31 * result + (mergerId != null ? mergerId.hashCode() : 0);
        result = 31 * result + (mergedUnix != null ? mergedUnix.hashCode() : 0);
        return result;
    }
}
