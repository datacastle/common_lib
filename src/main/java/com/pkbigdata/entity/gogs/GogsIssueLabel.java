package com.pkbigdata.entity.gogs;

import javax.persistence.*;

/**
 * Created by jay on 2016/8/12.
 */
@Entity
@Table(name = "issue_label", catalog = "gogs")
public class GogsIssueLabel implements java.io.Serializable{
    private long id;
    private Long issueId;
    private Long labelId;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "issue_id")
    public Long getIssueId() {
        return issueId;
    }

    public void setIssueId(Long issueId) {
        this.issueId = issueId;
    }

    @Basic
    @Column(name = "label_id")
    public Long getLabelId() {
        return labelId;
    }

    public void setLabelId(Long labelId) {
        this.labelId = labelId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsIssueLabel that = (GogsIssueLabel) o;

        if (id != that.id) return false;
        if (issueId != null ? !issueId.equals(that.issueId) : that.issueId != null) return false;
        if (labelId != null ? !labelId.equals(that.labelId) : that.labelId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (issueId != null ? issueId.hashCode() : 0);
        result = 31 * result + (labelId != null ? labelId.hashCode() : 0);
        return result;
    }
}
