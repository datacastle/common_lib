package com.pkbigdata.entity.gogs;

import javax.persistence.*;

/**
 * Created by jay on 2016/8/12.
 */
@Entity
@Table(name = "release", catalog = "gogs")
public class GogsRelease implements java.io.Serializable{
    private long id;
    private Long repoId;
    private Long publisherId;
    private String tagName;
    private String lowerTagName;
    private String target;
    private String title;
    private String sha1;
    private Long numCommits;
    private String note;
    private byte isDraft;
    private Byte isPrerelease;
    private Long createdUnix;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "repo_id")
    public Long getRepoId() {
        return repoId;
    }

    public void setRepoId(Long repoId) {
        this.repoId = repoId;
    }

    @Basic
    @Column(name = "publisher_id")
    public Long getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Long publisherId) {
        this.publisherId = publisherId;
    }

    @Basic
    @Column(name = "tag_name")
    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    @Basic
    @Column(name = "lower_tag_name")
    public String getLowerTagName() {
        return lowerTagName;
    }

    public void setLowerTagName(String lowerTagName) {
        this.lowerTagName = lowerTagName;
    }

    @Basic
    @Column(name = "target")
    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Basic
    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "sha1")
    public String getSha1() {
        return sha1;
    }

    public void setSha1(String sha1) {
        this.sha1 = sha1;
    }

    @Basic
    @Column(name = "num_commits")
    public Long getNumCommits() {
        return numCommits;
    }

    public void setNumCommits(Long numCommits) {
        this.numCommits = numCommits;
    }

    @Basic
    @Column(name = "note")
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Basic
    @Column(name = "is_draft")
    public byte getIsDraft() {
        return isDraft;
    }

    public void setIsDraft(byte isDraft) {
        this.isDraft = isDraft;
    }

    @Basic
    @Column(name = "is_prerelease")
    public Byte getIsPrerelease() {
        return isPrerelease;
    }

    public void setIsPrerelease(Byte isPrerelease) {
        this.isPrerelease = isPrerelease;
    }

    @Basic
    @Column(name = "created_unix")
    public Long getCreatedUnix() {
        return createdUnix;
    }

    public void setCreatedUnix(Long createdUnix) {
        this.createdUnix = createdUnix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsRelease that = (GogsRelease) o;

        if (id != that.id) return false;
        if (isDraft != that.isDraft) return false;
        if (repoId != null ? !repoId.equals(that.repoId) : that.repoId != null) return false;
        if (publisherId != null ? !publisherId.equals(that.publisherId) : that.publisherId != null) return false;
        if (tagName != null ? !tagName.equals(that.tagName) : that.tagName != null) return false;
        if (lowerTagName != null ? !lowerTagName.equals(that.lowerTagName) : that.lowerTagName != null) return false;
        if (target != null ? !target.equals(that.target) : that.target != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (sha1 != null ? !sha1.equals(that.sha1) : that.sha1 != null) return false;
        if (numCommits != null ? !numCommits.equals(that.numCommits) : that.numCommits != null) return false;
        if (note != null ? !note.equals(that.note) : that.note != null) return false;
        if (isPrerelease != null ? !isPrerelease.equals(that.isPrerelease) : that.isPrerelease != null) return false;
        if (createdUnix != null ? !createdUnix.equals(that.createdUnix) : that.createdUnix != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (repoId != null ? repoId.hashCode() : 0);
        result = 31 * result + (publisherId != null ? publisherId.hashCode() : 0);
        result = 31 * result + (tagName != null ? tagName.hashCode() : 0);
        result = 31 * result + (lowerTagName != null ? lowerTagName.hashCode() : 0);
        result = 31 * result + (target != null ? target.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (sha1 != null ? sha1.hashCode() : 0);
        result = 31 * result + (numCommits != null ? numCommits.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        result = 31 * result + (int) isDraft;
        result = 31 * result + (isPrerelease != null ? isPrerelease.hashCode() : 0);
        result = 31 * result + (createdUnix != null ? createdUnix.hashCode() : 0);
        return result;
    }
}
