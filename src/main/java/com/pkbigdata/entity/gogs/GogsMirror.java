package com.pkbigdata.entity.gogs;

import javax.persistence.*;

/**
 * Created by jay on 2016/8/12.
 */
@Entity
@Table(name = "mirror", catalog = "gogs")
public class GogsMirror implements java.io.Serializable{
    private long id;
    private Long repoId;
    private Integer interval;
    private byte enablePrune;
    private Long updatedUnix;
    private Long nextUpdateUnix;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "repo_id")
    public Long getRepoId() {
        return repoId;
    }

    public void setRepoId(Long repoId) {
        this.repoId = repoId;
    }

    @Basic
    @Column(name = "interval")
    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    @Basic
    @Column(name = "enable_prune")
    public byte getEnablePrune() {
        return enablePrune;
    }

    public void setEnablePrune(byte enablePrune) {
        this.enablePrune = enablePrune;
    }

    @Basic
    @Column(name = "updated_unix")
    public Long getUpdatedUnix() {
        return updatedUnix;
    }

    public void setUpdatedUnix(Long updatedUnix) {
        this.updatedUnix = updatedUnix;
    }

    @Basic
    @Column(name = "next_update_unix")
    public Long getNextUpdateUnix() {
        return nextUpdateUnix;
    }

    public void setNextUpdateUnix(Long nextUpdateUnix) {
        this.nextUpdateUnix = nextUpdateUnix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsMirror that = (GogsMirror) o;

        if (id != that.id) return false;
        if (enablePrune != that.enablePrune) return false;
        if (repoId != null ? !repoId.equals(that.repoId) : that.repoId != null) return false;
        if (interval != null ? !interval.equals(that.interval) : that.interval != null) return false;
        if (updatedUnix != null ? !updatedUnix.equals(that.updatedUnix) : that.updatedUnix != null) return false;
        if (nextUpdateUnix != null ? !nextUpdateUnix.equals(that.nextUpdateUnix) : that.nextUpdateUnix != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (repoId != null ? repoId.hashCode() : 0);
        result = 31 * result + (interval != null ? interval.hashCode() : 0);
        result = 31 * result + (int) enablePrune;
        result = 31 * result + (updatedUnix != null ? updatedUnix.hashCode() : 0);
        result = 31 * result + (nextUpdateUnix != null ? nextUpdateUnix.hashCode() : 0);
        return result;
    }
}
