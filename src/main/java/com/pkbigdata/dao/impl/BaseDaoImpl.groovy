package com.pkbigdata.dao.impl;


import com.pkbigdata.dao.BaseDao;
import com.pkbigdata.util.CommonUtil;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.*;


/**
 * Created by jay on 2014/11/28.
 */
@Repository("BaseDao")
class BaseDaoImpl implements BaseDao {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public <T> T getEntityById(Class<T> t, Serializable id) {
        String hql = "from "+t.getName() +" u where u.id = ?";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);

        Integer param = -1;
        if (id == null){
            param = -1;
        }else if (!CommonUtil.isStr2Num(id.toString())){
            param = -1;
        }else{
            param = Integer.valueOf(id.toString());
        }
        query.setParameter(0,param);

        return (T) query.setCacheable(true).uniqueResult();

    }

    @Override
    public <T> T selectSingleBySql(String sql, Object... params) {
        Session session = this.sessionFactory.getCurrentSession();
        SQLQuery query = session.createSQLQuery(sql);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        return (T) query.uniqueResult();
    }

    @Override
    public <T> T selectSingleBySql(String sql) {
        Session session = this.sessionFactory.getCurrentSession();
        SQLQuery query = session.createSQLQuery(sql);
        return (T) query.uniqueResult();
    }
    /**
     * 查找所有的实体
     *
     * @param t
     * @return
     */
    @Override
    public <T> List<T> getAllEntity(Class<T> t) {
        String hql = "from "+t.getName();
        Query query = sessionFactory.getCurrentSession().createQuery(hql);

        return query.list();
    }

    /**
     * 保存实体
     *
     * @param t
     */
    @Override
    public <T> void saveEntity(T t) {
        sessionFactory.getCurrentSession().save(t);
    }

    /**
     * 批量更新实体
     *
     * @param entitys
     */
    @Override
    public <T> void batchUpdate(List<T> entitys) {
        Session session = sessionFactory.getCurrentSession();
        for (int i = 0; i < entitys.size(); i++) {
            session.update(entitys.get(i));
            if (i % 20 == 0) {
                // 20个对象后才清理缓存，写入数据库
                session.flush();
                session.clear();
            }
        }
        // 最后清理一下----防止大于20小于40的不保存
        session.flush();
        session.clear();
    }

    /**
     * 通过实体删除
     *
     * @param t
     */
    @Override
    public <T> void delEntity(T t) {
        String hql = "delete "+t.getClass().getName()+" as model where model.id = ?";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setParameter(0,t['id']);
        query.executeUpdate()
    }

    /**
     * 通过id删除实体
     *
     * @param t
     * @param id
     */
    @Override
    public <T> Boolean delEntityById(Class<T> t, Serializable id) {
        String hql = "delete "+t.getName()+" as model where model.id = ?";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setParameter(0,Integer.valueOf(id.toString()));
        return (query.executeUpdate() > 0);
    }

    /**
     * 更新实体
     *
     * @param t
     */
    @Override
    public <T> Boolean updateEntity(T t) {
        sessionFactory.getCurrentSession().saveOrUpdate(t);
        return true;
    }

    /**
     * 批量保存实体
     *
     * @param entitys
     */
    @Override
    public <T> void batchSave(List<T> entitys) {
        Session session = sessionFactory.getCurrentSession();
        for (int i = 0; i < entitys.size(); i++) {
            session.save(entitys.get(i));
            if (i % 20 == 0) {
                // 20个对象后才清理缓存，写入数据库
                session.flush();
                session.clear();
            }
        }
        // 最后清理一下----防止大于20小于40的不保存
        session.flush();
        session.clear();
    }

    /**
     * 通过实体和实体属性后去唯一值
     *
     * @param entityClass
     * @param propertyName
     * @param value
     * @return
     */
    @Override
    public <T> T findUniqueByProperty(Class<T> entityClass, String propertyName, Object value) {

        String hql = "from "+entityClass.getName()+" u where u."+propertyName+"= ?";

        Query query = sessionFactory.getCurrentSession().createQuery(hql);

        query.setParameter(0, value);

        return (T) query.uniqueResult();
    }

    /**
     * 通过实体和实体类属性查找列表值
     *
     * @param entityClass
     * @param propertyName
     * @param value
     * @return
     */
    @Override
    public <T> List<T> findByProperty(Class<T> entityClass, String propertyName, Object value) {
        String hql = "from "+entityClass.getName()+" u where u."+propertyName+"= ?";

        Query query = sessionFactory.getCurrentSession().createQuery(hql);

        query.setParameter(0,value);

        return query.list();
    }

    /**
     * 通过实体集合删除实体
     * @param entitys
     * @param <T>
     */
    @Override
    public <T> void deleteAllEntitie(Collection<T> entitys) {
        if (entitys){
            Session session = sessionFactory.getCurrentSession();
            String hql = "delete "+entitys[0].getClass().getName()+" as model where model.id = ?";
            Query query = sessionFactory.getCurrentSession().createQuery(hql);
            query.setParameter(0,entitys[0]['id']);
            (1..entitys.size()-1).each{
                hql += " or model.id = ?"
                query.setParameter(it,entitys[it]['id']);
            }
            query.executeUpdate()
        }

    }

    /**
     * 通过hql获取列表数据
     *
     * @param hql
     * @return
     */
    @Override
    public <T> List<T> findByHqlString(String hql) {
        Session session = sessionFactory.getCurrentSession();

        Query query = session.createQuery(hql);
        List<T> list = query.list();

        if(list.size()>0){
            session.flush();
        }
        return list;
    }

    /**
     * 通过hql取第一条
     *
     * @param hql
     * @return
     */
    @Override
    public <T> T findByHqlFirst(String hql) {

        Session sesion = sessionFactory.getCurrentSession();

        Query queryObject = sesion.createQuery(hql);
        queryObject.setFirstResult(0);
        queryObject.setMaxResults(1);

        return (T) queryObject.uniqueResult();
    }

    /**
     * 通过hql取第一条
     *
     * @param hql
     * @param params
     * @return
     */
    @Override
    public <T> T findByHqlFirst(String hql, Object... params) {
        Session sesion = sessionFactory.getCurrentSession();

        Query queryObject = sesion.createQuery(hql);
        for(int i=0;i<params.length;i++){
            queryObject.setParameter(i,params[i]);
        }

        queryObject.setFirstResult(0);
        queryObject.setMaxResults(1);

        return (T) queryObject.uniqueResult();
    }

    /**
     * 通过sql更新数据
     *
     * @param sql
     * @return
     */
    @Override
    public int updateBySqlString(String sql) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createSQLQuery(sql);

        return query.executeUpdate();
    }

    /**
     * 通过sql获取list
     *
     * @param query
     * @return
     */
    @Override
    public <T> List<T> findListbySql(String query) {
        Session session = sessionFactory.getCurrentSession();
        Query queryList = session.createSQLQuery(query);

        return queryList.list();
    }

    /**
     * 通过属性查找列表，并进行排序
     *
     * @param entityClass
     * @param propertyName
     * @param value
     * @param isAsc
     * @return
     */
    @Override
    public <T> List<T> findByPropertyisOrder(Class<T> entityClass, String propertyName, Object value, boolean isAsc) {

        String paixu = "";
        if (true == isAsc){
            paixu = "asc";
        }else{
            paixu = "desc";
        }
        String hql = "from "+entityClass.getName()+" u where u."+propertyName+"= ? order by u."+propertyName+" "+paixu;

        Query query = sessionFactory.getCurrentSession().createQuery(hql);

        query.setParameter(0,value);

        return query.list();
    }

    /**
     * 通过hql获取唯一实体值
     *
     * @param hql
     * @return
     */
    @Override
    public <T> T findsingleResult(String hql) {

        Session sesion = sessionFactory.getCurrentSession();

        Query queryObject = sesion.createQuery(hql);

        return (T) queryObject.uniqueResult();
    }

    @Override
    public <T> T findsingleResult(String hql, Object... params) {

        Session sesion = sessionFactory.getCurrentSession();

        Query queryObject = sesion.createQuery(hql);
        for(int i=0;i<params.length;i++){
            queryObject.setParameter(i,params[i]);
        }

        return (T) queryObject.uniqueResult();
    }

    /**
     * 单表分页查询
     */
    @Override
    public <T> List<T> queryForPage(Class<T> entityClass, int page, int pagesize) {

        String hql = "from "+ entityClass.getName()+" u";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);

        query.setFirstResult((page-1)*pagesize);
        query.setMaxResults(pagesize);


        return query.list();
    }

    /**
     * 单表分页查询，通过属性排序,TRUE则使用order by property ASC
     * false 则使用order by property DESC
     */
    @Override
    public <T> List<T> queryForPage(Class<T> entityClass, int page,
                                    int pagesize, String propertyName, boolean isAsc) {

        String hql = "from "+ entityClass.getName()+" u order by u."+ propertyName+" "+(isAsc?"ASC":"DESC");
        Query query = sessionFactory.getCurrentSession().createQuery(hql);

        query.setFirstResult((page-1)*pagesize);
        query.setMaxResults(pagesize);


        return query.list();
    }

    /**
     * 通过hql分页
     */
    @Override
    public <T> List<T> queryForPageByHQL(String hql, int page, int pagesize) {

        Query query = sessionFactory.getCurrentSession().createQuery(hql);

        query.setFirstResult((page-1)*pagesize);
        query.setMaxResults(pagesize);

        return query.list();
    }

    /**
     * 执行绑定参数的hql。可用于添加，修改，删除
     * @param hql 在HQL查询语句中用”?”来定义参数位置
     *
     * @param params 可变参数 注意：参数只能是基本数据类型
     */
    @Override
    public int executeHql(String hql, Object... params) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        return query.executeUpdate();

    }

    /**
     * 执行绑定参数的hql。可用于添加，修改，删除
     * @param hql 使用name占位符 即在HQL语句中定义命名参数要用”:”开头
     * @param params 可变参数 注意：参数只能是基本数据类型
     */
    @Override
    public int  executeHql(String hql, Map<String, Object> params) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);

        Set<String> set = params.keySet();
        Iterator<String> it = set.iterator();
        while (it.hasNext()) {
            String key = it.next();
            Object value = params.get(key);
            query.setParameter(key, value);
        }

        return query.executeUpdate();

    }


    /**
     * 执行绑定参数的hql。用于查询出单个对象
     * @param hql 在HQL查询语句中用”?”来定义参数位置
     * @param params 可变参数 注意：参数只能是基本数据类型
     * @return 单个查询对象
     */
    @Override
    public <T> T selectByHql(String hql, Object... params) {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        return (T) query.uniqueResult();
    }

    /**
     * 执行绑定参数的hql。用于查询出集合对象
     * @param hql 在HQL查询语句中用”?”来定义参数位置
     * @param params 可变参数 注意：参数只能是基本数据类型
     * @return 集合对象
     */
    @Override
    public <T> List<T> selectListByHql(String hql, Object... params) {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        List list = query.list();
        return list;
    }

    /**
     * 执行绑定参数SQL。用于添加，修改，删除
     * @param sql 用“？”来定义参数位置
     * @param params 基本数据类型的参数
     */
    @Override
    public int executeSql(String sql, Object... params) {
        Session session = this.sessionFactory.getCurrentSession();
        SQLQuery query = session.createSQLQuery(sql);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        return query.executeUpdate();

    }

    /**
     * 执行绑定参数的SQL。用于查询单个对象
     * @param sql 用“？”来定义参数位置
     * @param params 基本数据类型的参数
     * @return
     */
    @Override
    public <T> T selectBySql(String sql, Class<T> entity, Object... params) {
        Session session = this.sessionFactory.getCurrentSession();
        SQLQuery query = session.createSQLQuery(sql).addEntity(entity);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        return (T) query.list();
    }

    @Override
    public <T> List<T> selectListBySql(String sql, Class<T> entity, Object... params) {
        Session session = this.sessionFactory.getCurrentSession();
        SQLQuery query = session.createSQLQuery(sql).addEntity(entity);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        return  query.list();
    }

    @Override
    public <T> List<T> queryForPageByHQL(String hql, int page, int pageSize, Object... params) {

        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setFirstResult((page-1)*pageSize);
        query.setMaxResults(pageSize);
        for(int i=0;i<params.length;i++){
            query.setParameter(i,params[i]);
        }
        return query.list();

    }


    @Override
    public <T> List<T> queryForPageBySQL(String sql, int page, int pageSize, Object... params) {
        Session session = this.sessionFactory.getCurrentSession();
        SQLQuery query = (SQLQuery)session.createSQLQuery(sql).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        query.setFirstResult((page-1)*pageSize);
        query.setMaxResults(pageSize);
        for(int i=0;i<params.length;i++){
            query.setParameter(i,params[i]);
        }
        List list = query.list();
        return (List<T>) query.list();
    }


    @Override
    public <T> List<T> queryForPageBySQL(String sql, int page, int pageSize) {
        Session session = this.sessionFactory.getCurrentSession();
        SQLQuery query = (SQLQuery)session.createSQLQuery(sql).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        query.setFirstResult((page-1)*pageSize);
        query.setMaxResults(pageSize);
        List list = query.list();
        return (List<T>) query.list();
    }

    @Override
    public <T> List<T> selectBySql(String sql, Object... params) {
        Session session = this.sessionFactory.getCurrentSession();
        SQLQuery query = (SQLQuery)session.createSQLQuery(sql).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        return (List<T>) query.list();
    }

    @Override
    public <T> List<T> selectBySql(String sql, Map<String,Type> type, Object... params) {
        Session session = this.sessionFactory.getCurrentSession();
        SQLQuery query = session.createSQLQuery(sql);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        for (String key:type.keySet()){
            query.addScalar(key,type.get(key));
        }
        return (List<T>) query.list();
    }

    @Override
    public Integer getTableCount(String sql) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        BigInteger bi = (BigInteger) query.uniqueResult();
        return bi.intValue();
    }

    /**
     *
     * @param entity
     * @param orderProperty
     * @param isAsc
     * @param <T>
     * @return
     */
    @Override
    public <T> List<T> findByProperties(Class<T> entity, String orderProperty,boolean isAsc,Object... params) {
        String hql = "from "+entity.getName()+" model where ";

        /**
         * 筛选
         */
        if (params!=null){
            for (int i = 0; i < params.length-1; i=i+2) {

                if (!check(params[i]+"")){
                    try {
                        throw new Exception("service.findByProperties 参数错误 ,必须包含常用的判断符号，例如=><!");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                hql += " model."+params[i]+" ?";
                if (i<params.length-3){
                    hql += " and";
                }
            }
        }

        /**
         * 排序
         */
        String paixu = "";
        if (true == isAsc){
            paixu = " asc";
        }else{
            paixu = " desc";
        }

        hql += " order by model."+orderProperty+paixu;

        Query query = sessionFactory.getCurrentSession().createQuery(hql);

        if (params!=null){
            int i = 0,j=0;
            while (i < params.length-1){
                query.setParameter(j,params[i+1]);
                i=i+2
                j++
            }
        }

        return query.list();
    }

    /**
     * 多参数查找，严格遵循params的长度是2的倍数,
     *
     * @param entity 实体表名
     * @param params 查找参数
     * @return
     */
    @Override
    public <T> List<T> findByProperties(Class<T> entity, Object... params) {
        String hql = "from "+entity.getName()+" model where ";

        /**
         * 筛选
         */
        if (params!=null){
            for (int i = 0; i < params.length-1; i=i+2) {
                if (!check(params[i]+"")){
                    try {
                        throw new Exception("service.findByProperties 参数错误 ,必须包含常用的判断符号，例如=><!");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                hql += " model."+params[i]+" ?";
                if (i<params.length-3){
                    hql += " and";
                }
            }
        }

        Query query = sessionFactory.getCurrentSession().createQuery(hql);

        if (params!=null){
            int i = 0,j=0;
            while (i < params.length-1){
                query.setParameter(j,params[i+1]);
                i=i+2
                j++
            }
        }

        return query.list();
    }

    private static boolean check(String param){
        List<String> tiaojian =  new ArrayList<String>();
        tiaojian.add("=");
        tiaojian.add(">");
        tiaojian.add("<");
        tiaojian.add("!");
        tiaojian.add("is");
        tiaojian.add("not");
        for (int i = 0; i < tiaojian.size(); i++) {
            if (param.contains(tiaojian.get(i))){
                return true;
            }
        }
        return false;
    }

    @Override
    public <T> List<T> selectListByLimit(String hql, int limit, Object... params) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setMaxResults(limit);
        for(int i=0;i<params.length;i++){
            query.setParameter(i,params[i]);
        }
        return query.list();
    }
}