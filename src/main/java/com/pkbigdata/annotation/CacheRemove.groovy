package com.pkbigdata.annotation

import java.lang.annotation.*

/**
 * Created by jay on 2016/11/29 0029.
 */
@Target([ElementType.METHOD, ElementType.TYPE ])
@Retention(RetentionPolicy.RUNTIME)
@Documented
@interface CacheRemove {

    Class<?> cacheClass() ;//删除的类
    String[] method() default [];//删除的方法,可选
    String key() default ''//删除的参数内容，可选
    /**
     * 删除条件，等于""则默认直接按键删除，否则condition为true才能删除
     * #returnValue,代表的是返回值，其他标准spring el表达式
     * returnValue是一个保留字，在使用缓存清除的方法上，方法参数名不能是returnValue，否则会出现无法访问参数的情况
     * @return
     */
    String condition() default "";
}