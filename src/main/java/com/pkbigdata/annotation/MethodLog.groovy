package com.pkbigdata.annotation

import java.lang.annotation.*

/**
 * Created by ck on 2016/9/22.
 */


@Target([ElementType.METHOD, ElementType.TYPE ])
@Retention(RetentionPolicy.RUNTIME)
@Documented
@interface MethodLog {
    String remark() default "";//操作内容
    String operType() default "";//操作内型(增/删/查/改)
}